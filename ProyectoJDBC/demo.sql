SELECT * FROM (
SELECT p.idProducte, p.nomProducte, (ifnull(sum(pl.quantitat),0) + IFNULL(p.stock,0)) AS stock
FROM producte p 
LEFT JOIN producte_lot pl ON p.idProducte = pl.idProducte
GROUP BY p.idProducte) AS productes
LEFT JOIN (
SELECT idProducte, sum(quantitat) FROM comanda_linia cl INNER JOIN comanda c ON cl.idComanda = c.idComanda WHERE c.estatComanda = 'PENDENT' GROUP BY idProducte) as linies
ON productes.idProducte = linies.idProducte;

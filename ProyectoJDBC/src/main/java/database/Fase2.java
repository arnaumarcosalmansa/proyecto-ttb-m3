package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Scanner;
import java.util.Set;

import javax.swing.plaf.synth.SynthSpinnerUI;

import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;

import com.Adrian.ProyectoJDBC.Program;

import utils.TravelingSalesman;

public class Fase2 {

	public static void opcion3(Scanner input, Database database) {
		ResultSet rs = null;
		Set<Integer> ids = null;

		System.out.println("Clientes disponibles:");
		rs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "idClient", "nomClient" }, "client",
				null);
		ids = DatabaseFunctions.getIdsFromResultSet("idClient", rs);

		System.out.println(DatabaseFunctions.resultSetToString(new String[] { "idClient", "nomClient" }, rs));

		System.out.print("Introduce el id de cliente: ");
		Integer client = 0;

		while (client <= 0) {
			try {
				client = input.nextInt();
			} catch (Exception e) {
				input.nextLine();
			}
		}

		if (!ids.contains(client)) {
			System.out.println("¡Ese cliente no existe!");
			return;
		}
		double latitud = 0, longitud = 0;
		rs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "latitud", "longitud" },
				"direccio INNER JOIN client ON direccio.idDireccio = client.idDireccio", "idClient = ?",
				new Object[] { client });
		try {
			if (rs.next()) {
				latitud = rs.getDouble("latitud");
				longitud = rs.getDouble("longitud");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double distancia = -1;
		try {
			distancia = TravelingSalesman.distanceToPointFromSabadell(latitud, longitud);
		} catch (NoSuchAuthorityCodeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FactoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		rs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "preu" }, "tarifa",
				"? BETWEEN desde AND fins", new Object[] { distancia });
		double portes = 0;
		try {
			rs.next();
			portes = rs.getDouble("preu");
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		ResultSet comandaCreada = DatabaseFunctions.insertIntoValues(database, "comanda",
				new String[] { "idClient", "dataComanda", "estatComanda", "ports" },
				new Object[] { client, LocalDate.now(), "PENDENT", portes });

		Integer comanda = 0;
		try {
			if (comandaCreada.next()) {
				comanda = comandaCreada.getInt(1);
			}
		} catch (SQLException e) {

		}

		int i = 1;
		while (true) {
			String[] columns = { "idProducte", "nomProducte", "preuVenda", "stock" };
			rs = DatabaseFunctions.selectFieldsFromWhere(database, columns, "producte", "tipusProducte = 'VENDIBLE'");

			Integer producte;

			do {
				System.out.println("Productos disponibles:");
				System.out.println(DatabaseFunctions.resultSetColumnString(rs));
				System.out.println(DatabaseFunctions.resultSetToString(columns, rs));

				Set<Integer> idsProductes = DatabaseFunctions.getIdsFromResultSet("idProducte", rs);
				System.out.print("Introduce el id de producto (0 para salir): ");
				producte = -1;
				while (!idsProductes.contains(producte) && producte != 0) {
					try {
						producte = input.nextInt();
					} catch (Exception e) {
						input.nextLine();
						System.out.println("Solo se permiten numeros enteros.");
						// e.printStackTrace();
					}
				}
				if (i == 1 && producte == 0) {
					System.out.println("Tienes que introducir minimo una linea.");
				}
			} while (i == 1 && producte == 0);

			if (producte != 0) {
				System.out.print("Introduce una cantidad: ");
				Integer quantitat = 0;
				while (quantitat <= 0) {
					try {
						quantitat = input.nextInt();
					} catch (Exception e) {
						input.nextLine();
					}
				}

				ResultSet tmprs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "preuVenda" },
						"producte", "idProducte = ?", new Object[] { producte });
				double preuVenda = -1;
				try {
					tmprs.next();
					preuVenda = tmprs.getDouble("preuVenda");
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.println("PRECIO DE VENTA --> " + preuVenda);
				System.out.print("¿Quieres cambiar el precio?(S/n): ");
				if ("s".equals(input.next().toLowerCase())) {
					System.out.print("Introduce el nuevo precio: ");
					boolean valid = false;
					while (!valid) {
						try {
							preuVenda = input.nextDouble();
							valid = true;
						} catch (Exception e) {
							input.nextLine();
						}
					}
				}
				DatabaseFunctions.insertIntoValues(database, "comanda_linia",
						new String[] { "idComanda", "idLinia", "idProducte", "quantitat", "preuVenda", },
						new Object[] { comanda, i, producte, quantitat, preuVenda });
				i++;
			} else {
				break;
			}
		}
	}

	public static void opcion4(Scanner input, Database database) {
		ResultSet rs = null;
		Set<Integer> ids = null;

		System.out.println("Clientes disponibles:");
		rs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "idClient", "nomClient" }, "client",
				null);
		System.out.println(DatabaseFunctions.resultSetToString(new String[] { "idClient", "nomClient" }, rs));

		ids = DatabaseFunctions.getIdsFromResultSet("idClient", rs);

		Integer cliente = -1;
		while (!ids.contains(cliente)) {
			System.out.print("Introduce el id de cliente: ");
			cliente = input.nextInt();
		}

		rs = DatabaseFunctions.selectFieldsFromWhere(database, new String[] { "idComanda", "estatComanda" }, "comanda",
				"comanda.idClient = ?", new Object[] { cliente });

		ids = DatabaseFunctions.getIdsFromResultSet("idComanda", rs);

		if (!ids.isEmpty()) {
			System.out.println("Pedidos del cliente " + cliente);
			System.out.println(DatabaseFunctions.resultSetToString(new String[] { "idComanda", "estatComanda" }, rs));
			Integer comanda = -1;
			while (!ids.contains(comanda)) {
				System.out.println("Introduce id del pedido:");
				comanda = input.nextInt();
			}
			rs = DatabaseFunctions.selectFieldsFromWhere(database,
					new String[] { "producte.idProducte", "producte.nomProducte", "comanda_linia.quantitat",
							"comanda_linia.preuVenda" },
					"comanda_linia INNER JOIN producte ON comanda_linia.idProducte = producte.idProducte",
					"comanda_linia.idComanda = ?", new Object[] { comanda });
			System.out.println(construirCabeceraPedido(database, comanda));
			System.out.println("LINEAS -->\nID\tPRODUCTO\tCANTIDAD\tPRECIO");
			System.out.println(DatabaseFunctions.resultSetToString(new String[] { "producte.idProducte",
					"producte.nomProducte", "comanda_linia.quantitat", "comanda_linia.preuVenda" }, rs));
			System.out.println("IMPORTE TOTAL --> " + calcularImporteTotal(rs));
		} else {
			System.out.println("Este cliente no tiene pedidos.");
		}
	}

	private static Double calcularImporteTotal(ResultSet rs) {
		double ret = 0;
		try {
			rs.first();
			rs.previous();
			while (rs.next()) {
				ret += rs.getInt("comanda_linia.quantitat") * rs.getInt("comanda_linia.preuVenda");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	private static String construirCabeceraPedido(Database database, Integer id) {
		StringBuilder sb = new StringBuilder();

		ResultSet rs = DatabaseFunctions.selectFieldsFromWhere(database, null, "comanda", "idComanda = ?",
				new Object[] { id });
		try {
			rs.next();
			sb.append("\nID PEDIDO --> ");
			sb.append(rs.getInt("idComanda"));

			sb.append("\nFECHA PEDIDO --> ");
			sb.append(rs.getDate("dataComanda"));

			sb.append("\nFECHA ENTREGA --> ");
			sb.append(rs.getDate("dataLliurament"));

			sb.append("\nESTADO PEDIDO --> ");
			sb.append(rs.getString("estatComanda"));

			sb.append("\nPORTES --> ");
			sb.append(rs.getDouble("ports"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sb.toString();
	}

}

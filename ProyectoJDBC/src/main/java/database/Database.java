package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
	private Connection connection = null;

	public Database(String url, Properties connectionProperties) throws SQLException {
		connection = DriverManager.getConnection(url, connectionProperties);
	}

	public Connection connection() {
		return connection;
	}
}

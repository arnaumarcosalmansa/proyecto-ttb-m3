package database;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.sound.midi.SysexMessage;
import javax.swing.plaf.synth.SynthSpinnerUI;

public class DatabaseFunctions {
	public static String showTablesInfo(Database db) {
		StringBuilder sb = new StringBuilder();
		try {
			DatabaseMetaData meta = db.connection().getMetaData();

			ResultSet tableSet = meta.getTables("ttb", null, "%", null);

			while (tableSet.next()) {
				sb.append("TABLA ---> ");
				String tableName = tableSet.getString(3);
				sb.append(tableName);
				sb.append('\n');

				Statement countQuery = db.connection().createStatement();
				ResultSet countResult = countQuery.executeQuery("SELECT count(*) AS count FROM " + tableName);
				countResult.next();
				int registros = countResult.getInt("count");

				sb.append("REGITROS ---> ");
				sb.append(registros);
				sb.append('\n');

				sb.append("ESTRUCTURA ---> ");
				sb.append('\n');

				Statement ddlQuery = db.connection().createStatement();
				ResultSet ddlResult = ddlQuery.executeQuery("SELECT * FROM " + tableName + " LIMIT 1");
				ResultSetMetaData ddlData = ddlResult.getMetaData();

				for (int i = 1; i < ddlData.getColumnCount() + 1; i++) {
					sb.append(ddlData.getColumnName(i) + '\t' + ddlData.getColumnTypeName(i) + '('
							+ ddlData.getPrecision(i) + ')' + '\n');
				}

				sb.append('\n');
			}

		} catch (SQLException e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
			return null;
		}
		return sb.toString();
	}

	public static Set<String> getTables(Database db) {
		Set<String> tables = new TreeSet<String>();
		try {
			DatabaseMetaData meta = db.connection().getMetaData();

			ResultSet tableSet = meta.getTables("ttb", null, "%", null);
			while (tableSet.next()) {
				String tableName = tableSet.getString(3);
				tables.add(tableName);
			}
		} catch (Exception e) {
			// System.err.println("Error: " + e.getMessage());
		}
		return tables;
	}

	public static ResultSet selectAllFrom(Database db, String table) {
		StringBuilder sb = new StringBuilder();
		ResultSet result = null;
		try {
			Statement query = db.connection().createStatement();

			result = query.executeQuery("SELECT * FROM " + table);

			// demasiado bonito para quitarlo
			/*
			 * for(;result.next();) { for(int i = 0; i < columns.length; i++) { Object o =
			 * result.getObject(i + 1); String data = o != null ? o.toString() : "\t";
			 * sb.append(data + '\t'); } sb.append('\n'); }
			 */
		} catch (Exception e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
			return null;
		}

		return result;
	}

	public static ResultSet selectFieldsFromWhere(Database db, String[] columns, String table, String where) {
		StringBuilder sb = new StringBuilder();
		ResultSet result = null;
		try {
			Statement query = db.connection().createStatement();
			String fields = "*";

			if (columns != null && columns.length != 0) {
				fields = String.join(", ", columns);

				for (int i = 0; i < columns.length; i++) {
					sb.append(columns[i]);
					sb.append('\t');
				}
				sb.append('\n');
			}

			String sql = "SELECT " + fields + " FROM " + table;

			if (where != null && !where.isEmpty()) {
				sql += " WHERE " + where;
			}

			result = query.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}

		return result;
	}

	public static ResultSet selectFieldsFromWhere(Database db, String[] columns, String table, String where,
			Object[] whereParams) {
		StringBuilder sb = new StringBuilder();
		ResultSet result = null;
		try {

			String fields = "*";

			if (columns != null && columns.length != 0) {
				fields = String.join(", ", columns);

				for (int i = 0; i < columns.length; i++) {
					sb.append(columns[i]);
					sb.append('\t');
				}
				sb.append('\n');
			}

			String sql = "SELECT " + fields + " FROM " + table;

			if (where != null && !where.isEmpty()) {
				sql += " WHERE " + where;
			}

			PreparedStatement query = db.connection().prepareStatement(sql);

			if (whereParams != null && whereParams.length > 0) {
				int i = 1;
				for (Object o : whereParams) {
					query.setObject(i, o);
					i++;
				}
			}
			result = query.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}

		return result;
	}

	public static String resultSetColumnString(ResultSet rs) {
		StringBuilder sb = new StringBuilder();

		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int columncount = rsmd.getColumnCount();

			for (int i = 0; i < columncount; i++) {
				sb.append(rsmd.getColumnName(i + 1));
				sb.append('\t');
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}
		return sb.toString();
	}

	public static String resultSetToString(String[] columns, ResultSet result) {
		StringBuilder sb = new StringBuilder();
		try {
			result.beforeFirst();
			if (columns == null) {
				ResultSetMetaData metadata = result.getMetaData();
				columns = new String[metadata.getColumnCount()];
				for (int i = 0; i < columns.length; i++) {
					columns[i] = metadata.getColumnName(i + 1);
				}
			}

			while (result.next()) {
				for (int i = 0; i < columns.length; i++) {
					Object data = result.getObject(columns[i]);

					String dataString = data != null ? data.toString() : "NULL";
					sb.append(dataString);
					sb.append('\t');
				}
				sb.append('\n');
			}
			result.beforeFirst();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}
		return sb.toString();
	}

	public static ResultSet insertIntoValues(Database db, String table, String[] columns, Object[] values) {
		ResultSet result = null;
		try {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.length; i++) {
				sb.append("?, ");
			}
			sb.delete(sb.length() - 2, sb.length());

			String sql = "INSERT INTO " + table + "(" + String.join(", ", columns) + ") VALUES (" + sb.toString() + ")";

			PreparedStatement query = db.connection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			for (int i = 0; i < values.length; i++) {
				query.setString(i + 1, values[i].toString());
			}

			query.executeUpdate();
			result = query.getGeneratedKeys();
		} catch (Exception e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());

		}
		return result;
	}

	public static ResultSet updateValuesWhere(Database db, String table, String[] columns, Object[] values,
			String where) {
		ResultSet result = null;
		try {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < columns.length; i++) {
				sb.append(columns[i]);
				sb.append(" = ?, ");
			}
			sb.delete(sb.length() - 2, sb.length());

			String sql = "UPDATE " + table + " SET " + sb.toString() + " WHERE " + where;

			PreparedStatement query = db.connection().prepareStatement(sql);

			for (int i = 0; i < values.length; i++) {
				query.setString(i + 1, values[i].toString());
			}

			// System.out.println(query.executeUpdate());
			query.executeUpdate();
		} catch (Exception e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}
		return result;
	}

	public static Set<Integer> getIdsFromResultSet(String idName, ResultSet result) {
		Set<Integer> ids = new HashSet<Integer>();
		try {
			result.first();
			result.previous();
			while (result.next()) {
				ids.add(result.getInt(idName));
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			// System.err.println("Error: " + e.getMessage());
		}

		return ids;
	}

	public static void createInforme(Database db) throws SQLException {

		Statement tableInforme = db.connection().createStatement();

		String drop = "DROP TABLE IF EXISTS informe";

		String sql = "CREATE TABLE informe(" + "Id integer auto_increment," + "idProducte integer(11),"
				+ "nomProducte varchar(15)," + "quantitatDemanada integer," + "quantitatExistent integer,"
				+ "quantitatAFabricar integer," + "quantitatFabricada integer," + "estat integer,"
				+ "CHECK (estat BETWEEN 0 AND 1)," + "PRIMARY KEY(Id));";

		tableInforme.execute(drop);
		tableInforme.execute(sql);

		/* Esto que lo haga otro que yo la lio xD */
		String fill = "INSERT INTO informe (idProducte, nomProducte, quantitatDemanada, quantitatExistent, quantitatAFabricar, quantitatFabricada, estat)\r\n"
				+ "SELECT p.idProducte, p.nomProducte, sum(cl.quantitat), p.stock, IF(p.stock < sum(cl.quantitat), sum(cl.quantitat) - stock, 0) as quantitatAFabricar, 0, IF(IF(p.stock < sum(cl.quantitat), sum(cl.quantitat) - stock, 0) > 0, 0, 1)\r\n"
				+ "FROM producte p INNER JOIN comanda_linia cl ON p.idProducte = cl.idProducte\r\n"
				+ "GROUP BY p.idProducte\r\n";

		tableInforme.execute(fill);
	}

}
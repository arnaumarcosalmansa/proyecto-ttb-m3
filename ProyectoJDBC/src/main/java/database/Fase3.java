package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Properties;

import com.Adrian.ProyectoJDBC.Program;

public class Fase3 {

	public static void opcion5(Database database) {
		try {
			DatabaseFunctions.createInforme(database);
			System.out.println("Informe generado.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void opcion6(Database database) {
		ResultSet rs = DatabaseFunctions.selectAllFrom(database, "informe");
		if (rs != null) {
			System.out.println(DatabaseFunctions.resultSetColumnString(rs));
			System.out.println(DatabaseFunctions.resultSetToString(null, rs));
		} else {
			System.out.println("La tabla 'informe' no existe");
		}
	}

	public static void opcion7(Database db) {

		try {

			ResultSet comandas = DatabaseFunctions.selectFieldsFromWhere(db, null, "comanda",
					"UPPER(estatComanda) = 'PENDENT' ORDER BY idComanda ASC");

			while (comandas.next()) {
				try {
					int idComanda = comandas.getInt("idComanda");
					db.connection().setAutoCommit(false);
					ResultSet lineas = DatabaseFunctions.selectFieldsFromWhere(db, null, "comanda_linia",
							"idComanda = ?", new Object[] { idComanda });
					while (lineas.next()) {
						ResultSet producte = DatabaseFunctions.selectFieldsFromWhere(db, null, "producte",
								"idProducte = ?", new Object[] { lineas.getInt("idProducte") });

						// si hay producto
						if (producte.next()) {
							// saca los lotes
							ResultSet lotes = DatabaseFunctions.selectFieldsFromWhere(db, null, "producte_lot",
									"idProducte = ? ORDER BY dataCaducitat ASC",
									new Object[] { producte.getInt("idProducte") });

							int cantidad = lineas.getInt("quantitat");

							int quantitat_total = 0;
							// suma la cantidad de cada lote
							while (lotes.next() && cantidad > 0) {
								int lote_stock = lotes.getInt("quantitat");
								int lote_id = lotes.getInt("idLot");
								int cantidad_movimiento = 0;

								if (cantidad <= lote_stock) {
									cantidad_movimiento = cantidad;
									lote_stock -= cantidad;
									cantidad = 0;
									DatabaseFunctions.updateValuesWhere(db, "producte_lot",
											new String[] { "quantitat" }, new Object[] { lote_stock },
											"idLot = " + lote_stock);
								} else {
									cantidad_movimiento = lote_stock;
									cantidad -= lote_stock;
									DatabaseFunctions.updateValuesWhere(db, "producte_lot",
											new String[] { "quantitat" }, new Object[] { 0 }, "idLot = " + lote_stock);
								}

								DatabaseFunctions.insertIntoValues(db, "diari_moviments",
										new String[] { "idProducte", "tipusMoviment", "quantitat", "observacions",
												"idComanda", "idLot" },
										new Object[] { lineas.getInt("idProducte"), "S", cantidad_movimiento,
												"Restando stock a lote", idComanda, lote_id });
							}

							if (cantidad > 0) {
								int producte_stock = producte.getInt("stock");
								if (producte_stock >= cantidad) {
									int movimiento = cantidad;
									int producte_id = producte.getInt("idProducte");
									producte_stock -= cantidad;
									cantidad = 0;
									DatabaseFunctions.updateValuesWhere(db, "producte", new String[] { "stock" },
											new Object[] { producte_stock }, "idProducte = " + producte_id);

									DatabaseFunctions.insertIntoValues(db, "diari_moviments",
											new String[] { "idComanda", "idProducte", "quantitat", "tipusMoviment",
													"observacions" },
											new Object[] { comandas.getInt("idComanda"), producte.getInt("idProducte"),
													movimiento, "S", "Restando stock al producto." });
								}
							}

							if (cantidad > 0) {
								throw new NoSuchElementException("No hay cantidad suficiente.");
							}

						}
					}
					// actualizamos el estado de la comanda
					DatabaseFunctions.updateValuesWhere(db, "comanda", new String[] { "estatComanda" },
							new Object[] { "PREPARADA" }, "idComanda = " + comandas.getInt("idComanda"));

					db.connection().commit();
					System.out.println("Pedido preparado " + comandas.getInt("idComanda"));
				} catch (SQLException | NoSuchElementException e) {
					db.connection().rollback();
					System.out.println("No se a realizado el pedido " + comandas.getInt("idComanda"));
					// e.printStackTrace();
				} finally {
					db.connection().setAutoCommit(true);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void opcion8(Database database) {
		String sql = "INSERT INTO ordre_compra (idProducte, idProveidor, quantitat, dataOrdre, dataRecepcio, estatOrdre)\r\n"
				+ "SELECT idProducte, idProveidor, stockMinim * 2, CURRENT_DATE, '0000-00-00', 'PENDENT'\r\n"
				+ "FROM producte\r\n" + "WHERE tipusProducte = 'INGREDIENT' AND stock < stockMinim;";
		try {
			database.connection().createStatement().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void opcion9(Database database) {
		ResultSet rs = DatabaseFunctions.selectFieldsFromWhere(database, null, "ordre_compra",
				"1 = 1 ORDER BY idProveidor,idProducte");
		System.out.println(DatabaseFunctions.resultSetColumnString(rs));
		System.out.println(DatabaseFunctions.resultSetToString(null, rs));
	}

	public static void opcion10(Database database) {
		ResultSet rs = DatabaseFunctions.selectAllFrom(database, "diari_moviments");
		System.out.println(DatabaseFunctions.resultSetColumnString(rs));
		System.out.println(DatabaseFunctions.resultSetToString(null, rs));
	}

}

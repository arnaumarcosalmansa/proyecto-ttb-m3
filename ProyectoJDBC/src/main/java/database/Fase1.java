package database;

import java.sql.ResultSet;
import java.util.Scanner;
import java.util.Set;

import com.Adrian.ProyectoJDBC.Program;

public class Fase1 {

	public static void opcion2(Scanner input, Database database) {
		ResultSet rs = null;
		System.out.println("Tablas disponibles:");

		Set<String> tablas = DatabaseFunctions.getTables(database);
		for (String s : tablas)
			System.out.println(' ' + s);

		System.out.print("\nEscriu el nom de la taula: ");
		String name = input.next();

		if (tablas.contains(name)) {
			rs = DatabaseFunctions.selectAllFrom(database, name);
			System.out.println(DatabaseFunctions.resultSetColumnString(rs));
			System.out.println(DatabaseFunctions.resultSetToString(null, rs));
		} else {
			System.out.println("\n¡La tabla no existe!\n");
		}
	}
}

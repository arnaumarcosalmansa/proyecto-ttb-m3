package com.Adrian.ProyectoJDBC;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import database.Database;
import database.DatabaseFunctions;
import database.Fase1;
import database.Fase2;
import database.Fase3;

public class Program {
	public static Database database;
	public static final Scanner input = new Scanner(System.in);

	private static String url = "jdbc:mysql://localhost:3306/ttb?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&zeroDateTimeBehavior=convertToNull";

	public static void main(String[] args) {
		Properties credenciales = new Properties();
		credenciales.put("user", "root");
		credenciales.put("password", "");

		try {
			database = new Database(url, credenciales);
			database.connection().createStatement()
					.execute("ALTER TABLE diari_moviments MODIFY COLUMN idOrdre INT(11);");
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.exit(1);
		}

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "");

		int seleccionado = 0;
		do {
			displayMenu();
			try {
				seleccionado = input.nextInt();
			} catch (Exception e) {
				input.nextLine();
				seleccionado = -1;
			}

			switch (seleccionado) {
			case 1:
				System.out.println(DatabaseFunctions.showTablesInfo(database));
				break;
			case 2:
				Fase1.opcion2(input, database);
				break;
			case 3:
				Fase2.opcion3(input, database);
				break;
			case 4:
				Fase2.opcion4(input, database);
				break;
			case 5:
				Fase3.opcion5(database);
				break;
			case 6:
				Fase3.opcion6(database);
				break;
			case 7:
				Fase3.opcion7(database);
				break;
			case 8:
				Fase3.opcion8(database);
				break;
			case 9:
				Fase3.opcion9(database);
				break;
			case 10:
				Fase3.opcion10(database);
				break;
			default:
				break;
			}
		} while (seleccionado != 0);
	}

	public static void displayMenu() {
		System.out.println(
				"1) Ver tabla y columnas\n2) Ver datos\n3) Generar pedido\n4) Ver pedidos de cliente\n5) Generar informe de situación\n6) Ver informe de situación\n7) Preparar pedidos\n8) Generar orden de compra\n9) Ver ordenes de compra\n10) Ver diario de movimientos\n0) Salir");
	}

}

class Producto {
	int id;
	int stock;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id != other.id)
			return false;
		return true;
	}
}

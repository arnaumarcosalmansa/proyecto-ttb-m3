-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Temps de generació: 13-12-2018 a les 19:17:24
-- Versió del servidor: 10.1.37-MariaDB
-- Versió de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `ttb`
--
CREATE DATABASE IF NOT EXISTS `ttb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ttb`;

-- --------------------------------------------------------

--
-- Estructura de la taula `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `nomClient` varchar(15) NOT NULL,
  `actiu` tinyint(1) NOT NULL DEFAULT '1',
  `idDireccio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClient`),
  KEY `c_Direc` (`idDireccio`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `client`
--

INSERT INTO `client` (`idClient`, `nomClient`, `actiu`, `idDireccio`) VALUES
(1, 'La Canasta', 1, 1),
(2, 'Baires', 1, 2),
(3, 'Pierre Herme', 1, 3),
(4, 'Aux Pains Papy', 1, 4),
(5, 'La Santiaguesa', 1, 5),
(6, 'SantAntoni', 1, 6),
(7, 'MIX', 1, 7),
(8, 'La Flor Negra', 1, 8),
(9, 'Bead Barn', 1, 9),
(10, 'Smeterling Pati', 1, 10),
(11, 'Es Ruiz', 1, 11);

-- --------------------------------------------------------

--
-- Estructura de la taula `comanda`
--

DROP TABLE IF EXISTS `comanda`;
CREATE TABLE IF NOT EXISTS `comanda` (
  `idComanda` int(11) NOT NULL AUTO_INCREMENT,
  `idClient` int(11) NOT NULL,
  `dataComanda` date NOT NULL,
  `dataLliurament` date DEFAULT NULL,
  `estatComanda` enum('PENDENT','PREPARADA','TRANSPORT','LLIURADA') NOT NULL DEFAULT 'PENDENT',
  `ports` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idComanda`),
  KEY `fk_client` (`idClient`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `comanda`
--

INSERT INTO `comanda` (`idComanda`, `idClient`, `dataComanda`, `dataLliurament`, `estatComanda`, `ports`) VALUES
(1, 1, '2018-12-15', NULL, 'PENDENT', '0'),
(2, 1, '2018-12-16', NULL, 'PENDENT', '0'),
(3, 2, '2018-12-17', NULL, 'PENDENT', '0'),
(4, 2, '2018-12-17', NULL, 'PENDENT', '0'),
(5, 2, '2018-12-18', NULL, 'PENDENT', '0'),
(6, 2, '2018-12-18', NULL, 'PENDENT', '0'),
(7, 3, '2018-12-01', NULL, 'PENDENT', '0'),
(8, 3, '2018-12-11', NULL, 'PENDENT', '0'),
(9, 4, '2018-12-12', NULL, 'PENDENT', '0'),
(10, 4, '2018-12-11', NULL, 'PENDENT', '0'),
(11, 4, '2018-12-12', NULL, 'PENDENT', '0'),
(12, 5, '2018-12-12', NULL, 'PENDENT', '0'),
(13, 5, '2018-12-21', NULL, 'PENDENT', '0'),
(14, 5, '2018-12-20', NULL, 'PENDENT', '0'),
(15, 5, '2018-12-10', NULL, 'PENDENT', '0'),
(16, 6, '2018-12-11', NULL, 'PENDENT', '0'),
(17, 7, '2018-12-12', NULL, 'PENDENT', '0'),
(18, 7, '2018-12-13', NULL, 'PENDENT', '0'),
(19, 8, '2018-12-11', NULL, 'PENDENT', '0'),
(20, 8, '2018-12-12', NULL, 'PENDENT', '0'),
(21, 8, '2018-12-13', NULL, 'PENDENT', '0'),
(22, 9, '2018-12-14', NULL, 'PENDENT', '0'),
(23, 11, '2018-12-15', NULL, 'PENDENT', '0'),
(24, 11, '2018-12-16', NULL, 'PENDENT', '0');

-- --------------------------------------------------------

--
-- Estructura de la taula `comanda_linia`
--

DROP TABLE IF EXISTS `comanda_linia`;
CREATE TABLE IF NOT EXISTS `comanda_linia` (
  `idComanda` int(11) NOT NULL,
  `idLinia` int(11) NOT NULL,
  `idProducte` int(11) NOT NULL,
  `quantitat` int(11) NOT NULL,
  `preuVenda` decimal(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idComanda`,`idLinia`),
  KEY `fk_producteLC` (`idProducte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `comanda_linia`
--

INSERT INTO `comanda_linia` (`idComanda`, `idLinia`, `idProducte`, `quantitat`, `preuVenda`) VALUES
(1, 1, 2, 40, '1500.00'),
(1, 2, 2, 1, '0.00'),
(1, 3, 10, 10, '1800.00'),
(2, 1, 10, 60, '1500.00'),
(2, 2, 12, 40, '1000.00'),
(3, 1, 12, 100, '1000.00'),
(4, 1, 12, 100, '1000.00'),
(4, 2, 12, 100, '5.00'),
(4, 3, 2, 1, '0.00'),
(5, 1, 13, 20, '1000.00'),
(6, 1, 13, 10, '1400.00'),
(6, 2, 2, 10, '1500.00'),
(6, 3, 10, 10, '1800.00'),
(6, 4, 12, 10, '1000.00'),
(7, 1, 3, 40, '1500.00'),
(7, 2, 2, 1, '0.00'),
(7, 3, 10, 10, '1800.00'),
(8, 1, 3, 24, '1500.00'),
(8, 2, 12, 24, '1000.00'),
(9, 1, 12, 10, '1000.00'),
(10, 1, 12, 10, '1000.00'),
(10, 2, 12, 30, '5.00'),
(10, 3, 2, 1, '0.00'),
(11, 1, 13, 20, '1000.00'),
(12, 1, 13, 1, '1400.00'),
(12, 2, 2, 1, '1500.00'),
(12, 3, 10, 1, '1800.00'),
(12, 4, 12, 10, '1000.00'),
(13, 1, 2, 12, '1500.00'),
(13, 2, 2, 1, '0.00'),
(13, 3, 10, 10, '1800.00'),
(14, 1, 10, 16, '1500.00'),
(14, 2, 12, 12, '1000.00'),
(15, 1, 12, 10, '1000.00'),
(16, 1, 12, 10, '1000.00'),
(16, 2, 12, 13, '5.00'),
(16, 3, 2, 1, '0.00'),
(17, 1, 13, 20, '1000.00'),
(18, 1, 13, 10, '1400.00'),
(18, 2, 2, 10, '1500.00'),
(18, 3, 10, 10, '1800.00'),
(18, 4, 12, 10, '1000.00'),
(19, 1, 3, 40, '1500.00'),
(19, 2, 2, 1, '0.00'),
(19, 3, 10, 10, '1800.00'),
(20, 1, 3, 6, '1500.00'),
(20, 2, 12, 4, '1000.00'),
(21, 1, 12, 10, '1000.00'),
(22, 1, 12, 10, '1000.00'),
(22, 2, 12, 13, '5.00'),
(22, 3, 2, 1, '0.00'),
(23, 1, 13, 20, '1000.00'),
(24, 1, 13, 1, '1400.00'),
(24, 2, 2, 15, '1500.00'),
(24, 3, 10, 15, '1800.00'),
(24, 4, 12, 15, '1000.00');

-- --------------------------------------------------------

--
-- Estructura de la taula `diari_moviments`
--

DROP TABLE IF EXISTS `diari_moviments`;
CREATE TABLE IF NOT EXISTS `diari_moviments` (
  `idMoviment` int(11) DEFAULT NULL,
  `moment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idProducte` int(11) NOT NULL,
  `tipusMoviment` enum('E','S') NOT NULL,
  `quantitat` int(11) NOT NULL,
  `observacions` varchar(50) NOT NULL,
  `idComanda` int(11) NOT NULL,
  `idOrdre` int(11) NOT NULL,
  `idLot` int(11) NOT NULL,
  KEY `fk_productDM` (`idProducte`),
  KEY `fk_comandaDM` (`idComanda`),
  KEY `fk_ordreDM` (`idOrdre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de la taula `direccio`
--

DROP TABLE IF EXISTS `direccio`;
CREATE TABLE IF NOT EXISTS `direccio` (
  `idDireccio` int(11) NOT NULL AUTO_INCREMENT,
  `direccio` varchar(50) NOT NULL,
  `poblacio` varchar(25) NOT NULL,
  `pais` varchar(15) NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  PRIMARY KEY (`idDireccio`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `direccio`
--

INSERT INTO `direccio` (`idDireccio`, `direccio`, `poblacio`, `pais`, `latitud`, `longitud`) VALUES
(1, 'Pastora 8, 29005', 'Málaga', 'Espanya', 39.1174353, -5.7933869),
(2, 'Plaza de Còrdova, 9 08003', 'Sabadell', 'Espanya', 41.5442476, 2.0604163),
(3, '72 Rue Bonaparete', 'París', 'França', 41.5442476, 2.0604163),
(4, '279 Grays Inn Road', 'Londres', 'Gran Bretanya', 51.5293753, -0.1903852),
(5, 'Calle Mayor 73 28001', 'Madrid', 'Espanya', 40.9284811, -5.2618384),
(6, 'Tajo 60', 'Barcelona', 'Espanya', 41.4830445, 2.0503166),
(7, 'Av. Iberia, 1', 'Sant Quirze', 'Espanya', 41.5525456, 2.0682696),
(8, 'Calle Larios', 'Málaga', 'Espanya', 39.0492284, -5.6562213),
(9, 'Gross Plass', 'Berlin', 'Alemanya', 0.2041477, -10.7433573),
(10, 'Major Street', 'Buenos Aires', 'Argentina', -34.59267, -58.3898747),
(11, 'Plaza Grande Pibe', 'Buenos Aires', 'Argentina', -34.6254113, -58.4426792);

-- --------------------------------------------------------

--
-- Estructura de la taula `ordre_compra`
--

DROP TABLE IF EXISTS `ordre_compra`;
CREATE TABLE IF NOT EXISTS `ordre_compra` (
  `idOrdre` int(11) NOT NULL AUTO_INCREMENT,
  `idProducte` int(11) NOT NULL,
  `idProveidor` int(11) NOT NULL,
  `quantitat` int(11) NOT NULL,
  `dataOrdre` date NOT NULL,
  `dataRecepcio` date NOT NULL,
  `estatOrdre` enum('PENDENT','REBUDA','','') NOT NULL DEFAULT 'PENDENT',
  PRIMARY KEY (`idOrdre`),
  KEY `fk_producteO` (`idProducte`),
  KEY `fk_proveidorO` (`idProveidor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de la taula `producte`
--

DROP TABLE IF EXISTS `producte`;
CREATE TABLE IF NOT EXISTS `producte` (
  `idProducte` int(11) NOT NULL AUTO_INCREMENT,
  `nomProducte` varchar(15) NOT NULL,
  `descripcioProducte` varchar(50) NOT NULL,
  `preuVenda` decimal(10,0) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `stockMinim` int(11) NOT NULL DEFAULT '0',
  `tipusProducte` enum('VENDIBLE','INGREDIENT') NOT NULL DEFAULT 'VENDIBLE',
  `unitatMesura` enum('UNITAT','GRAM','LITRE') NOT NULL DEFAULT 'UNITAT',
  `idProveidor` int(11) DEFAULT NULL,
  `tempsFabricacio` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProducte`),
  KEY `fk_proveidor` (`idProveidor`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `producte`
--

INSERT INTO `producte` (`idProducte`, `nomProducte`, `descripcioProducte`, `preuVenda`, `stock`, `stockMinim`, `tipusProducte`, `unitatMesura`, `idProveidor`, `tempsFabricacio`) VALUES
(2, 'pLiviano', 'pastelito liviano ligerito ligerito', '1500', 0, 12, 'VENDIBLE', 'UNITAT', NULL, 30),
(3, 'sucre', 'sucre blanca', '0', 0, 10000, 'INGREDIENT', 'GRAM', 3, 0),
(4, 'ous', '', '0', 0, 240, 'INGREDIENT', 'UNITAT', 2, 0),
(5, 'farina', '', '0', 0, 30000, 'INGREDIENT', 'GRAM', 3, 0),
(6, 'llevadura', '', '0', 0, 5000, 'INGREDIENT', 'GRAM', 2, 0),
(7, 'secret', 'El nostre toc de distinció', '0', 100, 100, 'INGREDIENT', 'UNITAT', 4, 0),
(8, 'nabius', '', '0', 0, 100000, 'INGREDIENT', 'GRAM', 3, 0),
(9, 'llimona', '', '0', 0, 4000, 'INGREDIENT', 'GRAM', 2, 0),
(10, 'pLlimona', 'pastis de llimona rico rico', '0', 1800, 6, 'VENDIBLE', 'UNITAT', NULL, 24),
(11, 'albahaca', '', '0', 0, 600, 'INGREDIENT', 'GRAM', 3, 0),
(12, 'pVelvet', 'pastis Velvet', '1000', 0, 20, 'VENDIBLE', 'UNITAT', NULL, 40),
(13, 'pErizo', 'pastis Erizo', '1400', 50, 20, 'VENDIBLE', 'UNITAT', NULL, 40),
(14, 'mantequilla', 'mantequilla', '0', 0, 2000, 'INGREDIENT', 'GRAM', 1, 0),
(15, 'cacau', 'cacau en pols', '0', 0, 2000, 'INGREDIENT', 'GRAM', 1, 0),
(16, 'cafe', 'cafe en pols', '0', 0, 2000, 'INGREDIENT', 'GRAM', 2, 0),
(17, 'xocolata', 'xocolata en pols', '0', 0, 2000, 'INGREDIENT', 'GRAM', 2, 0),
(18, 'margarina', 'margarina', '0', 0, 2000, 'INGREDIENT', 'GRAM', 2, 0),
(19, 'vainilla', 'vainilla', '0', 0, 1000, 'INGREDIENT', 'GRAM', 3, 0),
(20, 'vinagresidra', 'vinagre de sidra', '0', 0, 2000, 'INGREDIENT', 'LITRE', 3, 0),
(21, 'formatge', 'formatge', '0', 0, 1000, 'INGREDIENT', 'GRAM', 3, 0),
(22, 'bicarbonat', 'bicarbonat sodi', '0', 0, 1000, 'INGREDIENT', 'GRAM', 3, 0),
(23, 'surollet', 'suro de llet', '0', 0, 1000, 'INGREDIENT', 'LITRE', 1, 0),
(24, 'sal', 'sal marina', '0', 0, 5000, 'INGREDIENT', 'GRAM', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de la taula `producte_composicio`
--

DROP TABLE IF EXISTS `producte_composicio`;
CREATE TABLE IF NOT EXISTS `producte_composicio` (
  `idProducte` int(11) NOT NULL,
  `idComponent` int(11) NOT NULL,
  `quantitat` int(11) NOT NULL,
  PRIMARY KEY (`idProducte`,`idComponent`),
  KEY `fk_component` (`idComponent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `producte_composicio`
--

INSERT INTO `producte_composicio` (`idProducte`, `idComponent`, `quantitat`) VALUES
(2, 3, 115),
(2, 4, 4),
(2, 5, 115),
(2, 6, 10),
(2, 7, 1),
(7, 8, 100),
(10, 4, 4),
(10, 6, 8),
(10, 7, 1),
(10, 9, 100),
(10, 11, 20),
(12, 3, 500),
(12, 4, 2),
(12, 5, 250),
(12, 7, 2),
(12, 14, 225),
(12, 15, 15),
(12, 19, 30),
(12, 20, 100),
(12, 21, 230),
(12, 22, 170),
(12, 23, 240),
(12, 24, 30),
(13, 3, 170),
(13, 4, 3),
(13, 5, 130),
(13, 7, 1),
(13, 14, 170),
(13, 15, 40),
(13, 16, 50),
(13, 17, 120),
(13, 18, 150);

-- --------------------------------------------------------

--
-- Estructura de la taula `producte_lot`
--

DROP TABLE IF EXISTS `producte_lot`;
CREATE TABLE IF NOT EXISTS `producte_lot` (
  `idLot` int(11) NOT NULL AUTO_INCREMENT,
  `idProducte` int(11) NOT NULL,
  `dataEntrada` date NOT NULL,
  `dataCaducitat` date NOT NULL,
  `quantitat` int(11) NOT NULL,
  PRIMARY KEY (`idLot`),
  KEY `fk_producteL` (`idProducte`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `producte_lot`
--

INSERT INTO `producte_lot` (`idLot`, `idProducte`, `dataEntrada`, `dataCaducitat`, `quantitat`) VALUES
(1, 3, '2018-11-15', '2018-12-31', 40000),
(2, 3, '2018-11-22', '2019-01-31', 30000),
(3, 3, '2018-11-26', '2020-01-31', 70000),
(4, 4, '2018-11-24', '2019-12-20', 480),
(5, 6, '2018-11-24', '2019-12-31', 200),
(6, 6, '2018-11-24', '2019-12-01', 0),
(7, 6, '2018-11-24', '2019-12-01', 400),
(8, 6, '2018-11-22', '2019-01-31', 100),
(9, 9, '2018-11-22', '2019-01-31', 2000),
(10, 9, '2018-11-15', '2018-12-01', 20000),
(11, 14, '2019-11-15', '2020-12-31', 40000),
(12, 14, '2018-11-22', '2019-01-31', 30000),
(13, 15, '2019-11-26', '2020-01-31', 70000),
(14, 15, '2018-11-24', '2018-12-20', 480),
(15, 16, '2019-11-24', '2020-12-31', 200),
(16, 16, '2018-11-24', '2019-01-01', 0),
(17, 17, '2019-11-24', '2020-12-01', 400),
(18, 17, '2018-11-22', '2019-01-31', 100),
(19, 18, '2019-11-22', '2019-01-31', 2000),
(20, 18, '2018-11-15', '2019-12-01', 20000),
(21, 19, '2019-11-15', '2019-12-31', 40000),
(22, 19, '2018-11-22', '2019-01-31', 30000),
(23, 20, '2019-11-26', '2020-01-31', 70000),
(24, 20, '2018-11-24', '2019-12-20', 480),
(25, 21, '2019-11-24', '2018-12-31', 200),
(26, 21, '2018-11-24', '2019-12-01', 0),
(27, 22, '2019-11-24', '2019-12-01', 400),
(28, 23, '2019-11-22', '2019-01-31', 100),
(29, 23, '2019-11-22', '2019-01-31', 2000),
(30, 24, '2019-11-15', '2019-12-01', 20000);

-- --------------------------------------------------------

--
-- Estructura de la taula `proveidor`
--

DROP TABLE IF EXISTS `proveidor`;
CREATE TABLE IF NOT EXISTS `proveidor` (
  `idProveidor` int(11) NOT NULL AUTO_INCREMENT,
  `nomProveidor` varchar(15) NOT NULL,
  `idDireccio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProveidor`),
  KEY `fk_direccioP` (`idDireccio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `proveidor`
--

INSERT INTO `proveidor` (`idProveidor`, `nomProveidor`, `idDireccio`) VALUES
(1, 'UNO', NULL),
(2, 'DOS', NULL),
(3, 'TRES', NULL),
(4, 'QUATRE', NULL);

-- --------------------------------------------------------

--
-- Estructura de la taula `tarifa`
--

DROP TABLE IF EXISTS `tarifa`;
CREATE TABLE IF NOT EXISTS `tarifa` (
  `desde` int(11) NOT NULL,
  `fins` int(11) NOT NULL,
  `preu` decimal(10,2) NOT NULL,
  PRIMARY KEY (`desde`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `tarifa`
--

INSERT INTO `tarifa` (`desde`, `fins`, `preu`) VALUES
(0, 100, '300.00'),
(101, 300, '500.00'),
(301, 600, '1000.00'),
(801, 2000, '2000.00'),
(2001, 3000, '2500.00'),
(3001, 10000, '3500.00'),
(10001, 50000, '5000.00');

--
-- Restriccions per a les taules bolcades
--

--
-- Restriccions per a la taula `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `c_Direc` FOREIGN KEY (`idDireccio`) REFERENCES `direccio` (`idDireccio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restriccions per a la taula `comanda`
--
ALTER TABLE `comanda`
  ADD CONSTRAINT `fk_client` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`);

--
-- Restriccions per a la taula `comanda_linia`
--
ALTER TABLE `comanda_linia`
  ADD CONSTRAINT `fk_comanda` FOREIGN KEY (`idComanda`) REFERENCES `comanda` (`idComanda`),
  ADD CONSTRAINT `fk_producteLC` FOREIGN KEY (`idProducte`) REFERENCES `producte` (`idProducte`);

--
-- Restriccions per a la taula `diari_moviments`
--
ALTER TABLE `diari_moviments`
  ADD CONSTRAINT `fk_comandaDM` FOREIGN KEY (`idComanda`) REFERENCES `comanda` (`idComanda`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ordreDM` FOREIGN KEY (`idOrdre`) REFERENCES `ordre_compra` (`idOrdre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productDM` FOREIGN KEY (`idProducte`) REFERENCES `producte` (`idProducte`);

--
-- Restriccions per a la taula `ordre_compra`
--
ALTER TABLE `ordre_compra`
  ADD CONSTRAINT `fk_producteO` FOREIGN KEY (`idProducte`) REFERENCES `producte` (`idProducte`),
  ADD CONSTRAINT `fk_proveidorO` FOREIGN KEY (`idProveidor`) REFERENCES `proveidor` (`idProveidor`);

--
-- Restriccions per a la taula `producte`
--
ALTER TABLE `producte`
  ADD CONSTRAINT `fk_proveidor` FOREIGN KEY (`idProveidor`) REFERENCES `proveidor` (`idProveidor`);

--
-- Restriccions per a la taula `producte_composicio`
--
ALTER TABLE `producte_composicio`
  ADD CONSTRAINT `fk_component` FOREIGN KEY (`idComponent`) REFERENCES `producte` (`idProducte`),
  ADD CONSTRAINT `fk_producte` FOREIGN KEY (`idProducte`) REFERENCES `producte` (`idProducte`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restriccions per a la taula `producte_lot`
--
ALTER TABLE `producte_lot`
  ADD CONSTRAINT `fk_producteL` FOREIGN KEY (`idProducte`) REFERENCES `producte` (`idProducte`);

--
-- Restriccions per a la taula `proveidor`
--
ALTER TABLE `proveidor`
  ADD CONSTRAINT `fk_direccioP` FOREIGN KEY (`idDireccio`) REFERENCES `direccio` (`idDireccio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

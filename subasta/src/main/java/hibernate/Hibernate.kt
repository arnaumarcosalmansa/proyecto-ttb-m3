package hibernate

import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.service.ServiceRegistry
import org.hibernate.boot.MetadataSources
import org.hibernate.boot.registry.StandardServiceRegistryBuilder

class Hibernate
{
	companion object
	{
		var session: Session? = null
		var serviceRegistry: ServiceRegistry? = null
		var metadataSources: MetadataSources? = null
		
		var sessionFactory: SessionFactory? = null
		get() {
			field = field ?: createSessionFactory()
			
			println(field)
			
			return field
		}
		
		private fun createSessionFactory() : SessionFactory
		{
			serviceRegistry = StandardServiceRegistryBuilder()
	                .configure("hibernate.cfg.xml")
	                .build()
	             
			metadataSources = MetadataSources(serviceRegistry)
			val metadata = metadataSources!!.getMetadataBuilder().build()
				
			return metadata.getSessionFactoryBuilder()
        		//.applyInterceptor(LoggingInterceptor())
        		.build()
		}
	}
}
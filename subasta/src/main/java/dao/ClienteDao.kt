package dao

import dataobjects.Cliente

class ClienteDao : GenericDao<Cliente, String>(), IClienteDao
{
	public fun loadByNombre(nombre : String) : Cliente?
	{
		val session = sessionFactory.getCurrentSession();
		session.beginTransaction()
		val sqlstring = "FROM Cliente WHERE nombre = :userName"
        val query = session.createQuery(sqlstring)
        query.setString("userName",nombre);
        val results = query.list(); 
		
		session.transaction.commit()
		
		return if (results.size > 0) (results[0] as Cliente) else null
	}
}
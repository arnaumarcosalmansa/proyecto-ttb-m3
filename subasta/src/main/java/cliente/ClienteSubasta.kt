package cliente

import java.net.Socket
import java.io.ObjectOutputStream
import java.io.ObjectInputStream
import messages.Message
import messages.login.RequestLoginMessage
import java.util.Scanner
import messages.LoginMessage
import messages.subasta.UpdateFullStatusMessage
import messages.login.LoginAnswerMessage
import java.util.concurrent.Semaphore
import messages.subasta.StartSubastaMessage
import servidor.Subasta
import messages.subasta.PujaMessage
import dataobjects.Licitacion
import dataobjects.Cliente
import arnau.com.subasta.Config
import messages.subasta.UpdateStatusMessage
import messages.subasta.PujaRechazadaMessage
import messages.subasta.EndSubasta
import messages.EchoMessage
import java.net.ConnectException

class ClienteSubasta(val connection: Socket) : Thread() {

	var cliente: Cliente? = null
	val mutexCliente = Semaphore(1, true)

	val oos = ObjectOutputStream(connection.outputStream)
	val ois = ObjectInputStream(connection.inputStream)

	val input = Scanner(System.`in`)

	val networkListener = ClientNetworkReader(this)
	val networkWriter = ClientNetworkWriter(this)

	val colaDeAcciones = mutableListOf<Acciones>()
	val semaforoColaDeAcciones = Semaphore(1, true)

	var logged = false;
	var running = true

	var subasta: Subasta? = null

	init {
		networkListener.start()
		networkWriter.start()
	}

	override public fun run() {
		while (running) {
			semaforoColaDeAcciones.acquire()
			if (!colaDeAcciones.isEmpty()) {
				val accion = colaDeAcciones.removeAt(0)
				semaforoColaDeAcciones.release()
				processaAccion(accion)
			} else {
				semaforoColaDeAcciones.release()
			}
		}
	}


	private fun processaAccion(accion: Acciones) {
		when (accion) {
			Acciones.PEDIR_LOGIN -> {
				pedirLogin()
			}
			Acciones.ESCUCHAR_TECLADO -> {
				escucharTeclado()
				semaforoColaDeAcciones.acquire()
				colaDeAcciones.add(Acciones.ESCUCHAR_TECLADO)
				semaforoColaDeAcciones.release()
			}
		}
	}

	private fun pedirLogin() {
		print("Usuario: ")
		val usuario = input.nextLine()

		print("Contraseña: ")
		val console = System.console()

		var contrasena = ""
		console?.let {
			contrasena = console.readPassword().toString()
		} ?: run {
			contrasena = input.nextLine()
		}
		networkWriter.ponerEnCola(LoginMessage(usuario, contrasena))
	}

	private fun escucharTeclado() {
		val linea = input.nextLine()
		val splitted = linea.split(" ")
		if (splitted.size == 0) {
			return
		}

		when (splitted[0].toLowerCase()) {
			"pujar" -> {
				pujar(splitted)
			}
			"logout" -> {
				logout(splitted)
			}
			"saldo" -> {
				mutexCliente.acquire()
				printSystem("Tu saldo es de " + this.cliente!!.saldoActual)
				mutexCliente.release()
			}
			"help" -> {
				println("\n--------- Commandas del Sistema ---------")
				printSystem(
					"pujar => permite al usuario pujar en una subasta,\n" +
							"ej: pujar 'cantidad', pujar +'cantidad'."
				)
				printSystem(
					"saldo => permite al usuario ver su saldo actual,\n" +
							"ej: saldo."
				)
				printSystem(
					"logout => permite al usuario cerrar sesion,\n" +
							"ej: logout."
				)
				println("------- Fin Commandas del Sistema -------\n")
			}
		}
	}

	public fun processLoginAnswer(answer: LoginAnswerMessage) {
		if (answer.status == 0) {

			mutexCliente.acquire()
			this.cliente = answer.cliente
			mutexCliente.release()

			printSystem("Login aceptado...")
			semaforoColaDeAcciones.acquire()
			colaDeAcciones.add(Acciones.ESCUCHAR_TECLADO)
			semaforoColaDeAcciones.release()
			logged = true
		} else {
			printSystem("Login incorrecto...")
		}
	}

	public fun processRequestLogin(request: RequestLoginMessage) {
		semaforoColaDeAcciones.acquire()
		colaDeAcciones.add(Acciones.PEDIR_LOGIN)
		semaforoColaDeAcciones.release()
	}

	public fun processStartSubasta(message: StartSubastaMessage) {
		val lote = message.lote
		printSystem("Empieza la subasta de un lote de ${lote.producto?.nombre},\n empezando por un precio de ${lote.precioSalida}.")
		subasta = Subasta(lote)
	}

	public fun processUpdateFullStatus(message: UpdateFullStatusMessage) {
		val lote = message.subasta.lote
		printSystem("Se está subastando un lote de ${lote.producto?.nombre}, que ha empezado por un precio de ${lote.precioSalida}.")
		val licitaciones = message.subasta.licitaciones
		printSystem("Licitaciones: ")
		for (lic in licitaciones) {
			printSystem("\t${lic.cliente?.nombre} --> ${lic.cantidad}")
		}

		if (licitaciones.isEmpty()) {
			printSystem("\tNo hay licitaciones todavía")
		}

		this.subasta = message.subasta
	}

	public fun processUpdateStatus(message: UpdateStatusMessage) {
		val puja = message.puja
		subasta?.licitaciones?.add(puja) ?: run { println("error") }
		printSystem("Ultima puja de ${puja.cantidad}, por el usuario ${puja.cliente?.nombre}, el ${puja.momento}.")
	}

	public fun processPujaRechazada(message: PujaRechazadaMessage) {
		printSystem("PUJA RECHAZADA\nRazón: ${message.razon}.")
	}

	public fun processEndSubasta(message: EndSubasta) {
		printSystem("FINAL DE LA SUBASTA")
		message.pujaGanadora?.let {
			printSystem("Ha ganado la puja el usuario ${message.pujaGanadora.cliente?.nombre} por ${message.pujaGanadora.cantidad}.")
			if (message.pujaGanadora.cliente?.nombre == this.cliente!!.nombre) {
				mutexCliente.acquire()
				this.cliente!!.saldoActual += -message.pujaGanadora.cantidad
				mutexCliente.release()
			}
		} ?: run {
			printSystem("No ha habido pujas.")
		}
	}

	private fun updateFullStatus(message: UpdateFullStatusMessage) {
		val lote = message.subasta.lote

		printSystem("Subastando lote de ${lote.producto?.nombre}.")

		printSystem("Licitaciones:")

		val licitaciones = message.subasta.licitaciones

		licitaciones.forEach {
			printSystem("    ${it.cliente?.nombre} --> ${it.cantidad}")
		}
	}

	private fun pujar(args: List<String>) {
		if (args.size < 2) {
			printSystem("Puja invalida...")
			return
		}

		subasta?.let {
			var valorPuja = 0;
			if (args[1].startsWith("+")) {
				if (subasta?.licitaciones?.isEmpty() ?: true) {
					valorPuja = subasta?.lote?.precioSalida ?: 0
				} else {
					valorPuja = subasta?.licitaciones?.last()?.cantidad ?: 0
				}
				valorPuja += args[1].substring(1).toInt()
			} else {
				valorPuja = args[1].toInt()
			}

			val puja = Licitacion()
			puja.cantidad = valorPuja

			printSystem("Pujando...")

			mutexCliente.acquire()
			networkWriter.ponerEnCola(PujaMessage(this.cliente!!, valorPuja))
			mutexCliente.release()
		} ?: run {
			printSystem("No hoy subasta")
		}
	}

	private fun logout(args: List<String>) {
		printSystem("Adiós...")
		running = false
		this.connection.close()
	}

	public fun processEchoMessage(message: EchoMessage) {
		println("Admin: " + message.message)
	}

	public fun printSystem(mensage: String) {
		println("System: " + mensage)
	}
}


fun main(args: Array<String>) {
	try {
		val connection = Socket("localhost", Config.portNumber)

		val hilo = ClienteSubasta(connection)
		hilo.start()
	} catch (e: ConnectException) {
		println("no se pudo connectar")
	} catch (e: Exception) {
		println("algo salió mal")
	}
}
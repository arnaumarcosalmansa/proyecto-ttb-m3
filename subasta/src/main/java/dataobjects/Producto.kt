package dataobjects

import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.ManyToOne

import java.io.Serializable
import javax.persistence.GeneratedValue

@Entity
class Producto : Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	var id: Int = 0;
	
	@Column
	var nombre: String? = null;
	
	@OneToMany
	var lotes : Set<Lote> = emptySet();
}
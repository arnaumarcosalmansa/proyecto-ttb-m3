package dataobjects

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn

import java.io.Serializable
import java.util.Date
import javax.persistence.GenerationType
import javax.persistence.GeneratedValue

@Entity
class Licitacion : Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	var id = 0;
	
	@ManyToOne
	@JoinColumn
	var lote : Lote? = null;
	
	@ManyToOne
	@JoinColumn
	var cliente : Cliente? = null;
	
	@Column
	var cantidad : Int = 0;
	
	@Column
	var momento : Date = Date()
}
package dataobjects

import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn

import java.io.Serializable
import javax.persistence.OneToOne
import java.util.Date
import javax.persistence.GeneratedValue

@Entity
class Lote : Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	var id: Int = 0;
	
	@ManyToOne
	@JoinColumn
	var producto: Producto? = null
	
	@Column
	var fechaSubasta = Date()
	
	@Column
	var precioSalida: Int = 0
	
	@OneToMany
	var licitaciones : Set<Licitacion> = emptySet();
	
	@OneToOne
	var licitacionGanadora: Licitacion? = null
}
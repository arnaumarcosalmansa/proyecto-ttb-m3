package messages.subasta

import messages.Message
import dataobjects.Licitacion

class PujaRechazadaMessage(val razon: String) : Message("pujarechazada")
{
}
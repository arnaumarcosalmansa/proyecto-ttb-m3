package servidor

import dataobjects.Lote
import messages.subasta.PujaMessage
import dataobjects.Licitacion
import dao.LicitacionDao
import messages.subasta.UpdateStatusMessage
import hibernate.DataSource
import java.io.Serializable
import java.util.Date
import messages.subasta.PujaRechazadaMessage
import java.util.Random
import java.util.concurrent.Semaphore

class Subasta(val lote: Lote) : Serializable {

	val licitaciones = mutableListOf<Licitacion>()
	val startedAt = Date()

	public fun pujar(message: PujaMessage): PujaRechazadaMessage? {
		
		//recogemos el cliente actualizado de la base de datos
		DataSource.acquire()
		val cliente_dao = DataSource.source.clientes;
		val cliente = cliente_dao.get(message.cliente.nombre!!);
		DataSource.release()
		
		//creamos la puja
		val puja = Licitacion()
		
		puja.lote = lote
		puja.cliente = cliente
		puja.cantidad = message.puja
		puja.momento = Date()
		
		//si hay una subasta en curso
		Subasta.instancia?.let {
			//si el cliente se puede permitir pujar tal cantidad de dinero	
			if (cliente.saldoActual >= message.puja) {
				//recojemos la cantidad maxima pujada
				var mayorCantidad = it.getMaximaCantidadPujada();
				
				//comprovamos que la cantidad maxima pujada sea mallor a la cantidad maxima
				if (puja.cantidad > mayorCantidad) {
					this.licitaciones.add(puja) //añadimos a la lista
					var messageUpdate = UpdateStatusMessage(puja) //creamos el messageUpdate
					ServerSubasta.server.sendToAllLogged(messageUpdate) //enviamos a todos el messageUpdate
				} else {
					//en caso de que la cantidad pujada no sea mallor a la cantidad maxima
					return PujaRechazadaMessage("La puja es demasiado baja.")
				}
				
			} else {
				//si el cliente no se puede permitir pujar tal cantidad de dinero
				return PujaRechazadaMessage("La puja excede el limite de saldo que tiene el usuario actual.")
			}
			
		} ?: run {
			//si no hay ninguna subasta en curso
			return PujaRechazadaMessage("No hay subastas activas.")
		}
		//si se a notificado a todos que halguien a pujado se devuelve nullo
		return null;
	}

	public fun getMaximaCantidadPujada(): Int {
		var max = this.lote.precioSalida
		if (licitaciones.isNotEmpty()) {
			max = licitaciones.last().cantidad
		}
		return max
	}

	companion object {
		var instancia: Subasta? = null
			get
			private set

		val permit = Semaphore(1, true)
		var closedAt = 0L

		private val rand = Random()

		fun crear() {
			DataSource.acquire()
			val productos = DataSource.source.productos.list()
			val producto = productos[rand.nextInt(productos.size)]
			val lote = Lote()
			lote.producto = producto
			lote.precioSalida = rand.nextInt(500) + 100
			lote.fechaSubasta = Date()

			DataSource.source.lotes.saveOrUpdate(lote)

			instancia = Subasta(lote)
			DataSource.release()
		}

		fun pujar(puja: Licitacion) {
			var valida = false
			instancia!!.let {
				if (!it.licitaciones.isEmpty()) {
					val last = it.licitaciones.last()
					//if (last. )
				}
			}
		}

		fun cerrar() {
			DataSource.acquire()
			instancia!!.let {

				if (!it.licitaciones.isEmpty()) {
					it.licitaciones.forEach {
						DataSource.source.licitaciones.saveOrUpdate(it)
					}
					
					val ganadora = it.licitaciones.last()
					
					//recoger el cliente y restalde el total de la puja
					val clienteGanador = DataSource.source.clientes.get(ganadora.cliente!!.nombre!!)
					clienteGanador.saldoActual += -ganadora.cantidad
					
					 DataSource.source.clientes.saveOrUpdate(clienteGanador)

					
					it.lote.licitacionGanadora = ganadora
					
					
					
				}

				DataSource.source.lotes.saveOrUpdate(it.lote)
			}

			instancia = null
			DataSource.release()
		}
	}
}
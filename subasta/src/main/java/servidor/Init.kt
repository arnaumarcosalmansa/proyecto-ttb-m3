package servidor

import hibernate.DataSource
import dataobjects.Producto
import dataobjects.Lote
import dataobjects.Cliente

class Init {

	companion object {
		init {
			/*
			registrarProductos()
			registrarLotes()
			registrarUsuarios()
			 */
		}

		public fun registrarProductos() {
			val productos = List<Producto>(10) {
				val p = Producto()
				p.id = 0
				p.nombre = "Producto ${it}"
				p
			}

			DataSource.acquire()
			for (p in productos) {
				DataSource.source.productos.saveOrUpdate(p)
			}
			DataSource.release()
		}

		public fun registrarLotes() {
			for (i in 1..11) {
				val lote = Lote()
				lote.id = 0
				lote.precioSalida = 100
				DataSource.acquire()
				lote.producto = DataSource.source.productos.get(i)

				DataSource.source.lotes.saveOrUpdate(lote)
				DataSource.release()
			}
		}
		
		public fun registrarUsuarios() {
			DataSource.acquire()
			val arnau = Cliente()
			arnau.nombre = "arnau"
			arnau.contrasena = "1234"
			arnau.saldoInicial = 990
			arnau.saldoActual = 990
			
			DataSource.source.clientes.saveOrUpdate(arnau)
			
			val adrian = Cliente()
			adrian.nombre = "adrian"
			adrian.contrasena = "1234"
			adrian.saldoInicial = 1000
			adrian.saldoActual = 1000
			
			DataSource.source.clientes.saveOrUpdate(adrian)
			
			val pepe = Cliente()
			pepe.nombre = "pepe"
			pepe.contrasena = "1234"
			pepe.saldoInicial = 1050
			pepe.saldoActual = 1050
			
			DataSource.source.clientes.saveOrUpdate(pepe)
			
			val adrianR = Cliente()
			adrianR.nombre = "adrianR"
			adrianR.contrasena = "1234"
			adrianR.saldoInicial = 1000
			adrianR.saldoActual = 1000
			
			DataSource.source.clientes.saveOrUpdate(adrianR)
			
			val iphone = Cliente()
			iphone.nombre = "iphone"
			iphone.contrasena = "1234"
			iphone.saldoInicial = 1200
			iphone.saldoActual = 1200
			
			DataSource.source.clientes.saveOrUpdate(iphone)
			DataSource.release()
		}
	}
}
package servidor

import java.net.Socket
import dataobjects.Cliente
import java.io.ObjectOutputStream
import messages.login.RequestLoginMessage
import java.io.ObjectInputStream
import messages.Message
import messages.LoginMessage
import hibernate.DataSource
import messages.subasta.PujaMessage
import dataobjects.Licitacion
import java.util.concurrent.Semaphore
import messages.subasta.UpdateFullStatusMessage
import messages.login.LoginAnswerMessage
import messages.login.LogoutMessage
import messages.subasta.PujaRechazadaMessage

//ojo con la subasta nula
class ClientDispatcher(val client: Socket, var subasta: Subasta?, val mutexSubasta: Semaphore) : Thread() {

	private val datasource = DataSource.source

	private var run: Boolean = true;

	private var cliente: Cliente? = null;

	val oos = ObjectOutputStream(client.outputStream)
	val ois = ObjectInputStream(client.inputStream)

	var logged = false;

	init {
		//this.start();
	}

	//implementar la funcionalidad
	public override fun run() {
		try {
			while (!logged && run) {
				val log = requestLogin()
				var clt: Cliente? = null
				log?.let {
					clt = checkLogin(it)
				}
				clt?.let {
					logged = true
					oos.writeObject(LoginAnswerMessage(0, it))
					cliente = it
				} ?: run {
					logged = false
					oos.writeObject(LoginAnswerMessage(1, null))
				}
				oos.flush()
			}

			//informa que se a logeado
			Subasta.permit.acquire()
			Subasta.instancia?.let {
				oos.writeObject(UpdateFullStatusMessage(it))
				oos.flush()
			}
			Subasta.permit.release()


			while (logged && run) {
				val message = ois.readObject() as Message
				
				when (message) {
					is PujaMessage -> {
						Subasta.permit.acquire()
						var puja: PujaRechazadaMessage? = Subasta.instancia?.pujar(message)
						Subasta.permit.release()

						puja?.let {
							oos.writeObject(it)
							oos.flush()
						}
					}
					is LogoutMessage -> {
						logoutClient(message)
					}
				}
			}
		} catch (e: Exception) {
			//println("cliente desconectado")
			logoutClient(null)
		}
	}

	private fun logoutClient(message: LogoutMessage?) {
		client.close()
		
		logged = false;
		val resultado = ServerSubasta.server.notifyDisconnect(this) //esto ya saca el clientDispatcher de la lista de clientsDispatchers
		
		if(resultado) {
			println("El cliente " + cliente?.nombre + " se a desconectado " + resultado)
		} else {
			println("El cliente no se ha podido desconectar.")
		}
	}

	private fun requestLogin(): LoginMessage? {
		oos.writeObject(RequestLoginMessage())
		oos.flush()

		val message = ois.readObject() as Message
		if (message is LoginMessage) {
			return message
		} else {
			return null
		}
	}

	private fun checkLogin(log: LoginMessage): Cliente? {
		DataSource.acquire()
		val cliente = datasource.clientes.loadByNombre(log.nombre)
		DataSource.release()
		cliente?.let {
			if (it.contrasena == log.contrasena) {
				return it
			}
		}
		return null
	}

	//recive un booleano, true = interrupt, false = run false
	public fun stop(options: Boolean) {
		if (options) this.interrupt();
		else this.run = false;
		
		this.client.close()
	}
}
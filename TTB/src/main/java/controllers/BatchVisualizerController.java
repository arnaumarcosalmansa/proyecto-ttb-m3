package controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import classes.Batch;
import classes.Product;
import classes.Provider;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import managers.Manager;
import managers.ProductManager;

public class BatchVisualizerController extends VisualizerController<Product>
{
	@FXML
	private TableColumn batchIdColumn;
	@FXML
	private TableColumn batchProductColumn;
	@FXML
	private TableColumn batchQuantityColumn;
	@FXML
	private TableColumn batchEntryDateColumn;
	@FXML
	private TableColumn batchExpirationDateColumn;
	@FXML
	private TableView batchTable;
	
	private ObservableList<Batch> batches;
	
	@FXML
	private TextField inputQuantity;
	@FXML
	private TextField inputExpirationDate;
	@FXML
	private Button addButton;
	@FXML
	private Button dropButton;
	@FXML
	private Label errorDate;
	
	@FXML
	public void initialize()
	{
		
	}
	
	@FXML
	public void dropBatch(ActionEvent event) 
	{
		ObservableList<Batch> list = batchTable.getSelectionModel().getSelectedItems();
		for(Batch obj: list) {
			this.object.getLots().remove(obj);
		}
		this.clearAndWrite();
	}
	
	private void initializeTable()
	{
		batchIdColumn.setCellValueFactory(new PropertyValueFactory<Batch, Integer>("id"));
		batchProductColumn.setCellValueFactory(new Callback<CellDataFeatures<Batch, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Batch, String> data) {
				return data
						.getValue()
						.getProduct()
						.getNameProperty();
			}
		});
		
		batchQuantityColumn.setCellValueFactory(new PropertyValueFactory<Batch, Integer>("quantity"));
		batchEntryDateColumn.setCellValueFactory(new PropertyValueFactory<Batch, LocalDate>("entryDate"));
		batchExpirationDateColumn.setCellValueFactory(new PropertyValueFactory<Batch, LocalDate>("expirationDate"));
		
		batchTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		batchTable.setItems(batches);
	}
	
	@FXML
	public void close(ActionEvent event)
	{
		Stage thisStage = (Stage) batchTable.getScene().getWindow();
		thisStage.close();
	}
	
	@Override
	public void setObject(Product object)
	{
		this.object = object;
		batches = FXCollections.observableArrayList();

		batches.addAll(this.object.getLots());
		initializeTable();
	}
	
	@FXML
	@Override
	public void integerComparisonKeyTyped(KeyEvent key)
	{
		super.integerComparisonKeyTyped(key); //el boton de añadir debe poder introducir simbolos
	}
	
	@FXML
	public void dataComparisionKeyTyped(KeyEvent key) 
	{
		super.dataComparisionKeyTyped(this.inputExpirationDate, key);
	}
	
	@FXML
	public void addLotListener(ActionEvent event) 
	{
		if(event.getSource().equals(this.addButton))
		{
			if(this.inputQuantity != null && !this.inputQuantity.getText().equals(""))
			{
				//this.object.getLots().add(new Batch(null, Integer.valueOf(this.inputQuantity.getText())));
				if(this.inputExpirationDate != null && inputExpirationDate.getText().equals("")) 
				{
					this.object.afegirLot(Integer.valueOf(this.inputQuantity.getText()), null);
					this.clearData();
				}
				else 
				{
					this.errorDate.setVisible(false);
					String stringDate = inputExpirationDate.getText();
				
					try 
					{//control de excepciones por si la fecha introducida sale de los valores
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
						
						LocalDate eDate = LocalDate.parse(stringDate, dtf);
						if(eDate.isAfter(LocalDate.now()))
						{
							this.object.afegirLot(Integer.valueOf(this.inputQuantity.getText()), eDate);
							this.clearData();
						}
						else
						{
							this.errorDate.setText("Error, la fecha debe ser posterior a hoy."); //error no respeta dias o meses
							this.errorDate.setVisible(true);
						}
					}
					catch(Exception e) 
					{
						this.errorDate.setText("Error, la fecha debe ir en formato YYYY-MM-DD"); //error no respeta dias o meses
						this.errorDate.setVisible(true);
					}
				
				}
			}
			else 
			{
				this.errorDate.setText("Error, debe introducir una cantidad."); //error falta cantidad
				this.errorDate.setVisible(true);
			}
			this.clearAndWrite();
		}
	}
	
	public void clearData() 
	{
		this.inputQuantity.setText("");
		this.inputExpirationDate.setText("");
	}
	
	public void clearAndWrite() 
	{
		batches.clear();
		batches.addAll(this.object.getLots());
	}
}

package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public abstract class TabController<T> extends Controller<T>
{
	@FXML
	public void dataComparisionAdbancedKeyTyped(TextField dateTextField, KeyEvent key) 
	{
		//se deve poder introducir numeros y guiones
		if(dateTextField.getText().length() < 11) 
		{
			char caracter = key.getCharacter().charAt(0);
			
			if(!Character.isDigit(caracter)) 
			{
				if(caracter != '-' && caracter != '>' && caracter != '<') key.consume();
				else {
					this.validateSignes(dateTextField, key);
				}
			}
				
		}
		else key.consume();
	}
	
	public void integerComparisionAdvancedKeyTyped(TextField integerText, KeyEvent key) {
		char pressed = key.getCharacter().charAt(0);
		if(!Character.isDigit(pressed) && pressed != '>' && pressed != '<')
		{
			key.consume();
		}
		else this.validateSignes(integerText, key);
	}
	
	@FXML
	public void doubleComparisonKeyTyped(TextField doubleText, KeyEvent key)
	{
		char pressed = key.getCharacter().charAt(0);
		if(doubleText.getText().indexOf(".") > -1) 
		{
			if(!Character.isDigit(pressed) && pressed != '>' && pressed != '<') key.consume();
			else this.validateSignes(doubleText, key);
		}
		else 
		{
			if(!Character.isDigit(pressed) && pressed != '>' && pressed != '<' && pressed != '.') key.consume();
			else {
				this.validateSignes(doubleText, key);
			}
			
		}
	}
	
	private void validateSignes(TextField textField, KeyEvent key) {
		char car = key.getCharacter().charAt(0);
		if(car == '<' || car == '>' || car == '-') {
			int cant = 0;
			
			if(car == '-') {
				for(char a: textField.getText().toCharArray()) {
					if(a == car) cant++;
				}
			}
			
			switch(car) {
			case '-':
				if(cant >= 2) key.consume();
				break;
			case '<':
				if(textField.getText().indexOf(">") > -1) key.consume();
				else {
					if(textField.getText().indexOf("<") > -1) key.consume();
				}
				break;
			case '>':
				if(textField.getText().indexOf("<") > -1) key.consume();
				else {
					if(textField.getText().indexOf(">") > -1) key.consume();
				}
				break;
			}
			
			
			
		}
	}
	
}

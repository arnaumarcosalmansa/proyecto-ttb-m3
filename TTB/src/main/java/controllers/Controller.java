package controllers;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public abstract class Controller<T>
{
	protected Controller parent;
	protected T object;

	public abstract void setResult(Object result, Controller whoSetsTheResult);
	
	public void setParentController(Controller parent)
	{
		this.parent = parent;
	}
	
	public void setObject(T object)
	{
		this.object = object;
	}
	
	//generico, solo ints
	@FXML
	public void onKeyTyped(KeyEvent key) 
	{
		if(!Character.isDigit(key.getCharacter().charAt(0))) key.consume();
	}
	
	@FXML
	public void integerComparisonKeyTyped(KeyEvent key)
	{
		char pressed = key.getCharacter().charAt(0);
		if(!Character.isDigit(pressed) && pressed != '>' && pressed != '<')
		{
			key.consume();
		}
	}
	
	@FXML
	public void dataComparisionKeyTyped(TextField dateTextField, KeyEvent key) 
	{
		//se deve poder introducir numeros y guiones
		if(dateTextField.getText().length() < 10) 
		{
			char caracter = key.getCharacter().charAt(0);
			
			if(!Character.isDigit(caracter)) 
			{
				if(caracter != '-') key.consume();
				else {
					int cant = 0;
					for(char a: dateTextField.getText().toCharArray()) {
						if(a == '-') cant++;
					}
					if(cant >= 2) key.consume();
				}
			}
				
		}
		else key.consume();
	}
	
	protected ArrayList<Node> editableNodes = new ArrayList<Node>();
	
	protected abstract void prepareEditableNodes();
	
	//limpia el contenido de los filtros, los radioButtons se tratan en cada uno de sus controladores
	protected void clearEditableNodes()
	{
		for(Node n : editableNodes)
		{
			if(n instanceof TextField)
			{
				TextField tf = (TextField) n;
				tf.setText("");
			}
			else if(n instanceof TextArea) 
			{
				TextArea ta = (TextArea) n;
				ta.setText("");
			}
			else if(n instanceof CheckBox)
			{
				CheckBox cb = (CheckBox) n;
				cb.setSelected(false);
			}
		}
	}
	
	//para valores double
	public void onKeyTypedDouble(TextField doubleText, KeyEvent key) 
	{
		char caracter = key.getCharacter().charAt(0);
		if(doubleText.getText().contains(".") && caracter == '.') key.consume();
		else 
		{
			if(!Character.isDigit(caracter) && caracter != '.') key.consume();
		}
	}
	
	//para cordenadas
	public void onKeyTypedCoords(TextField coords, KeyEvent key) 
	{
		char caracter = key.getCharacter().charAt(0);
		if(!Character.isDigit(caracter) && caracter != '.' && caracter != ' ' && caracter != '-') key.consume();
		else 
		{
			if(coords.getText().contains(" ") && caracter == ' ') key.consume();
			else 
			{
				Integer points = 0;
				for(char e: coords.getText().toCharArray()) 
				{
					if(e == caracter) points++;
				}
				if(points >= 2 && (caracter == '.' || caracter == '-')) key.consume();
			}
		}
	}
	
	//para cifs
	public void onKeyTypedCif(TextField cif, KeyEvent key) 
	{
		if(cif != null) {
			if(cif.getText().length() < 8) this.onKeyTyped(key);
			else 
			{
				if(cif.getText().length() == 8) 
				{
					char pressed = key.getCharacter().charAt(0);
					if(!Character.isLetter(pressed)) key.consume();
				}
				else key.consume();
			}
		}
	}
	
	//para telefonos
	public void onKeyTypedPhone(TextField phoneTextField, KeyEvent key) 
	{
		if(phoneTextField.getText().length() < 9) this.onKeyTyped(key);
		else key.consume();
	}
	
	public boolean validateDouble(String data) {
		int pos = data.indexOf(".");
		boolean result = true;
		if(pos == (data.length() - 1) || pos == 0) result = false;
		return !result;
	}
}

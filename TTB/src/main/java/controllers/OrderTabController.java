package controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.util.Callback;
import classes.Client;
import classes.Order;
import classes.OrderLine;
import classes.OrderStatus;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import classes.UnitOfMeasurement;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafxutils.SceneUtils;
import managers.OrderManager;
import managers.ProductManager;
import queries.Condition;
import travellingsalesman.TravelingSalesman;

public class OrderTabController extends TabController<Order>
{
	@FXML
	private TextField orderCodeTextField;
	
	@FXML
	private TextField orderClientCifTextField;
	
	@FXML
	private TextField orderDateOrderTextField;
	
	@FXML
	private TextField orderDataDeliveryTextField;
	
	@FXML
	private TextField orderShippingTextField;
	
	@FXML
	private CheckBox pendingCheckBox;
	
	@FXML
	private CheckBox preparedCheckBox;
	
	@FXML
	private CheckBox transportCheckBox;
	
	@FXML
	private CheckBox deliveredCheckBox;
	
	@FXML
	private TableView<Order> orderTable;
	
	@FXML
	private TableColumn orderIdColumn;
	
	@FXML
	private TableColumn orderClientCifColumn;
	
	@FXML
	private TableColumn orderDateColumn;
	
	@FXML
	private TableColumn deliveryDateColumn;
	
	@FXML
	private TableColumn orderStatusColumn;
	
	@FXML
	private TableColumn orderShippingColumn;
	
	private ObservableList<Order> orders;
	
	@FXML
	private Label errorData;
	
	@FXML
	public void initialize()
	{
		prepareEditableNodes();
		initCheckBoxes();
		
		initializeTable();
		orders.addAll(OrderManager.singleton().readAll());
	}
	
	private void initCheckBoxes()
	{
		pendingCheckBox.setUserData(OrderStatus.PENDENT);
		
		preparedCheckBox.setUserData(OrderStatus.PREPARADA);
		
		transportCheckBox.setUserData(OrderStatus.TRANSPORT);
		
		deliveredCheckBox.setUserData(OrderStatus.LLIURADA);
	}
	
	protected void prepareEditableNodes()
	{
		editableNodes.add(orderCodeTextField);
		editableNodes.add(orderDateOrderTextField);
		editableNodes.add(orderDataDeliveryTextField);
		editableNodes.add(orderShippingTextField);
		
		editableNodes.add(deliveredCheckBox);
		editableNodes.add(pendingCheckBox);
		editableNodes.add(transportCheckBox);
		editableNodes.add(preparedCheckBox);
	}
	
	private void prepareToggles()
	{		
		pendingCheckBox.setUserData(OrderStatus.PENDENT);
		preparedCheckBox.setUserData(OrderStatus.PREPARADA);
		transportCheckBox.setUserData(OrderStatus.TRANSPORT);
		deliveredCheckBox.setUserData(OrderStatus.LLIURADA);
	}
	
	private void initializeTable()
	{
		orderIdColumn.setCellValueFactory(new PropertyValueFactory<Order, Integer>("id"));
		orderDateColumn.setCellValueFactory(new PropertyValueFactory<Order, LocalDate>("orderDate"));
		deliveryDateColumn.setCellValueFactory(new PropertyValueFactory<Order, LocalDate>("deliveryDate"));
		orderStatusColumn.setCellValueFactory(new PropertyValueFactory<Order, OrderStatus>("status"));
		orderShippingColumn.setCellValueFactory(new PropertyValueFactory<Order, Double>("shipping"));
		
		orderClientCifColumn.setCellValueFactory(new PropertyValueFactory<Order, Client>("client"));

		orderClientCifColumn.setCellValueFactory(new Callback<CellDataFeatures<Order, String>, 
                ObservableValue<String>>()
		{  
			@Override
			public ObservableValue<String> call(CellDataFeatures<Order, String> param)
			{
				return param.getValue().getClient().getCIFProperty();
			}  
		});
		
		orders = FXCollections.observableArrayList();
		orderTable.setRowFactory(tv ->
		{
			TableRow<Order> row = new TableRow<>();
			row.setOnMouseClicked(event ->
			{
				if (event.getClickCount() == 2 && (!row.isEmpty()))
				{
					Stage thisStage = (Stage)((Node) event.getSource()).getScene().getWindow();
					try
					{
						SceneUtils.singleton().openNewFrame("/gui/order_frame.fxml", thisStage, this, FrameController.ACTION_VIEW_AND_MODIFY, row.getItem(), "Pedido");
					}
					catch(Exception e)
					{
						//e.printStackTrace();
					}
				}
			});
			return row;
		});
		
		orderTable.setItems(orders);
	}
	
	@SuppressWarnings("restriction")
	public void searchButtonPressed()
	{
		Condition<Order>[] conditionArray = buildConditionArray();
		if(conditionArray != null) {
			Set<Order> results = OrderManager.singleton().search(conditionArray);
			updateTable(results);
		}
	}

	private Condition<Order>[] buildConditionArray()
	{
		errorData.setVisible(false);
		ArrayList<Condition<Order>> conditions = new ArrayList<Condition<Order>>();
		boolean error = false;
		
		String orderCode = orderCodeTextField.getText();
		if (!orderCode.isEmpty())
		{
			conditions.add(Order.CONDITIONS.idEquals(Integer.valueOf(orderCode)));
		}
		
		String orderClientCif = orderClientCifTextField.getText();
		if(!orderClientCif.isEmpty())
		{
			conditions.add(Order.CONDITIONS.clientMatchesConditions(Client.CONDITIONS.cifEquals(orderClientCif)));
		}

		String orderOrderDate = orderDateOrderTextField.getText().trim().replace(" ", "");
		if(!orderOrderDate.isEmpty())
		{
			try
			{
				if(orderOrderDate.startsWith(">"))
				{
					orderOrderDate = orderOrderDate.substring(1, orderOrderDate.length());
					conditions.add(Order.CONDITIONS.orderDateBiggerThan(LocalDate.parse(orderOrderDate)));
				}
				else if(orderOrderDate.startsWith("<"))
				{
					orderOrderDate = orderOrderDate.substring(1, orderOrderDate.length());
					conditions.add(Order.CONDITIONS.orderDateLessThan(LocalDate.parse(orderOrderDate)));
				}
				else
				{
					conditions.add(Order.CONDITIONS.orderDateEquals(LocalDate.parse(orderOrderDate)));
				}
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				error = true;
				errorData.setVisible(true);
				errorData.setText("Error: el formato de la fecha no es corecto 'yyyy-MM-dd'");
			}
		}
			
		String orderDateDelivery = orderDataDeliveryTextField.getText().trim().replace(" ", "");
		if(!orderDateDelivery.isEmpty())
		{
			try
			{
				if(orderDateDelivery.startsWith(">"))
				{
					orderDateDelivery = orderDateDelivery.substring(1, orderDateDelivery.length());
					conditions.add(Order.CONDITIONS.deliveryDateBiggerThan(LocalDate.parse(orderDateDelivery)));
				}
				else if(orderDateDelivery.startsWith("<"))
				{
					orderDateDelivery = orderDateDelivery.substring(1, orderDateDelivery.length());
					conditions.add(Order.CONDITIONS.deliveryDateLessThan(LocalDate.parse(orderDateDelivery)));
				}
				else
				{
					conditions.add(Order.CONDITIONS.deliveryDateEquals(LocalDate.parse(orderDateDelivery)));
				}
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				error = true;
				errorData.setVisible(true);
				errorData.setText("Error: el formato de la fecha no es corecto 'yyyy-MM-dd'");
			}
		}
		
		String orderShipping = orderShippingTextField.getText().trim().replace(" ", "");
		if(!orderShipping.isEmpty())
		{
			try
			{
				if(orderShipping.startsWith(">"))
				{
					orderShipping = orderShipping.substring(1, orderShipping.length());
					conditions.add(Order.CONDITIONS.orderShippingBiggerThan(Double.valueOf(orderShipping)));
				}
				else if(orderShipping.startsWith("<"))
				{
					orderShipping = orderShipping.substring(1, orderShipping.length());
					conditions.add(Order.CONDITIONS.orderShippingLessThan(Double.valueOf(orderShipping)));
				}
				else
				{
					conditions.add(Order.CONDITIONS.orderShippingEquals(Double.valueOf(orderShipping)));
				}
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				error = true;
				errorData.setVisible(true);
				errorData.setText("Error: formato de error en portes, formato 'xx.xx'");
			}
		}
		
		if(!error) 
		{
			ArrayList<OrderStatus> estados = new ArrayList<OrderStatus>();
			if(pendingCheckBox.isSelected()) estados.add((OrderStatus)pendingCheckBox.getUserData());
			if(preparedCheckBox.isSelected()) estados.add((OrderStatus) preparedCheckBox.getUserData());
			if(transportCheckBox.isSelected()) estados.add((OrderStatus) transportCheckBox.getUserData());
			if(deliveredCheckBox.isSelected()) estados.add((OrderStatus) deliveredCheckBox.getUserData());
			
			OrderStatus[] arrayEstados = new OrderStatus[estados.size()];
			
			estados.toArray(arrayEstados);
			conditions.add(Order.CONDITIONS.statusIn(arrayEstados));
			Condition[] conditionArray = new Condition[conditions.size()];
			conditions.toArray(conditionArray);
			
			return conditionArray;
		}
		else return null;
	}
	
	private void updateTable(Set<Order> newData)
	{
		orders.clear();
		orders.addAll(newData);
	}

	public void onKeyTyped(KeyEvent e)
	{
		super.onKeyTyped(e);
	}

	public void actionEvent(ActionEvent e)
	{

	}

	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		if(whoSetsTheResult instanceof OrderFrameController)
		{
			OrderManager.singleton().saveOrUpdate(result);
			this.updateTable(OrderManager.singleton().readAll());
		}
		else if(whoSetsTheResult instanceof ClientSelectorController)
		{
			if(result != null)
			{
				Client c = (Client) result;
				orderClientCifTextField.setText(c.getCIF());
			}
			else
			{
				orderClientCifTextField.setText("");
			}
		}
	}

	@FXML
	public void clearFilters(ActionEvent event)
	{
		super.clearEditableNodes();
		pendingCheckBox.setSelected(true);
		preparedCheckBox.setSelected(true);
		transportCheckBox.setSelected(true);
		deliveredCheckBox.setSelected(true);
	}
	
	@FXML
	public void selectClient(ActionEvent event)
	{
		try
		{
			Stage thisStage = (Stage)((Button) event.getSource()).getScene().getWindow();
			SceneUtils.singleton().openNewSelector("/gui/client_selector.fxml", thisStage, this, "Seleccionar cliente");
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	@FXML
	public void create(ActionEvent event)
	{
		try
		{
			Stage thisStage = (Stage)((Button) event.getSource()).getScene().getWindow();
			SceneUtils.singleton().openNewFrame("/gui/order_frame.fxml", thisStage, this, FrameController.ACTION_CREATE, new Order(), "Nuevo pedido");
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	@FXML
	public void onKeyTypedDouble(KeyEvent key) 
	{
		super.onKeyTypedDouble(this.orderShippingTextField, key);
	}
	
	@FXML
	public void dataComparisionKeyTypedShared(KeyEvent key) 
	{
		super.dataComparisionAdbancedKeyTyped(this.orderDateOrderTextField, key);
	}
	
	@FXML
	public void dataComparisionKeyTypedDelivery(KeyEvent key) 
	{
		super.dataComparisionAdbancedKeyTyped(this.orderDataDeliveryTextField, key);
	}
	
	@FXML
	public void doubleComparisonKeyTyped(KeyEvent key) {
		super.doubleComparisonKeyTyped(this.orderShippingTextField, key);
	}
	
	@FXML
	public void calculateRoute(ActionEvent event)
	{
		ArrayList<Client> clients = getClients();
		ArrayList<Client> ruta = new ArrayList<Client>();
		
		TravelingSalesman.startTravel(clients, ruta);
		
		try
		{
			Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
			SceneUtils.singleton().openNewFrame("/gui/route_frame.fxml", thisStage, this, 0, ruta, "Ruta de distribución");
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	private ArrayList<Client> getClients()
	{
		Set<Order> comandasPreparadas = OrderManager.singleton().search(Order.CONDITIONS.statusEquals(OrderStatus.PREPARADA));
		
		Set<Client> clients = new HashSet<Client>();
		
		for(Order o : comandasPreparadas)
		{
			clients.add(o.getClient());
		}
		
		return new ArrayList<Client>(clients);
	}
	
	@FXML
	public void prepararPedido(ActionEvent event)
	{
		Set<Order> pedidos = OrderManager.singleton().search(Order.CONDITIONS.statusEquals(OrderStatus.PENDENT));
		int noPreparadas = 0;
		for(Order pedido : pedidos)
		{
			List<OrderLine> lineas = pedido.getLinies();
			boolean sePuedePreparar = true;
			for(OrderLine linia : lineas)
			{
				Product p = linia.getProducte();
				if(p.getStock() < linia.getQuantity())
				{
					sePuedePreparar = false;
					noPreparadas++;
					break;
				}
			}
			
			if(sePuedePreparar)
			{
				for(OrderLine linia : lineas)
				{
					Product p = linia.getProducte();
					p.subtractProducte(linia.getQuantity());
				}
				pedido.setStatus(OrderStatus.PREPARADA);
			}
		}
		
		System.out.println("no se pudieron preparar " + noPreparadas + " pedidos");
		this.updateTable(OrderManager.singleton().readAll());
	}
}

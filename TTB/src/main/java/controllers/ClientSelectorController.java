package controllers;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Pattern;

import classes.Client;
import classes.ContactInfo;
import classes.Client;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import managers.ClientManager;
import queries.Condition;

public class ClientSelectorController extends SelectorController<Client> {

	@FXML
	private TextField clientCifTextField;
	@FXML
	private TextField clientNameTextField;
	@FXML
	private TextField clientPhoneTextField;
	@FXML
	private TextField clientCountryTextField;
	@FXML
	private TextField clientCityTextField;
	@FXML
	private CheckBox clientIsActiveCheckBox;
	@FXML
	private Button clearFilters;
	
	@FXML
	private TableColumn clientCifColumn;
	@FXML
	private TableColumn clientNameColumn;
	@FXML
	private TableColumn clientPhoneColumn;
	@FXML
	private TableColumn clientCountryColumn;
	@FXML
	private TableColumn clientCityColumn;
	@FXML
	private TableColumn clientActiveColumn;
	@FXML
	private TableView clientTable;
	private ObservableList<Client> clients;
	
	
	@FXML
	private void clearFiltersInputs() {
		clientCifTextField.setText("");
		clientNameTextField.setText("");
		clientPhoneTextField.setText("");
		clientCountryTextField.setText("");
		clientCityTextField.setText("");
		clientIsActiveCheckBox.setSelected(false);
		searchButtonPressed();
	}
	
	@FXML
	private void searchButtonPressed() {
		Condition<Client>[] conditions = buildConditionArray();
		
		Set<Client> results = ClientManager.singleton().search(conditions);

		updateTable(results);
	}
	
	private void updateTable(Set<Client> newData) {
		clients.clear();
		clients.addAll(newData);
	}
	
	private Condition<Client>[] buildConditionArray(){
		
		ArrayList<Condition<Client>> conditions = new ArrayList<Condition<Client>>();
		String clientCif = clientCifTextField.getText();
		if(!clientCif.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(clientCif);
				conditions.add(Client.CONDITIONS.cifMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.cifContains(clientCif));
			}
		}
				
		String clientName = clientNameTextField.getText();
		if(!clientName.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(clientName);
				conditions.add(Client.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.nameContains(clientName));
			}
		}
		
		String clientPhone = clientPhoneTextField.getText();
		if(!clientPhone.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(clientPhone);
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneContains(clientPhone)));
			}
			
		}
		
		String clientCountry = clientCountryTextField.getText();
		if(!clientCountry.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(clientCountry);
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryContains(clientCountry)));
			}
		}
		String clientCity = clientCityTextField.getText();
		if(!clientCity.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(clientCityTextField.getText());
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityContains(clientCity)));
			}
		}
		
		if(clientIsActiveCheckBox.isSelected())
		{
			conditions.add(Client.CONDITIONS.activeEquals(clientIsActiveCheckBox.isSelected()));
		}
		
		Condition[] cons = new Condition[conditions.size()];
		conditions.toArray(cons);
		return cons;
	}
	
	@FXML
	private void initialize()
	{
		prepareEditableNodes();
		initTable();
	}
	
	private void initTable()
	{
		clientCifColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("CIF"));
		clientNameColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("name"));
		clientActiveColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getIsActiveProperty();
			}
		});
		clientCityColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getContact().getCityProperty();
			}
			
		});
		
		clientCountryColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getContact().getCountryProperty();
			}
			
		});
		
		clientPhoneColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Client, String> data){  
				return data.getValue().getContact().getPhoneProperty();
			}  
		});
		
		clients = FXCollections.observableArrayList();
		clients.addAll(ClientManager.singleton().readAll());
		
		clientTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		clientTable.setItems(clients);
	}
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		
	}

	@FXML
	public void acceptPressed(ActionEvent e)
	{
		Client p = (Client) clientTable
				.getSelectionModel()
				.getSelectedItem();
		parent.setResult(p, this);
		
		Stage thisStage = (Stage) clientTable.getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void cancelar(ActionEvent e)
	{
		parent.setResult(null, this);
		Stage thisStage = (Stage) clientTable.getScene().getWindow();
		thisStage.close();
	}

	@Override
	protected void prepareEditableNodes()
	{	
		editableNodes.add(clientCifTextField);
		editableNodes.add(clientNameTextField);
		editableNodes.add(clientPhoneTextField);
		editableNodes.add(clientCountryTextField);
		editableNodes.add(clientCityTextField);
		editableNodes.add(clientIsActiveCheckBox);
	}
	
	@FXML
	public void clearFilters(ActionEvent event)
	{
		super.clearEditableNodes();
	}
	
	//para cifs
	@FXML
	public void onKeyTypedCif(KeyEvent key) 
	{
		super.onKeyTypedCif(this.clientCifTextField, key);
	}
		
	//para telefonos
	@FXML
	public void onKeyTypedPhone(KeyEvent key) 
	{
		super.onKeyTypedPhone(this.clientPhoneTextField, key);
	}
}

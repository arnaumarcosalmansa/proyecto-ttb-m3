package controllers;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Pattern;

import classes.Client;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import classes.UnitOfMeasurement;
import javafx.scene.control.Button;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import managers.ProductManager;
import queries.Condition;

public class ProductSelectorController extends SelectorController
{
	@FXML
	private TextField productIdTextField;
	@FXML
	private TextField productNameTextField;

	@FXML
	private ToggleGroup productTypeToggleGroup;
	
	@FXML
	private CheckBox unitCheckBox;
	
	@FXML
	private CheckBox literCheckBox;
	
	@FXML
	private CheckBox gramCheckBox;
	
	@FXML
	private TableColumn productIdColumn;
	
	@FXML
	private TableColumn productNameColumn;
	
	@FXML
	private TableColumn productUnitOfMeasureColumn;
	
	@FXML
	private TableColumn productTypeColumn;
	
	@FXML
	private TableView productTable;

	private ObservableList<Product> products;

	@FXML
	public void initialize()
	{
		prepareEditableNodes();
		prepareToggles();
		iniciarTabla();
		products = FXCollections.observableArrayList();
		
	}
	
	private void iniciarTabla()
	{
		productIdColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("productId"));
		productNameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("productName"));
		productUnitOfMeasureColumn.setCellValueFactory(new PropertyValueFactory<Product, UnitOfMeasurement>("unitatMesura"));
		productTypeColumn.setCellValueFactory(new PropertyValueFactory<Product, ProductType>("type"));
		
		productTable.setItems(products);
	}
	
	@Override
	protected void prepareEditableNodes()
	{
		editableNodes.add(productIdTextField);
		editableNodes.add(productNameTextField);

		ObservableList<Toggle> productTypeRadioButtons = productTypeToggleGroup.getToggles();
		
		for(Toggle t : productTypeRadioButtons)
		{
			editableNodes.add((Node) t);
		}
		
		editableNodes.add(unitCheckBox);
		editableNodes.add(literCheckBox);
		editableNodes.add(gramCheckBox);
	}
	
	private void prepareToggles()
	{
		ObservableList<Toggle> types = productTypeToggleGroup.getToggles();
		for(Toggle t : types)
		{
			RadioButton rb = (RadioButton) t;
			if(rb.getText().equals("Vendible"))
			{
				rb.setUserData(ProductType.VENDIBLE);
			}
			else if(rb.getText().equals("Ingredient"))
			{
				rb.setUserData(ProductType.INGREDIENTE);
			}
			else
			{
				rb.setUserData(null);
			}
		}
		
		unitCheckBox.setUserData(UnitOfMeasurement.UNIDADES);
		literCheckBox.setUserData(UnitOfMeasurement.LITROS);
		gramCheckBox.setUserData(UnitOfMeasurement.GRAMOS);
	}
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	public void setParentController(Controller parent)
	{
		super.setParentController(parent);
		if(parent instanceof ComponentVisualizerController)
		{
			products.addAll(ProductManager.singleton().search(Product.CONDITIONS.typeEquals(ProductType.INGREDIENTE)));
		}
		else
		{
			products.addAll(ProductManager.singleton().readAll());
		}
		
		productTable.setItems(products);
	}

	@FXML
	public void searchButtonPressed(ActionEvent event)
	{
		Condition[] conditions = buildConditionArray();
		Set<Product> result = ProductManager.singleton().search(conditions);
		this.products.clear();
		this.products.addAll(result);
	}
	
	private Condition<Product>[] buildConditionArray()
	{		
		ArrayList<Condition<Product>> conditions = new ArrayList<Condition<Product>>();
		String stringId = productIdTextField.getText();
		String stringName = productNameTextField.getText();
		
		int id = -1;
		String nombre = null;
		
		if(stringId != null && !stringId.isEmpty())
		{
			id = Integer.parseInt(stringId);
			conditions.add(Product.CONDITIONS.idEqual(id));
		}
		
		if(stringName != null && !stringName.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(stringName);
				conditions.add(Product.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Product.CONDITIONS.nameContainString(stringName));
			}
		}
		
		ArrayList<UnitOfMeasurement> unidades = new ArrayList<UnitOfMeasurement>();
		
		if(unitCheckBox.isSelected()) unidades.add((UnitOfMeasurement) unitCheckBox.getUserData());
		if(literCheckBox.isSelected()) unidades.add((UnitOfMeasurement) literCheckBox.getUserData());
		if(gramCheckBox.isSelected()) unidades.add((UnitOfMeasurement) gramCheckBox.getUserData());
		
		if(unidades.size() > 0)
		{
			UnitOfMeasurement[] auxUnidades = new UnitOfMeasurement[unidades.size()];
			unidades.toArray(auxUnidades);
			conditions.add(Product.CONDITIONS.measureUnityIn(auxUnidades));	
		}
		
		RadioButton selectedRadioButton = (RadioButton) productTypeToggleGroup.getSelectedToggle();

		try
		{
			ProductType type = ProductType.valueOf(selectedRadioButton.getText().toUpperCase());
			conditions.add(Product.CONDITIONS.typeEquals(type));
		}
		catch (Exception e)
		{
			//e.printStackTrace();
		}
		
		Condition[] cons = new Condition[conditions.size()];
		conditions.toArray(cons);
		return cons;
	}
	/*@FXML
	public void onKeyTyped(KeyEvent event)
	{
		super.onKeyTyped(event);
	}*/
	
	@FXML
	public void acceptPressed(ActionEvent event)
	{
		Product p = (Product) productTable
				.getSelectionModel()
				.getSelectedItem();
		parent.setResult(p, this);
		
		Stage thisStage = (Stage) productTable.getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void cancelar(ActionEvent event)
	{
		parent.setResult(null, this);
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void clearFilters()
	{
		super.clearEditableNodes();
		
		unitCheckBox.setSelected(true);
		literCheckBox.setSelected(true);
		gramCheckBox.setSelected(true);
		
		ObservableList<Toggle> productTypeRadioButtons = productTypeToggleGroup.getToggles();
		
		for(Toggle t : productTypeRadioButtons)
		{
			RadioButton rb = (RadioButton) t;
			if(rb.getText().toUpperCase().equals("TODOS"))
			{
				rb.setSelected(true);
			}
		}
	}
}

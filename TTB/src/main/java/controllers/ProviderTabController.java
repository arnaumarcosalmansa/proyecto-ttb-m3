package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import classes.Client;
import classes.ContactInfo;
import classes.Product;
import classes.Provider;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import managers.ClientManager;
import managers.ProviderManager;
import queries.Condition;

public class ProviderTabController extends TabController<Provider>
{
	@FXML
	private TextField providerCifTextField;
	@FXML
	private TextField providerNameTextField;
	@FXML
	private TextField providerPhoneTextField;
	@FXML
	private TextField providerCountryTextField;
	@FXML
	private TextField providerCityTextField;
	@FXML
	private CheckBox providerActiveCheckBox;
	
	@FXML
	private TableColumn providerCifColumn;
	@FXML
	private TableColumn providerNameColumn;
	@FXML
	private TableColumn providerPhoneColumn;
	@FXML
	private TableColumn providerCountryColumn;
	@FXML
	private TableColumn providerCityColumn;
	@FXML
	private TableColumn providerActiveColumn;
	
	@FXML
	private TableView providerTable;
	
	@FXML
	private Button clearProviderButton;
	@FXML
	private Button searchProviderButton;
	@FXML
	private Button addProviderButton;
	@FXML
	private Button altaProviderButton;
	@FXML
	private Button bajaProviderButton;
	
	ObservableList<Provider> providers;
	@FXML
	public void initialize()
	{	
		this.prepareEditableNodes();
		initTable();
	}
	
	@FXML
	private void initTable()
	{
		providers = FXCollections.observableArrayList();
		//asumo que esta bien...
		providerCifColumn.setCellValueFactory(new PropertyValueFactory<Provider, String>("CIF"));;
		
		providerNameColumn.setCellValueFactory(new PropertyValueFactory<Provider, String>("nameProveidor")); 
		
		providerPhoneColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				// TODO Auto-generated method stub
				return data.getValue().getContact().getPhoneProperty();
			}
		});
		providerCountryColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				return data.getValue().getContact().getCountryProperty();
			}
			
		});
		providerCityColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data){  
				return data.getValue().getContact().getCityProperty();
			}  
		});
		providerActiveColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				return data.getValue().getIsActiveProperty();
			}
		});
		
		providerTable.setRowFactory(tv ->
		{
			TableRow<Provider> row = new TableRow<>();
			row.setOnMouseClicked(event ->
			{
				if (event.getClickCount() == 2 && (!row.isEmpty()))
				{
					Provider rowData = row.getItem();
					try
					{
						SceneUtils.singleton().openNewFrame("/gui/provider_frame.fxml", (Stage) providerTable.getScene().getWindow(),this, FrameController.ACTION_VIEW_AND_MODIFY, rowData, "Proveedor");
					}
					catch (IOException e)
					{
						//e.printStackTrace();
					}
				}
			});
			return row;
		});
		
		providers.addAll(ProviderManager.singleton().readAll());
		providerTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		providerTable.setItems(providers);;
	}
	
	@Override
	public void setResult(Object result, Controller elQueEnviaElResultado)
	{
		if(elQueEnviaElResultado instanceof FrameController)
		{
			ProviderManager.singleton().saveOrUpdate(result);
			this.updateTable(ProviderManager.singleton().readAll());
		}
	}

	@Override
	protected void prepareEditableNodes() {
		editableNodes.add(providerCifTextField);
		editableNodes.add(providerNameTextField);
		editableNodes.add(providerPhoneTextField);
		editableNodes.add(providerCityTextField);
		editableNodes.add(providerCountryTextField);
		editableNodes.add(providerActiveCheckBox);
	}
	
	@FXML
	public void searchButtonPressed(ActionEvent event) {
		this.updateTable(ProviderManager.singleton().search(this.buildConditionArray()));
	}
	/*implementar constructr*/
	private Condition<Provider>[] buildConditionArray()
	{
		ArrayList<Condition<Provider>> conditions = new ArrayList<Condition<Provider>>();
		
		if(!providerCifTextField.getText().isEmpty()) {
			conditions.add(Provider.CONDITIONS.cifEquals(providerCifTextField.getText()));
		}
		
		if(!providerNameTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(providerNameTextField.getText());
				conditions.add(Provider.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.nameContains(providerNameTextField.getText()));
			}
		}
		
		if(!providerPhoneTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(providerPhoneTextField.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneContains(providerPhoneTextField.getText())));
			}
		}
		
		if(!providerCountryTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(providerCountryTextField.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryContains(providerCountryTextField.getText())));
			}
		}
		
		if(!providerCityTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(providerCityTextField.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityContains(providerCityTextField.getText())));
			}
		}
		if(providerActiveCheckBox.isSelected()) {
			conditions.add(Provider.CONDITIONS.activeEquals(providerActiveCheckBox.isSelected()));
		}
		
		Condition[] arrayConditions = new Condition[conditions.size()];
		conditions.toArray(arrayConditions);
		return arrayConditions;
	}
	
	private void updateTable(Set<Provider> newData)
	{
		providers.clear();
		providers.addAll(newData);
	}
	
	@FXML
	public void clearButtonPressed(ActionEvent event) {
		super.clearEditableNodes();
		this.updateTable(ProviderManager.singleton().readAll());
	}
	
	@FXML
	public void onKeyTypedPhone(KeyEvent key) {
		super.onKeyTypedPhone(this.providerPhoneTextField, key);
	}
	
	@FXML
	public void onKeyTypedCif(KeyEvent key) {
		super.onKeyTypedCif(this.providerCifTextField, key);
	}
	
	@FXML
	public void onAltaButtonPressed(ActionEvent event) {
		ObservableList<Provider> selecteds = providerTable.getSelectionModel().getSelectedItems();
		//ProviderManager er = (ProviderManager) ProviderManager.singleton();	
		for(Provider pro: selecteds) {
			pro.setActive(true);
			//er.saveOrUpdate(pro);
		}
		this.updateTable(ProviderManager.singleton().readAll());
	}
	
	@FXML
	public void onBaixaButtonPressed(ActionEvent event) {
		ObservableList<Provider> selecteds = providerTable.getSelectionModel().getSelectedItems();
		//ProviderManager er = (ProviderManager) ProviderManager.singleton();
		for(Provider pro: selecteds) {
			pro.setActive(false);
			//er.saveOrUpdate(pro);
		}
		this.updateTable(ProviderManager.singleton().readAll());
	}
	
	@FXML
	public void addClientButtonPressed(ActionEvent event) {
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		try {
			SceneUtils.singleton().openNewFrame("/gui/provider_frame.fxml", (Stage) providerTable.getScene().getWindow(),this, FrameController.ACTION_CREATE, new Provider(true), "Nuevo proveedor");
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}	
}

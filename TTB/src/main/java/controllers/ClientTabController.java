package controllers;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.fxml.FXML;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import managers.ClientManager;
import managers.ProductManager;
import queries.Condition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.event.ActionEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import classes.Client;
import classes.ContactInfo;
import classes.Product;
import classes.Provider;

public class ClientTabController extends TabController<Client> {
	
	@FXML
	private TextField clientCifTextField;
	@FXML
	private TextField clientNameTextField;
	@FXML
	private TextField clientPhoneTextField;
	@FXML
	private TextField clientCountryTextField;
	@FXML
	private TextField clientCityTextField;
	@FXML
	private CheckBox clientActiveCheckBox;
	
	@FXML
	private TableColumn clientCifColumn;
	@FXML
	private TableColumn clientNameColumn;
	@FXML
	private TableColumn clientPhoneColumn;
	@FXML
	private TableColumn clientCountryColumn;
	@FXML
	private TableColumn clientCityColumn;
	@FXML
	private TableColumn clientActiveColumn;
	
	@FXML
	private TableView clientTable;
	
	@FXML
	private Button clearFilterButton;
	@FXML
	private Button searchProviderButton;
	@FXML
	private Button addClient;
	@FXML
	private Button activeClient;
	@FXML
	private Button desactiveClient;
	
	ObservableList<Client> clients;
		
	@FXML
	public void addClientAction(ActionEvent event) {
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		try {
			SceneUtils.singleton().openNewFrame("/gui/client_frame.fxml", (Stage) clientTable.getScene().getWindow(),this, FrameController.ACTION_CREATE, new Client(true), "Nuevo cliente");
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}
	
	@FXML
	public void initialize()
	{
		initTable();
		this.prepareEditableNodes();
	}
	
	private void initTable()
	{
		clients = FXCollections.observableArrayList();
		clients.addAll(ClientManager.singleton().readAll());
		
		clientCifColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("CIF"));
		clientNameColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("name"));
		clientPhoneColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				// TODO Auto-generated method stub
				return data.getValue().getContact().getPhoneProperty();
			}
		});
		
		clientCountryColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getContact().getCountryProperty();
			}		
		});
		clientCityColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Client, String> data){  
				return data.getValue().getContact().getCityProperty();
			}  
		});
		
		clientTable.setRowFactory(tv ->
		{
			TableRow<Client> row = new TableRow<>();
			row.setOnMouseClicked(event ->
			{
				if (event.getClickCount() == 2 && (!row.isEmpty()))
				{
					Client rowData = row.getItem();
					try
					{
						SceneUtils.singleton().openNewFrame("/gui/client_frame.fxml", (Stage) clientTable.getScene().getWindow(),this, FrameController.ACTION_VIEW_AND_MODIFY, rowData, "Cliente");
					}
					catch (IOException e)
					{
						//e.printStackTrace();
					}
				}
			});
			return row;
		});
				
		clientActiveColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getIsActiveProperty();
			}
		});
		
		clientTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		clientTable.setItems(clients);
	}
	
	@FXML
	public void searchButtonPressed(ActionEvent event)
	{
		Condition<Client>[] conditions = buildConditionArray();
		Set<Client> results = ClientManager.singleton().search(conditions);
		clients.clear();
		clients.addAll(results);
	}
	
	private Condition<Client>[] buildConditionArray()
	{
		ArrayList<Condition<Client>> conditions = new ArrayList<Condition<Client>>();
		
		if(!clientCifTextField.getText().isEmpty()) {
			conditions.add(Client.CONDITIONS.cifContains(clientCifTextField.getText()));
		}
		
		if(!clientNameTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(clientNameTextField.getText());
				conditions.add(Client.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.nameContains(clientNameTextField.getText()));
			}
		}
		
		if(!clientPhoneTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(clientPhoneTextField.getText());
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneContains(clientPhoneTextField.getText())));
			}
		}
		
		if(!clientCountryTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(clientCountryTextField.getText());
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryContains(clientCountryTextField.getText())));
			}
		}
		
		if(!clientCityTextField.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(clientCityTextField.getText());
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Client.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityContains(clientCityTextField.getText())));
			}
		}
		
		if(clientActiveCheckBox.isSelected()) {
			conditions.add(Client.CONDITIONS.activeEquals(clientActiveCheckBox.isSelected()));
		}
		
		Condition[] arrayConditions = new Condition[conditions.size()];
		conditions.toArray(arrayConditions);
		return arrayConditions;
	}

	@Override
	public void setResult(Object result, Controller elQueEnviaElResultado) {
		if(elQueEnviaElResultado instanceof FrameController)
		{
			ClientManager.singleton().saveOrUpdate(result);
			this.updateTable(ClientManager.singleton().readAll());
		}
	}

	@Override
	protected void prepareEditableNodes() {
		editableNodes.add(this.clientCifTextField);
		editableNodes.add(this.clientCityTextField);
		editableNodes.add(this.clientCountryTextField);
		editableNodes.add(this.clientNameTextField);
		editableNodes.add(this.clientPhoneTextField);
		editableNodes.add(this.clientActiveCheckBox);		
	}
	
	@FXML
	@Override
	protected void clearEditableNodes() {
		super.clearEditableNodes();
		this.searchButtonPressed(null);
	}
	
	@FXML
	public void onKeyTypedCif(KeyEvent key) 
	{
		super.onKeyTypedCif(this.clientCifTextField, key);
	}
	
	//para telefonos
	@FXML
	public void onKeyTypedPhone(KeyEvent key) 
	{
		super.onKeyTypedPhone(this.clientPhoneTextField, key);
	}
	
	private void updateTable(Set<Client> newData)
	{
		clients.clear();
		clients.addAll(newData);
	}
	
	@FXML
	public void activeClient(ActionEvent event) 
	{
		this.modifiStatus(clientTable.getSelectionModel().getSelectedItems(), true);
	}
	
	@FXML
	public void desactiveClient(ActionEvent event) {
		this.modifiStatus(clientTable.getSelectionModel().getSelectedItems(), false);
	}
	
	private void modifiStatus(ObservableList<Client> clis, boolean estatus) {
		for(Client cli: clis) {
			cli.setActive(estatus);
			ClientManager.singleton().saveOrUpdate(cli);
		}
		this.updateTable(ClientManager.singleton().readAll());
	}
}

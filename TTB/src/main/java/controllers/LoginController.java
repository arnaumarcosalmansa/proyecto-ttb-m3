package controllers;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafxutils.SceneUtils;
import utils.Login;

public class LoginController
{
	@FXML
	private Button login_button;
	@FXML
	private TextField usernameTextField;

	@FXML
	private Label failedLoginLabel;

	@FXML
	private PasswordField passwordField;

	private Login login;
	
	@FXML
	public void initialize()
	{
		login = new Login("shadow");
	}

	@FXML
	private void loginSubmit(ActionEvent action) throws Exception
	{
		String username = usernameTextField.getText();
		String password = passwordField.getText();
		
		boolean loginAccepted = login.checkLogin(username, password);
		if (loginAccepted)
		{
			Button b = (Button) action.getSource();
			SceneUtils.singleton().openSceneOnStage((Stage) b.getScene().getWindow(), "/gui/main_frame.fxml");
		}
		else
		{
			failedLoginLabel.setTextFill(Color.web("red"));
			failedLoginLabel.setText("Login incorrecto.");
		}
	}
}

package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.stage.Stage;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import java.util.Map;
import java.util.Map.Entry;

import classes.Component;
import classes.Product;

public class ComponentVisualizerController extends VisualizerController<Product>
{
	private Component componentToAdd = new Component();
	
	@FXML
	private TableColumn productIdColumn;
	@FXML
	private TableColumn productNameColumn;
	@FXML
	private TableColumn productDescriptionColumn;
	@FXML
	private TableColumn productQuantityColumn;
	@FXML
	private TableColumn productUnitOfMeasurementColumn;
	@FXML
	private TableColumn productTypeColumn;
	@FXML
	private TableView componentTable;
	
	private ObservableList<Component> components;
	
	
	@FXML
	private TextField productIdTextField;
	@FXML 
	private TextField quantityNumber;
	@FXML
	private Button addButton;
	@FXML
	private Button exitButton;
	@FXML
	private Button dropButton;
	@FXML
	private Button searchProduct;
	
	
	
	//esconder el boton de añadir lote si se esta creando el producto
	@Override
	public void setObject(Product object)
	{
		this.object = object;
		initTable();
	}
	
	private void initTable()
	{
		components = FXCollections.observableArrayList();
		
		productIdColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Component, Integer>, ObservableValue<Integer>>() {
			@Override
			public ObservableValue<Integer> call(CellDataFeatures<Component, Integer> component)
			{
				return component.getValue().getProduct().getIdProperty().asObject();
			}
        });
		
		productNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Component, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Component, String> param)
			{
				return param.getValue().getProduct().getNameProperty();
			}
        });
		

		
		productDescriptionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Component, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Component, String> param) {
				return param.getValue().getProduct().getDescriptionProperty();
			}
		});
		
		productQuantityColumn.setCellValueFactory(new PropertyValueFactory<Component, Integer>("quantity"));
		
		productUnitOfMeasurementColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Component, String>, ObservableValue<String>>()
		{
			@Override
			public ObservableValue<String> call(CellDataFeatures<Component, String> param)
			{
				return param.getValue().getProduct().getUnityMeasuraProperty();
			}
		});
		
		productTypeColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Component, String>, ObservableValue<String>>()
		{
			@Override
			public ObservableValue<String> call(CellDataFeatures<Component, String> param)
			{
				return param.getValue().getProduct().getTypeProcedure();
			}
		});
		
		componentTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		updateTable();
	}
	
	private void updateTable()
	{
		parseComponents();
		componentTable.setItems(components);
	}
	
	private void parseComponents()
	{
		components.clear();
		if(this.object.getComposicio() != null)
		{
			for(Entry<Product, Integer> componentEntry : this.object.getComposicio().entrySet())
			{
				components.add(new Component(componentEntry.getKey(), componentEntry.getValue()));
			}
		}
	}
	
	@FXML
	public void close(ActionEvent event)
	{
		Stage thisStage = (Stage) componentTable.getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void selectProduct(ActionEvent event)
	{
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		try
		{
			SceneUtils.singleton().openNewSelector("/gui/product_selector.fxml", thisStage, this, "Seleccionar componentes");
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
	}
	
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		if(whoSetsTheResult instanceof ProductSelectorController)
		{
			if(result != null)
			{
				Product p = (Product) result;
				productIdTextField.setText(p.getProductId() + "");
				componentToAdd.setProduct(p);
			}
			else
			{
				productIdTextField.setText("");
			}
		}
	}
	
	@FXML
	public void addComponent(ActionEvent event)
	{
		componentToAdd.setQuantity(Integer.valueOf(quantityNumber.getText()));
		this.object.afegirComponent(componentToAdd.getProduct(), componentToAdd.getQuantity());
		componentToAdd = new Component();
		updateTable();
	}
	
	@FXML
	public void removeComponent(ActionEvent event)
	{
		Component componente = (Component) this.componentTable.getSelectionModel().getSelectedItem();
		this.object.getComposicio().remove(componente.getProduct());
		updateTable();
	}
}

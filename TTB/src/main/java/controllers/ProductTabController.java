package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import classes.ProductType;
import classes.UnitOfMeasurement;
import classes.Batch;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import managers.ProductManager;
import queries.Condition;
import javafx.scene.control.SelectionMode;


public class ProductTabController extends TabController<Product>
{
	@FXML
	private TextField productCodeTextField;

	@FXML
	private TextField productNameTextField;

	@FXML
	private TextField productProviderTextField;

	@FXML
	private ToggleGroup productTypeToggleGroup;
	
	@FXML
	private CheckBox unitCheckBox;
	
	@FXML
	private CheckBox literCheckBox;
	
	@FXML
	private CheckBox gramCheckBox;

	@FXML
	private CheckBox stockCheckBox;
	
	@FXML
	private TextField stockTextField;
	
	@FXML
	private TextField priceTextField;
	
	@FXML
	private TableView<Product> productTable;

	@FXML
	private TableColumn productIdColumn;
	@FXML
	private TableColumn productNameColumn;
	@FXML
	private TableColumn productDescriptionColumn;
	@FXML
	private TableColumn productSellingPriceColumn;
	@FXML
	private TableColumn productTotalStockColumn;
	@FXML
	private TableColumn productMinStockColumn;
	@FXML
	private TableColumn productMeasureUnityColumn;
	@FXML
	private TableColumn productTypeColumn;
	@FXML
	private TableColumn productProviderCifColumn;
	// contiene los objetos a mostrar
	private ObservableList<Product> products;

	@FXML
	public void initialize()
	{
		prepareEditableNodes();
		prepareToggles();
		iniciarTabla();
		
		products.addAll(ProductManager.singleton().readAll());
	}
	
	@Override
	protected void prepareEditableNodes()
	{
		editableNodes.add(productCodeTextField);
		editableNodes.add(productNameTextField);
		editableNodes.add(productProviderTextField);
		editableNodes.add(stockCheckBox);
		editableNodes.add(stockTextField);
		editableNodes.add(priceTextField);

		ObservableList<Toggle> productTypeRadioButtons = productTypeToggleGroup.getToggles();
		
		for(Toggle t : productTypeRadioButtons)
		{
			editableNodes.add((Node) t);
		}
	}
	
	private void prepareToggles()
	{
		ObservableList<Toggle> types = productTypeToggleGroup.getToggles();
		for(Toggle t : types)
		{
			RadioButton rb = (RadioButton) t;
			if(rb.getText().equals("Vendible"))
			{
				rb.setUserData(ProductType.VENDIBLE);
			}
			else if(rb.getText().equals("Ingredient"))
			{
				rb.setUserData(ProductType.INGREDIENTE);
			}
			else
			{
				rb.setUserData(null);
			}
		}
		
		unitCheckBox.setUserData(UnitOfMeasurement.UNIDADES);
		literCheckBox.setUserData(UnitOfMeasurement.LITROS);
		gramCheckBox.setUserData(UnitOfMeasurement.GRAMOS);
	}

	private void iniciarTabla()
	{
		productIdColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("productId"));
		productNameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("productName"));
		productDescriptionColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("description"));
		productSellingPriceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
		productTotalStockColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
		productMinStockColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("minStock"));
		productMeasureUnityColumn.setCellValueFactory(new PropertyValueFactory<Product, UnitOfMeasurement>("unitatMesura"));
		productTypeColumn.setCellValueFactory(new PropertyValueFactory<Product, ProductType>("type"));
		// usa su methodo toString
		productProviderCifColumn.setCellValueFactory(new PropertyValueFactory<Product, Provider>("provider"));
		
		productProviderCifColumn.setCellValueFactory(new Callback<CellDataFeatures<Product, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Product, String> data) {
				StringProperty ret = data.getValue().getProvider() != null ? data.getValue().getProvider().getCIFProperty() : new SimpleStringProperty("Sin proveedor");
				return ret;
			}
		});
		
		products = FXCollections.observableArrayList();
		productTable.setRowFactory(tv ->
		{
			TableRow<Product> row = new TableRow<>();
			row.setOnMouseClicked(event ->
			{
				if (event.getClickCount() == 2 && (!row.isEmpty()))
				{
					Product rowData = row.getItem();
					try
					{
						SceneUtils.singleton().openNewFrame("/gui/product_frame.fxml", (Stage) productTable.getScene().getWindow(),this, FrameController.ACTION_VIEW_AND_MODIFY, rowData, "Producto");
					}
					catch (IOException e)
					{
						//e.printStackTrace();
					}
				}
			});
			return row;
		});
		productTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		productTable.setItems(products);
	}

	@SuppressWarnings("restriction")
	public void searchButtonPressed()
	{
		Condition<Product>[] conditionArray = buildConditionArray();
		Set<Product> results = ProductManager.singleton().search(conditionArray);
		updateTable(results);
	}

	private Condition<Product>[] buildConditionArray()
	{
		ArrayList<Condition<Product>> conditions = new ArrayList<Condition<Product>>();
		
		String productCode = productCodeTextField.getText();
		if (!productCode.isEmpty())
		{
			conditions.add(Product.CONDITIONS.idMatches(productCode));
		}

		String productName = productNameTextField.getText();
		if (!productName.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(productName);
				conditions.add(Product.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Product.CONDITIONS.nameContainString(productName));
			}
		}

		String providerCif = productProviderTextField.getText();
		if (!providerCif.isEmpty())
		{
			try
			{
				Pattern p = Pattern.compile(providerCif);
				conditions.add(Product.CONDITIONS.providerMatchesConditions(Provider.CONDITIONS.cifMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Product.CONDITIONS.providerMatchesConditions(Provider.CONDITIONS.cifContains(providerCif)));
			}
		}
		
		String stock = stockTextField.getText().trim().replace(" ", "");
		try
		{
			if(stock.startsWith(">"))
			{
				stock = stock.substring(1, stock.length());
				conditions.add(Product.CONDITIONS.stockBiggerThan(Integer.valueOf(stock)));				
			}
			else if(stock.startsWith("<"))
			{
				stock = stock.substring(1, stock.length());
				conditions.add(Product.CONDITIONS.stockLessThan(Integer.valueOf(stock)));
			}
			else
			{
				conditions.add(Product.CONDITIONS.stockEquals(Integer.valueOf(stock)));
			}
		}
		catch(Exception e)
		{
			
		}
		
		String price = priceTextField.getText().trim().replace(" ", "");
		try
		{
			if(price.startsWith(">"))
			{
				price = price.substring(1, price.length());
				conditions.add(Product.CONDITIONS.priceBiggerThan(Double.valueOf(price)));
			}
			else if(price.startsWith("<"))
			{
				price = price.substring(1, price.length());
				conditions.add(Product.CONDITIONS.priceLessThan(Double.valueOf(price)));
			}
			else
			{
				conditions.add(Product.CONDITIONS.priceEquals(Double.valueOf(price)));
			}
		}
		catch(Exception e)
		{
			
		}
		
		ArrayList<UnitOfMeasurement> unidades = new ArrayList<UnitOfMeasurement>();
		
		if(unitCheckBox.isSelected()) unidades.add((UnitOfMeasurement) unitCheckBox.getUserData());
		if(literCheckBox.isSelected()) unidades.add((UnitOfMeasurement) literCheckBox.getUserData());
		if(gramCheckBox.isSelected()) unidades.add((UnitOfMeasurement) gramCheckBox.getUserData());
		
		if(unidades.size() > 0)
		{
			UnitOfMeasurement[] auxUnidades = new UnitOfMeasurement[unidades.size()];
			unidades.toArray(auxUnidades);
			conditions.add(Product.CONDITIONS.measureUnityIn(auxUnidades));	
		}
		
		RadioButton selectedRadioButton = (RadioButton) productTypeToggleGroup.getSelectedToggle();

		try
		{
			ProductType type = ProductType.valueOf(selectedRadioButton.getText().toUpperCase());
			conditions.add(Product.CONDITIONS.typeEquals(type));
		}
		catch (Exception e)
		{
			// e.printStackTrace();
		}

		boolean brokenStock = stockCheckBox.isSelected();

		if (brokenStock)
		{
			conditions.add(Product.CONDITIONS.stockIsBroken());
		}
		
		Condition[] conditionArray = new Condition[conditions.size()];
		conditions.toArray(conditionArray);
		
		return conditionArray;
	}

	private void updateTable(Set<Product> newData)
	{
		products.clear();
		products.addAll(newData);
	}

	@FXML
	public void selectProvider(ActionEvent event)
	{
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		try
		{
			SceneUtils.singleton().openNewSelector("/gui/provider_selector.fxml", thisStage, this, "Seleccionar proveedor");
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}
	
	@FXML
	public void clearFilters(ActionEvent e)
	{
		this.clearEditableNodes();
		this.unitCheckBox.setSelected(true);
		this.literCheckBox.setSelected(true);
		this.gramCheckBox.setSelected(true);
		this.productTypeToggleGroup.selectToggle(productTypeToggleGroup.getToggles().get(0));
		this.searchButtonPressed();
	}
	
	/*public void onKeyTyped(KeyEvent e)
	{
		super.onKeyTyped(e);
	}*/

	public void actionEvent(ActionEvent e)
	{

	}
	
	/*@FXML
	public void integerComparisonKeyTyped(KeyEvent key)
	{
		super.integerComparisonKeyTyped(key);
	}*/
	
	@FXML
	public void doubleComparisonKeyTyped(KeyEvent key)
	{
		super.doubleComparisonKeyTyped(this.priceTextField, key);
	}
	

	@Override
	public void setResult(Object result, Controller elQueEnviaElResultado)
	{
		if(elQueEnviaElResultado instanceof FrameController)
		{
			ProductManager.singleton().saveOrUpdate(result);
			this.updateTable(ProductManager.singleton().readAll());
		}
		else if(elQueEnviaElResultado instanceof SelectorController)
		{
			if(result != null)
			{
				Provider p = (Provider) result;
				this.productProviderTextField.setText(p.getCIF());
			}
			else
			{
				this.productProviderTextField.setText("");
			}
		}
	}
	
	@FXML
	public void create()
	{
		try
		{
			SceneUtils.singleton().openNewFrame("/gui/product_frame.fxml", (Stage) productTable.getScene().getWindow(), this, FrameController.ACTION_CREATE, new Product(), "Nuevo producto");
		}
		catch (IOException e)
		{
			//e.printStackTrace();
		}
	}
	
	@FXML
	@Override
	public void integerComparisonKeyTyped(KeyEvent key) {
		super.integerComparisionAdvancedKeyTyped(this.stockTextField, key);
	}
}
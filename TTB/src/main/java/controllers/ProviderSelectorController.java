package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import classes.ProductType;
import classes.UnitOfMeasurement;
import classes.ContactInfo;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import managers.ProductManager;
import managers.ProviderManager;
import queries.Condition;
import javafx.scene.control.SelectionMode;

public class ProviderSelectorController extends SelectorController<Provider> {

	@FXML
	private TableColumn providerCifColumn;
	@FXML
	private TableColumn providerNameColumn;
	@FXML
	private TableColumn providerPhoneColumn;
	@FXML
	private TableColumn providerCountryColumn;
	@FXML
	private TableColumn providerCityColumn;
	@FXML
	private TableColumn providerActiveColumn;
	@FXML
	private TableView providerTable;
	private ObservableList<Provider> providers;
	@FXML
	private TextField inputCifProvider;
	@FXML
	private TextField inputNameProvider;
	@FXML
	private TextField inputPhoneProvider;
	@FXML
	private TextField inputCountryProvider;
	@FXML
	private TextField inputLocationProvider;
	@FXML
	private CheckBox inputActive;
	@FXML
	private Button clearFilters;
	
	@FXML
	private void clearFiltersInputs() {
		inputCifProvider.setText("");
		inputNameProvider.setText("");
		inputPhoneProvider.setText("");
		inputCountryProvider.setText("");
		inputLocationProvider.setText("");
		inputActive.setSelected(false);
		searchButtonPressed();
	}
	
	@FXML
	private void searchButtonPressed() {
		Condition<Provider>[] conditions = buildConditionArray();
		
		Set<Provider> results = ProviderManager.singleton().search(conditions);

		updateTable(results);
	}
	
	private void updateTable(Set<Provider> newData) {
		providers.clear();
		providers.addAll(newData);
	}
	
	private Condition<Provider>[] buildConditionArray(){
		
		ArrayList<Condition<Provider>> conditions = new ArrayList<Condition<Provider>>();
		
		if(!inputCifProvider.getText().isEmpty()) {
			conditions.add(Provider.CONDITIONS.cifEquals(inputCifProvider.getText()));
		}
		
		if(!inputNameProvider.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(inputNameProvider.getText());
				conditions.add(Provider.CONDITIONS.nameMatches(p));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.nameMatches(inputNameProvider.getText()));
			}
		}
		
		if(!inputPhoneProvider.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(inputPhoneProvider.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.phoneContains(inputPhoneProvider.getText())));
			}
		}
		
		if(!inputCountryProvider.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(inputCountryProvider.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.countryContains(inputCountryProvider.getText())));
			}
		}
		if(!inputLocationProvider.getText().isEmpty()) {
			try
			{
				Pattern p = Pattern.compile(inputLocationProvider.getText());
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityMatches(p)));
			}
			catch(Exception e)
			{
				conditions.add(Provider.CONDITIONS.contactInfoMatchConditions(ContactInfo.CONDITIONS.cityContains(inputLocationProvider.getText())));
			}
		}
		
		if(inputActive.isSelected()) {
			conditions.add(Provider.CONDITIONS.activeEquals(inputActive.isSelected()));
		}
		
		Condition[] cons = new Condition[conditions.size()];
		conditions.toArray(cons);
		return cons;
	}
	
	@FXML
	private void initialize()
	{
		initTable();
	}
	
	private void initTable()
	{
		providerCifColumn.setCellValueFactory(new PropertyValueFactory<Provider, String>("CIF"));
		providerNameColumn.setCellValueFactory(new PropertyValueFactory<Provider, String>("nameProveidor"));
		providerActiveColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				return data.getValue().getIsActiveProperty();
			}
		});
		providerCityColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				return data.getValue().getContact().getCityProperty();
			}
			
		});
		
		providerCountryColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data) {
				return data.getValue().getContact().getCountryProperty();
			}
			
		});
		
		providerPhoneColumn.setCellValueFactory(new Callback<CellDataFeatures<Provider, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Provider, String> data){  
				return data
						.getValue()
						.getContact()
						.getPhoneProperty();
			}  
		});
		
		providers = FXCollections.observableArrayList();
		providers.addAll(ProviderManager.singleton().readAll());
		
		providerTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		providerTable.setItems(providers);
	}
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		
	}

	@FXML
	public void acceptPressed(ActionEvent e)
	{
		Provider p = (Provider) providerTable
				.getSelectionModel()
				.getSelectedItem();
		parent.setResult(p, this);
		
		Stage thisStage = (Stage) providerTable.getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void cancelar(ActionEvent e)
	{
		parent.setResult(null, this);
		Stage thisStage = (Stage) providerTable.getScene().getWindow();
		thisStage.close();
	}
	
	@FXML
	public void onKeyTypedPhone(KeyEvent key) {
		super.onKeyTypedPhone(this.inputPhoneProvider, key);
	}
	
	@FXML
	public void onKeyTypedCif(KeyEvent key) {
		super.onKeyTypedCif(this.inputCifProvider, key);
	}

	@Override
	protected void prepareEditableNodes() {
		// TODO Auto-generated method stub
		
	}
}

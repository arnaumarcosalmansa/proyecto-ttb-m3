package controllers;

public abstract class FrameController<T> extends Controller<T>
{
	public static final int ACTION_VIEW_AND_MODIFY = 0;
	public static final int ACTION_CREATE = 1;
	
	protected boolean isEditing = false;
	protected int action = ACTION_VIEW_AND_MODIFY;
	
	public boolean isEditing()
	{
		return isEditing;
	}

	public void setEditing(boolean isEditing)
	{
		this.isEditing = isEditing;
	}

	public int getAction()
	{
		return action;
	}

	public void setAction(int action)
	{
		this.action = action;
	}
	
}

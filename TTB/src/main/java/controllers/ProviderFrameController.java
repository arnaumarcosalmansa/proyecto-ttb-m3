package controllers;

import classes.ContactInfo;
import classes.Coordinates;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import classes.UnitOfMeasurement;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafxutils.SceneUtils;
import managers.ProviderManager;

public class ProviderFrameController extends FrameController<Provider> {
	@FXML
	private TextField providerIdTextField;
	@FXML
	private TextField providerNameTextField;
	@FXML
	private TextField providerPhoneTextField;
	@FXML
	private TextField providerCountryTextField;
	@FXML
	private TextField providerPoblacionTextField;
	@FXML
	private TextField providerAddressTextField;
	@FXML
	private TextField providerCoordsTextField;
	@FXML
	private TextField providerContacteTextField;
	
	@FXML
	private CheckBox providerActiveCB;
	
	@FXML
	private Button cancelButton;
	@FXML
	private Button saveButton;
	@FXML
	private Button modifyButton;
	
	private Provider object;
	
	@FXML
	private Label errorData;
	
	@FXML
	public void initialize() {
		this.prepareEditableNodes();
	}
	
	@Override
	public void setObject(Provider object) {
		this.object = object;
		this.providerIdTextField.setText(this.object.getCIF());
		if(!this.object.getCIF().isEmpty() && this.object.getCIF() != null) providerIdTextField.setEditable(false);
		this.providerNameTextField.setText(this.object.getNameProveidor());
		this.providerPhoneTextField.setText(this.object.getContact().getPhone());
		this.providerCountryTextField.setText(this.object.getContact().getCountry());
		this.providerAddressTextField.setText(this.object.getContact().getAddress());
		this.providerPoblacionTextField.setText(this.object.getContact().getPoblacio());
		this.providerActiveCB.setSelected(this.object.isActive());
		this.providerCoordsTextField.setText(this.object.getContact().getCoords().getLatitude() + " " + this.object.getContact().getCoords().getLongitude());
		this.providerContacteTextField.setText(this.object.getContact().getPersonaContacte());
	}
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult) {
		
	}

	@Override
	protected void prepareEditableNodes() {
		editableNodes.add(this.providerIdTextField);
		editableNodes.add(this.providerNameTextField);
		editableNodes.add(this.providerPhoneTextField);
		editableNodes.add(this.providerCountryTextField);
		editableNodes.add(this.providerPoblacionTextField);
		editableNodes.add(this.providerAddressTextField);
		editableNodes.add(this.providerCoordsTextField);
		editableNodes.add(this.providerActiveCB);
		editableNodes.add(providerContacteTextField);
	}
	
	@Override
	public void setAction(int action)
	{
		super.setAction(action);
		if(action == FrameController.ACTION_CREATE)
		{
			this.setEditing(true);
		}
		else
		{
			this.setEditing(false);
		}
	}

	@Override
	public void setEditing(boolean isEditing)
	{
		super.setEditing(isEditing);
		boolean disabled = !isEditing;

		for(Node n : editableNodes)
		{
			n.setDisable(disabled);
		}
		
		modifyButton.setDisable(isEditing);
		modifyButton.setVisible(disabled);
		
		cancelButton.setDisable(disabled);
		cancelButton.setVisible(isEditing);
		
		saveButton.setDisable(disabled);
		saveButton.setVisible(isEditing);
	}
	
	@FXML
	public void modify(ActionEvent event) 
	{
		this.setEditing(true);
	}
	
	@FXML
	public void save(ActionEvent event) 
	{
		this.errorData.setVisible(false);
		Provider pro = this.parseForm();
		
		if(pro != null) {
			this.setEditing(false);
			
			if(this.getAction() == FrameController.ACTION_CREATE)
			{
				Stage frameStage = (Stage) saveButton.getScene().getWindow();
				frameStage.close();
			}
			
			parent.setResult(this.object, this);
		}
	}
	
	@FXML
	public void cancel(ActionEvent event) 
	{
		this.errorData.setVisible(false);
		
		this.setEditing(false);
		
		if(this.getAction() == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		this.setObject(this.object);
	}
	
	//valida las cordenadas del proveedor
	@FXML
	public void onKeyTypedCoords(KeyEvent key) 
	{
		super.onKeyTypedCoords(this.providerCoordsTextField, key);
	}
	
	//valida el cif del proveedor
	@FXML
	public void onKeyTypedCif(KeyEvent key) 
	{
		super.onKeyTypedCif(this.providerIdTextField, key);
	}
	
	//valida el telefono del proveedor
	@FXML
	public void onKeyTypedPhone(KeyEvent key) 
	{
		super.onKeyTypedPhone(this.providerPhoneTextField, key);
	}
	
	private Provider parseForm()
	{
		boolean hasMissingData = false;
		String id = "";
		String name = providerNameTextField.getText();
		
		if(providerIdTextField.getText() == null || providerIdTextField.getText().isEmpty()) {
			providerIdTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			if(providerIdTextField.getText().length() == 9) {
				id = providerIdTextField.getText();
				providerIdTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			else {
				providerIdTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el cif no cumple con la longitud requerida.");
				this.errorData.setVisible(true);
			}
		}
		
		if(providerNameTextField.getText() == null || providerNameTextField.getText().isEmpty()) {
			providerNameTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			providerNameTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		
		String phone = "";
		if(providerPhoneTextField.getText() == null || providerPhoneTextField.getText().isEmpty()) {
			providerPhoneTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else{
			if(providerPhoneTextField.getText().length() == 9) {
				phone = providerPhoneTextField.getText();
				providerPhoneTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			else {
				providerPhoneTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el telefono no cumple la longitud requerida.");
				this.errorData.setVisible(true);
			}
		}
		
		String country = providerCountryTextField.getText();
		if(providerCountryTextField.getText() == null || providerCountryTextField.getText().isEmpty()) {
			providerCountryTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			providerCountryTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		String poblacio = providerPoblacionTextField.getText();
		if(providerPoblacionTextField.getText() == null || providerPoblacionTextField.getText().isEmpty()) {
			providerPoblacionTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			providerPoblacionTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		String address = providerAddressTextField.getText();
		if(providerAddressTextField.getText() == null || providerAddressTextField.getText().isEmpty()) {
			providerAddressTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			providerAddressTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		String cordinates = providerCoordsTextField.getText();
		double latitud = 0;
		double longitud = 0;
		if((this.providerCoordsTextField.getText().isEmpty() || this.providerCoordsTextField.getText() == null))
		{
			this.providerCoordsTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
			//this.providerCoordsTextField.setText(this.object.getContact().getCoords().getLatitude() + " " + this.object.getContact().getCoords().getLongitude());
		}
		else {
			if(!Coordinates.validateCoordinates(this.providerCoordsTextField.getText())){
				hasMissingData = true;
				this.errorData.setText("Error: se espera 'latitud longitud' con formato 'xx.xx xx.xx' en las cordenadas.");
				this.errorData.setVisible(true);
			}
			else {
				try {
					latitud = Double.parseDouble(cordinates.split(" ")[0]);
					longitud = Double.parseDouble(cordinates.split(" ")[1]);
					providerCoordsTextField.setStyle(SceneUtils.DEFAULT_STYLE);
				}
				catch(Exception e) {
					hasMissingData = true;
					this.errorData.setText("Error: se espera 'latitud longitud' con formato 'xx.xx xx.xx' en las cordenadas.");
					this.errorData.setVisible(true);
				}
			}
		}
		
		boolean active = providerActiveCB.isSelected();
		
		String contacte = providerContacteTextField.getText();
		if(providerContacteTextField.getText() == null || providerContacteTextField.getText().isEmpty()) {
			this.providerContacteTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			providerContacteTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		Provider result = null;
		if(!hasMissingData)
		{
			Coordinates coords = new Coordinates(latitud, longitud);
			ContactInfo contact = new ContactInfo();
			result = this.object;
			contact.fill(address, poblacio, country, contacte, phone, coords);
			result.fill(id, name, active, contact);
		}
		return result;
	}
}

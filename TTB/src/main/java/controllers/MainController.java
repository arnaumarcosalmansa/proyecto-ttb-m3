package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import classes.Client;
import classes.Order;
import classes.Product;
import classes.Provider;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class MainController
{
	@FXML
	private TabPane tabPanel;

	@FXML
	public void initialize()
	{
		Tab tab = new Tab();
		Tab tab2 = new Tab();
		Tab tab3 = new Tab();
		Tab tab4 = new Tab();

		try
		{
			tab.setContent(FXMLLoader.load(this.getClass().getResource(Product.GUI_TAB_PATH)));
			tab.setText("Productos");
			tabPanel.getTabs().addAll(tab);

			tab2.setContent(FXMLLoader.load(this.getClass().getResource(Order.GUI_TAB_PATH)));
			tab2.setText("Pedidos");
			tabPanel.getTabs().addAll(tab2);
			
			tab3.setContent(FXMLLoader.load(this.getClass().getResource(Client.GUI_TAB_PATH)));
			tab3.setText("Clientes");
			tabPanel.getTabs().addAll(tab3);
			
			tab4.setContent(FXMLLoader.load(this.getClass().getResource(Provider.GUI_TAB_PATH)));
			tab4.setText("Proveedores");
			tabPanel.getTabs().addAll(tab4);
		}
		catch (IOException e)
		{
			//e.printStackTrace();
		}
	}
}

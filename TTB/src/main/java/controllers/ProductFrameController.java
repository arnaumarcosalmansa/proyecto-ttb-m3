package controllers;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.stage.Stage;
import javafxutils.SceneUtils;
import managers.ProviderManager;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.RadioButton;

import java.io.IOException;
import java.util.ArrayList;

import classes.Product;
import classes.ProductType;
import classes.Provider;
import classes.UnitOfMeasurement;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;



public class ProductFrameController extends FrameController<Product>
{
	@FXML
	private TextField productIdTextField;
	
	@FXML
	private TextField productNameTextField;
	
	@FXML
	private TextArea productDescriptionTextArea;
	
	@FXML
	private TextField productQuantityTextField;
	
	@FXML
	private TextField productPriceTextField;
	
	@FXML
	private TextField productStockTextField;
	
	@FXML
	private TextField productMinStockTextField;
	
	@FXML
	private TextField productProviderTextField;
	
	@FXML
	private Button productProviderButton;
	
	@FXML
	private ToggleGroup productMeasureUnityToggleGroup;
	
	@FXML
	private ToggleGroup productTypeToggleGroup;
	@FXML
	private RadioButton literRadioButton;
	@FXML
	private RadioButton gramRadioButton;
	@FXML
	private RadioButton unityRadioButton;
	@FXML
	private RadioButton salableRadioButton;
	@FXML
	private RadioButton ingredientRadioButton;
	
	
	@FXML
	private Button viewBatchesButton;
	
	@FXML
	private Button modifyButton;
	
	@FXML
	private Button saveButton;
	
	@FXML
	private Button cancelButton;
	
	@FXML
	private Label errorData;
	
	public void initialize()
	{
		productProviderTextField.setDisable(true);
		productIdTextField.setDisable(true);
		prepareEditableNodes();
		prepareRadioButtons();
	}
	

	protected void prepareEditableNodes()
	{
		editableNodes.add(productIdTextField);
		editableNodes.add(productNameTextField);
		editableNodes.add(productDescriptionTextArea);
		editableNodes.add(productQuantityTextField);
		editableNodes.add(productPriceTextField);
		editableNodes.add(productStockTextField);
		editableNodes.add(productMinStockTextField);
		editableNodes.add(productProviderTextField);
		
		editableNodes.add(productProviderButton);
		
		ObservableList<Toggle> measureUnityRadioButtons =  productMeasureUnityToggleGroup.getToggles();
		
		for(Toggle t : measureUnityRadioButtons)
		{
			editableNodes.add((Node) t);
		}
		
		ObservableList<Toggle> typeRadioButtons =  productTypeToggleGroup.getToggles();
	
		for(Toggle t : typeRadioButtons)
		{
			editableNodes.add((Node) t);
		}
	}
	
	public void prepareRadioButtons()
	{
		literRadioButton.setUserData(UnitOfMeasurement.LITROS);
		gramRadioButton.setUserData(UnitOfMeasurement.GRAMOS);
		unityRadioButton.setUserData(UnitOfMeasurement.UNIDADES);
		
		salableRadioButton.setUserData(ProductType.VENDIBLE);
		ingredientRadioButton.setUserData(ProductType.INGREDIENTE);
	}
	
	@Override
	public void setAction(int action)
	{
		super.setAction(action);
		if(action == FrameController.ACTION_CREATE)
		{
			this.setEditing(true);
			viewBatchesButton.setVisible(false);
		}
		else
		{
			this.setEditing(false);
		}
	}

	@Override
	public void setEditing(boolean isEditing)
	{
		super.setEditing(isEditing);
		boolean disabled = !isEditing;

		for(Node n : editableNodes)
		{
			n.setDisable(disabled);
		}
		
		modifyButton.setDisable(isEditing);
		modifyButton.setVisible(disabled);
			
		cancelButton.setDisable(disabled);
		cancelButton.setVisible(isEditing);
		
		saveButton.setDisable(disabled);
		saveButton.setVisible(isEditing);
				
	}
	
	
	private Product parseForm()
	{
		boolean hasMissingData = false;
		String id = productIdTextField.getText();
		String name = productNameTextField.getText();
		if(productNameTextField.getText() == null || productNameTextField.getText().isEmpty()) {
			productNameTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			productNameTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		String description = productDescriptionTextArea.getText();
		if(description == null) {
			description = "";
		}
		else {
			productDescriptionTextArea.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		String quantityString = productQuantityTextField.getText();
		Double quantity = null;
		if(productQuantityTextField.getText() == null || productQuantityTextField.getText().isEmpty()) {
			productQuantityTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}else {
			if(super.validateDouble(quantityString)) {
				productQuantityTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: la cantidad no cumple con un formato double 'xx.xx'");
			}
			else {
				try
				{
					quantity = Double.valueOf(quantityString);
					productQuantityTextField.setStyle(SceneUtils.DEFAULT_STYLE);
				}
				catch(Exception e)
				{
					productQuantityTextField.setStyle(SceneUtils.ERROR_STYLE);
					hasMissingData = true;
				}
			}
		}
		
		String priceString = productPriceTextField.getText();
		Double price = null;
		if(productPriceTextField.getText() == null || productPriceTextField.getText().isEmpty()) {
			productPriceTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		} else {
			if(super.validateDouble(priceString)) {
				productPriceTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el precio no cumple con un formato double 'xx.xx'.");
			}
			else {
				try
				{
					price = Double.valueOf(priceString);
					productPriceTextField.setStyle(SceneUtils.DEFAULT_STYLE);
				}
				catch(Exception e)
				{
					productPriceTextField.setStyle(SceneUtils.ERROR_STYLE);
					hasMissingData = true;
				}
			}
		}
		
		String stockString = productStockTextField.getText();
		Integer stock = null;
		if(productStockTextField.getText() == null || productStockTextField.getText().isEmpty()) {
			productStockTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		} else {
			try
			{
				stock = Integer.valueOf(stockString);
				productStockTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			catch(Exception e)
			{
				productStockTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
			}
		}
		
		String minStockString = productMinStockTextField.getText();
		Integer minStock = null;
		if(productMinStockTextField.getText() == null || productMinStockTextField.getText().isEmpty()) {
			productMinStockTextField.setStyle(SceneUtils.ERROR_STYLE);
		} else {
			try
			{
				minStock = Integer.valueOf(minStockString);
				productMinStockTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			catch(Exception e)
			{
				productMinStockTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
			}
		}
		
		String providerCif = productProviderTextField.getText();
		Provider provider = null;
		if(productProviderTextField.getText() == null || productProviderTextField.getText().isEmpty()) {
			productProviderTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		} else {
			provider = (Provider) ProviderManager.singleton().searchOne(Provider.CONDITIONS.cifEquals(providerCif));
			productProviderTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		UnitOfMeasurement measureUnity = null;
		ObservableList<Toggle> measureUnityRadioButtons =  productMeasureUnityToggleGroup.getToggles();
		boolean goOn = true;
		for(int i = 0; goOn && i < measureUnityRadioButtons.size(); i++)
		{
			Toggle t = measureUnityRadioButtons.get(i);
			if(t.isSelected())
			{
				measureUnity = (UnitOfMeasurement) t.getUserData();
				goOn = false;
			}
		}
		
		ProductType productType = null;
		goOn = true;
		ObservableList<Toggle> typeRadioButtons =  productTypeToggleGroup.getToggles();
		for(int i = 0; goOn && i < typeRadioButtons.size(); i++)
		{
			Toggle t = typeRadioButtons.get(i);
			if(t.isSelected())
			{
				productType = (ProductType) t.getUserData();
				goOn = false;
			}
		}
		
		Product result = null;
		if(!hasMissingData)
		{
			result = this.object;
			result.fill(name, description, stock, minStock, measureUnity, productType, provider, price, quantity);
		}
		return result;
	}
	
	@FXML
	public void cancel(ActionEvent event) 
	{
		this.setEditing(false);
		
		if(this.getAction() == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		this.setObject(this.object);
	}
	
	@FXML
	public void save(ActionEvent event)
	{
		Product p = this.parseForm();		
		if(p == null) return;
		
		this.setEditing(false);
		
		if(this.getAction() == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		parent.setResult(p, this);
	}
	
	@FXML
	public void modify(ActionEvent event)
	{
		this.setEditing(true);
	}
	
	@Override
	public void setObject(Product p)
	{
		super.setObject(p);
		productIdTextField.setText(object.getProductId().toString());
		productNameTextField.setText(object.getProductName());
		productDescriptionTextArea.setText(object.getDescription());
		productQuantityTextField.setText(object.getPes() + "");
		productPriceTextField.setText(object.getPrice() + "");
		productStockTextField.setText(object.getStock().toString());
		productMinStockTextField.setText(object.getMinStock().toString());
		
		if(object.getProvider() == null || object.getProvider().getCIF() == null)
		{
			productProviderTextField.setText("");
		}
		else
		{
			productProviderTextField.setText(object.getProvider().getCIF());
		}
	}
	
	@FXML
	public void viewBatches(ActionEvent event)
	{
		try
		{
			String nombre = "Lotes del producto " + this.object.getProductId() + " - " + this.object.getProductName();
			SceneUtils.singleton().openNewVisualizer("/gui/batch_visualizer.fxml", (Stage) this.productIdTextField.getScene().getWindow(), this, object, nombre);
		}
		catch (IOException e)
		{
			//e.printStackTrace();
		}
	}
	
	@FXML
	public void viewComposition(ActionEvent event)
	{
		try
		{
			String nombre = "Composición del producto " + this.object.getProductId() + " - " + this.object.getProductName();
			SceneUtils.singleton().openNewVisualizer("/gui/component_visualizer.fxml", (Stage) this.productIdTextField.getScene().getWindow(), this, object, nombre);
		}
		catch(IOException e)
		{
			//e.printStackTrace();
		}
	}


	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		if(whoSetsTheResult instanceof ProviderSelectorController)
		{
			if(result != null)
			{
				Provider provider = (Provider) result;
				productProviderTextField.setText(provider.getCIF());
			}
			else
			{
				productProviderTextField.setText("");
			}
		}
	}
	
	@FXML
	public void selectProvider(ActionEvent event)
	{
		Stage thisStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
		try
		{
			SceneUtils.singleton().openNewSelector("/gui/provider_selector.fxml", thisStage, this, "Seleccionar proveedor");
		}
		catch (IOException e)
		{
			//e.printStackTrace();
		}
	}
	
	/*@FXML
	@Override
	public void onKeyTyped(KeyEvent key) {
		super.onKeyTyped(key);
	}*/
	
	@FXML
	public void onKeyTypedDoublePrice(KeyEvent key) 
	{
		super.onKeyTypedDouble(this.productPriceTextField, key);
	}
	
	@FXML
	public void onKeyTypedDoubleQuantity(KeyEvent key) 
	{
		super.onKeyTypedDouble(this.productQuantityTextField, key);
	}
}

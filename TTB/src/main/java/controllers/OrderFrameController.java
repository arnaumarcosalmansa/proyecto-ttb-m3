package controllers;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;
import java.util.List;

import classes.Client;
import classes.Component;
import classes.Order;
import classes.OrderLine;
import classes.OrderStatus;
import classes.Product;
import classes.ProductType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import managers.ClientManager;
import managers.ProductManager;

public class OrderFrameController extends FrameController<Order>
{

	@FXML
	private TextField orderIdTextField;
	@FXML
	private TextField clientIdTextField;
	@FXML
	private TextField orderDateTextField;
	@FXML
	private TextField orderDeliveryDateTextField;
	@FXML
	private TextField orderShippingTextField;
	
	@FXML
	private RadioButton pendingRadioButton;
	@FXML
	private RadioButton preparedRadioButton;
	@FXML
	private RadioButton transportedRadioButton;
	@FXML
	private RadioButton deliveredRadioButton;
	
	@FXML
	private ToggleGroup orderStatusToggleGroup;
	
	@FXML
	private Button modifyButton;
	@FXML
	private Button saveButton;
	@FXML
	private Button cancelButton;
	
	@FXML
	private Button selectClientButton;
	@FXML
	private Button selectProductButton;
	
	@FXML
	private Button addOrderLineButton;
	@FXML
	private Button removeOrderLineButton;
	
	@FXML
	private TableColumn lineNumberColumn;
	@FXML
	private TableColumn lineProductIdColumn;
	@FXML
	private TableColumn lineProductNameColumn;
	@FXML
	private TableColumn lineQuantityColumn;
	@FXML
	private TableColumn linePriceColumn;
	
	@FXML
	private TableView lineTable;
	
	private ObservableList lines;
	
	@FXML
	private TextField totalTextField;
	
	@FXML
	private TextField productIdTextField;
	@FXML
	private TextField quantityTextField;
	
	@FXML
	private Label errorData;
	
	private OrderLine temporaryOrderLine = new OrderLine();
	
	public void initialize()
	{
		prepareEditableNodes();
		prepareRadioButtons();
	}
	
	@Override
	public void setObject(Order object)
	{
		super.setObject(object);
		loadContent(object);
		initTable();
	}
	
	private void loadContent(Order object)
	{
		orderIdTextField.setText(object.getId() + "");
		clientIdTextField.setText(object.getClient().getCIF() + "");
		orderDateTextField.setText(object.getOrderDate() + "");
		orderDeliveryDateTextField.setText(object.getDeliveryDate() + "");
		orderShippingTextField.setText(object.getShipping() + "");
		
		for(Toggle t : orderStatusToggleGroup.getToggles())
		{
			if(t.getUserData().equals(object.getStatus()))
			{
				t.setSelected(true);
			}
		}
	}
	
	private Order parseForm() {
		
		boolean hasMissingData = false;
		String clientCif = ""; //
		Integer orderId = 0; //
		Integer productId = 0; //
		LocalDate dataEntrega = null; //
		LocalDate dateCreacio = null; //
		Double portes = 0.0; //
		
		Order ordre = null;
		errorData.setVisible(false);
		clientCif = clientIdTextField.getText();
		if(this.clientIdTextField.getText().isEmpty() || this.clientIdTextField.getText() == null) {
			clientIdTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			clientIdTextField.setStyle(SceneUtils.DEFAULT_STYLE);
		}
		
		
		if(orderIdTextField.getText().isEmpty() || orderIdTextField.getText() == null) {
			orderIdTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			try {
				orderId = Integer.parseInt(orderIdTextField.getText());
				orderIdTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			catch(Exception e) {
				orderIdTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
			}
		}
		
		if(orderDateTextField.getText().isEmpty() || orderDateTextField.getText() == null) {
			orderDateTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			try {
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				dateCreacio = LocalDate.parse(orderDateTextField.getText(), dtf);
				orderDateTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			catch(Exception e) {
				orderDateTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el formato de la fecha de entrega es incorrecto, 'yyyy-MM-dd'");
				errorData.setVisible(true);
			}
		}
		
		if(orderDeliveryDateTextField.getText() == null || orderDeliveryDateTextField.getText().isEmpty()) {
			orderDeliveryDateTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			try {
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				dataEntrega = LocalDate.parse(orderDeliveryDateTextField.getText(), dtf);
				orderDeliveryDateTextField.setStyle(SceneUtils.DEFAULT_STYLE);
				if(dataEntrega.isBefore(dateCreacio))
				{
					this.errorData.setText("Error: la fecha de entrega no puede ser anterior a la actual");
					errorData.setVisible(true);
				}
			}
			catch(Exception e) {
				orderDeliveryDateTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el formato de la fecha de entrega es incorrecto, 'yyyy-MM-dd'");
				errorData.setVisible(true);
			}
		}
		
		if(this.orderShippingTextField.getText() == null || this.orderShippingTextField.getText().isEmpty()) {
			orderShippingTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {

			try {
				portes = Double.parseDouble(orderShippingTextField.getText());
				orderShippingTextField.setStyle(SceneUtils.DEFAULT_STYLE);
			}
			catch(Exception e) {
				orderShippingTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				errorData.setText("Error: formato en el double de portes 'xx.xx'");
				errorData.setVisible(true);
			}
		}
		
		OrderStatus status = (OrderStatus) this.orderStatusToggleGroup.getSelectedToggle().getUserData();
		
		if(!hasMissingData) {
			
			Client client = (Client) ClientManager.singleton().searchOne(Client.CONDITIONS.cifEquals(clientCif));
			Order result = new Order();
		
			result.fill(orderId, client, dateCreacio, dataEntrega, status, portes, this.object.getLinies());
			ordre = result;
		}
		
		return ordre;
	}
	
	private void initTable()
	{
		lineNumberColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OrderLine, Integer>, ObservableValue<Integer>>()
		{
			@Override
			public ObservableValue<Integer> call(CellDataFeatures<OrderLine, Integer> param)
			{
				return new SimpleIntegerProperty(lines.indexOf(param.getValue()) + 1).asObject();
			}
		});
		lineProductIdColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OrderLine, Integer>, ObservableValue<Integer>>()
		{
			@Override
			public ObservableValue<Integer> call(CellDataFeatures<OrderLine, Integer> param)
			{
				return param.getValue().getProducte().getIdProperty().asObject();
			}
		});
		lineProductNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<OrderLine, String>, ObservableValue<String>>()
		{
			@Override
			public ObservableValue<String> call(CellDataFeatures<OrderLine, String> param)
			{
				return param.getValue().getProducte().getNameProperty();
			}
		});
		lineQuantityColumn.setCellValueFactory(new PropertyValueFactory<OrderLine, Integer>("quantity"));
		linePriceColumn.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("price"));
		
		lines = FXCollections.observableArrayList();
		updateTable();
	}
	
	private void updateTable()
	{	
		lines.clear();
		lines.addAll(this.object.getLinies());
		lineTable.setItems(lines);
		updateTotal();
	}
	
	private void updateTotal()
	{
		double total = 0;
		for(Object o : lines)
		{
			OrderLine ol = (OrderLine) o;
			total += ol.getPrice();
		}
		DecimalFormat df = new DecimalFormat("#.00"); 
		totalTextField.setText(df.format(total));
	}
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult)
	{
		if(whoSetsTheResult instanceof ProductSelectorController)
		{
			if(result != null)
			{
				Product p = (Product) result;
				productIdTextField.setText(p.getProductId() + "");
				temporaryOrderLine.setProducte(p);
			}
			else
			{
				productIdTextField.setText("");
			}
		}
		else if(whoSetsTheResult instanceof ClientSelectorController)
		{
			if(result != null)
			{
				Client c = (Client) result;
				String cif = c.getCIF();
				cif = cif != null ? cif : "";
				clientIdTextField.setText(cif);
			}
			else
			{
				clientIdTextField.setText("");
			}
		}
	}

	@Override
	protected void prepareEditableNodes()
	{
		editableNodes.add(orderIdTextField);
		editableNodes.add(clientIdTextField);
		editableNodes.add(orderDateTextField);
		editableNodes.add(orderDeliveryDateTextField);
		editableNodes.add(orderShippingTextField);
		
		editableNodes.add(productIdTextField);
		editableNodes.add(quantityTextField);
		
		editableNodes.add(addOrderLineButton);
		editableNodes.add(removeOrderLineButton);
				
		ObservableList<Toggle> orderStatusRadioButtons =  orderStatusToggleGroup.getToggles();
		
		for(Toggle t : orderStatusRadioButtons)
		{
			editableNodes.add((Node) t);
		}
	}
	
	public void prepareRadioButtons()
	{
		pendingRadioButton.setUserData(OrderStatus.PENDENT);
		preparedRadioButton.setUserData(OrderStatus.PREPARADA);
		transportedRadioButton.setUserData(OrderStatus.TRANSPORT);
		deliveredRadioButton.setUserData(OrderStatus.LLIURADA);
	}
	
	@FXML
	public void modify(ActionEvent event)
	{
		setEditing(true);
	}
	
	@FXML
	public void save(ActionEvent event) //no funciona
	{
		Order order = parseForm();
		if(order == null) return;
		this.parent.setResult(order, this);
		if(this.action == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		else
		{
			setEditing(false);
		}

	}
	
	@Override
	public void setAction(int action)
	{
		super.setAction(action);
		if(action == FrameController.ACTION_CREATE)
		{
			this.setEditing(true);
			List<Toggle> toggles = this.orderStatusToggleGroup.getToggles();
			for(Toggle t : toggles)
			{
				RadioButton rb = (RadioButton) t;
				rb.setDisable(true);
			}
			this.orderDateTextField.setDisable(true);
		}
		else
		{
			this.setEditing(false);
		}
	}
	
	@Override
	public void setEditing(boolean isEditing)
	{
		super.setEditing(isEditing);
		boolean disabled = !isEditing;
		
		for(Node n : editableNodes)
		{
			n.setDisable(disabled);
		}
		
		modifyButton.setDisable(isEditing);
		modifyButton.setVisible(disabled);
		
		cancelButton.setDisable(disabled);
		cancelButton.setVisible(isEditing);
		
		saveButton.setDisable(disabled);
		saveButton.setVisible(isEditing);
		
		selectClientButton.setDisable(disabled);
		selectProductButton.setDisable(disabled);
	}
	
	@FXML
	public void selectProduct(ActionEvent event)
	{
		try
		{
			Stage thisStage = (Stage)((Button)event.getSource()).getScene().getWindow();
			SceneUtils.singleton().openNewSelector("/gui/product_selector.fxml", thisStage, this, "Seleccionar producto");
		}
		catch(Exception e)
		{
			
		}
	}
	
	@FXML
	public void selectClient(ActionEvent event)
	{
		try
		{
			Stage thisStage = (Stage)((Button)event.getSource()).getScene().getWindow();
			SceneUtils.singleton().openNewSelector("/gui/client_selector.fxml", thisStage, this, "Seleccionar cliente");
		}
		catch(Exception e)
		{
			
		}
	}
	
	private OrderLine parseOrderLine()
	{
		boolean hasMissingData = false;
		
		OrderStatus estat = (OrderStatus) this.orderStatusToggleGroup.getSelectedToggle().getUserData();
		
		if(!estat.equals(OrderStatus.PENDENT)) return null;
		
		Integer productId = null;
		String stringId = productIdTextField.getText();
		if(stringId != null && !stringId.isEmpty())
		{
			try
			{
				productId = Integer.parseInt(stringId);
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				hasMissingData = true;
			}
		}
		else hasMissingData = true;
		
		productIdTextField.setText("");
		String stringQuantity = quantityTextField.getText();
		int quantity = 0;
		if(stringQuantity != null && !stringQuantity.isEmpty())
		{
			try
			{
				quantity = Integer.parseInt(stringQuantity);
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				hasMissingData = true;
			}
		}
		else hasMissingData = true;
		
		if(!hasMissingData) {
			quantityTextField.setText("");
			
			Product p = (Product) ProductManager.singleton().searchOne(Product.CONDITIONS.idEqual(productId));
			
			//boolean stockOk = p.subtractProducte(quantity);
	
			temporaryOrderLine.fill(p, quantity);
			OrderLine ret = temporaryOrderLine;
		
			temporaryOrderLine = new OrderLine();
			return ret;
		}
		else return null;
	}
	
	@FXML
	public void addOrderLine(ActionEvent event)
	{
		OrderLine linia = parseOrderLine();
		if(linia == null)
		{
			this.errorData.setText("El pedido debe estar en estado PENDIENTE");
			this.errorData.setVisible(true);
		}
		else
		{
			this.errorData.setVisible(false);
			
			this.object.getLinies().add(linia);
			updateTable();
		}
	}
	
	@FXML
	public void removeOrderLine(ActionEvent event)
	{
		OrderLine linia = (OrderLine) lineTable.getSelectionModel().getSelectedItem();
		this.object.getLinies().remove(linia);
		updateTable();
	}
	
	@FXML
	public void dataComparisionKeyTypedShared(KeyEvent key) 
	{
		super.dataComparisionKeyTyped(this.orderDateTextField, key);
	}
	
	@FXML
	public void dataComparisionKeyTypedDelivery(KeyEvent key) 
	{
		super.dataComparisionKeyTyped(this.orderDeliveryDateTextField, key);
	}
	
	@FXML
	public void doubleComparisonKeyTyped(KeyEvent key) {
		super.onKeyTypedDouble(this.orderShippingTextField, key);
	}
	
	@FXML
	public void cancel(ActionEvent event) {
		this.errorData.setVisible(false);
		
		this.setEditing(false);
		
		if(this.getAction() == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		this.setObject(this.object);
	}
}

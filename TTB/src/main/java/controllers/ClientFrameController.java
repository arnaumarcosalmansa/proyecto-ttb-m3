package controllers;

import classes.Client;
import classes.ContactInfo;
import classes.Coordinates;
import classes.Provider;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafxutils.SceneUtils;
import managers.ClientManager;

public class ClientFrameController extends FrameController<Client> {

	@FXML
	private TextField clientIdTextField;
	@FXML
	private TextField clientNameTextField;
	@FXML
	private TextField clientPhoneTextField;
	@FXML
	private TextField clientCountryTextField;
	@FXML
	private TextField clientPoblacionTextField;
	@FXML
	private TextField clientAddressTextField;
	@FXML
	private TextField cleintCoordsTextField;
	
	@FXML
	private CheckBox clientActiveCB;
	
	@FXML
	private Button saveButton;
	@FXML
	private Button cancelButton;
	@FXML
	private Button modifyButton;
	
	@FXML
	private Label errorData;
	
	private Client object;
	
	@FXML
	public void initialize()
	{
		prepareEditableNodes();
	}
	
	@Override
	public void setAction(int action)
	{
		super.setAction(action);
		if(action == FrameController.ACTION_CREATE)
		{
			this.setEditing(true);
		}
		else
		{
			this.setEditing(false);
		}
	}

	@Override
	public void setEditing(boolean isEditing)
	{
		super.setEditing(isEditing);
		boolean disabled = !isEditing;

		for(Node n : editableNodes)
		{
			n.setDisable(disabled);
		}
		
		modifyButton.setDisable(isEditing);
		modifyButton.setVisible(disabled);
		
		cancelButton.setDisable(disabled);
		cancelButton.setVisible(isEditing);
		
		saveButton.setDisable(disabled);
		saveButton.setVisible(isEditing);
	}
	
	
	@FXML
	public void save(ActionEvent event) 
	{
		Client er = this.parseForm();
		if(er != null) {
			this.setEditing(false);
			
			if(this.getAction() == FrameController.ACTION_CREATE)
			{
				Stage frameStage = (Stage) saveButton.getScene().getWindow();
				frameStage.close();
			}
			
			parent.setResult(this.object, this);
		}
	}
	
	@FXML
	public void modify(ActionEvent event) 
	{
		this.setEditing(true);
	}
	
	@FXML
	public void cancel(ActionEvent event) {
		this.errorData.setVisible(false);
		
		this.setEditing(false);
		
		if(this.getAction() == FrameController.ACTION_CREATE)
		{
			Stage frameStage = (Stage) saveButton.getScene().getWindow();
			frameStage.close();
		}
		this.setObject(this.object);
	}

	@Override
	public void setResult(Object result, Controller whoSetsTheResult) 
	{
		
	}
	
	@Override
	public void setObject(Client cli) {
		this.object = cli;
		this.clientIdTextField.setText(this.object.getCIF());
		if(!this.object.getCIF().isEmpty() && this.object.getCIF() != null) clientIdTextField.setEditable(false);
		this.clientNameTextField.setText(this.object.getName());
		this.clientPhoneTextField.setText(this.object.getContact().getPhone());
		this.clientAddressTextField.setText(this.object.getContact().getAddress());
		this.clientCountryTextField.setText(this.object.getContact().getCountry());
		this.clientPoblacionTextField.setText(this.object.getContact().getPoblacio());
		this.cleintCoordsTextField.setText(this.object.getContact().getCoords().getLatitude() + " " + this.object.getContact().getCoords().getLongitude());
	
		this.clientActiveCB.setSelected(object.isActive());
	}

	@Override
	protected void prepareEditableNodes() 
	{
		editableNodes.add(this.clientIdTextField);
		editableNodes.add(this.clientNameTextField);
		editableNodes.add(this.clientPhoneTextField);
		editableNodes.add(this.clientAddressTextField);
		editableNodes.add(this.clientCountryTextField);
		editableNodes.add(this.clientPoblacionTextField);
		editableNodes.add(this.cleintCoordsTextField);
		
		editableNodes.add(this.clientActiveCB);
	}
	
	@FXML
	public void onKeyTypedCoords(KeyEvent key) 
	{
		super.onKeyTypedCoords(this.cleintCoordsTextField, key);
	}
	
	//para cifs
	@FXML
	public void onKeyTypedCif(KeyEvent key) 
	{
		super.onKeyTypedCif(this.clientIdTextField, key);
	}
	
	//para telefonos
	@FXML
	public void onKeyTypedPhone(KeyEvent key) 
	{
		super.onKeyTypedPhone(this.clientPhoneTextField, key);
	}
	
	private Client parseForm()
	{
		boolean hasMissingData = false;
		String id = "";
		String name = clientNameTextField.getText();
		
		if(clientIdTextField.getText() == null || clientIdTextField.getText().isEmpty()) {
			clientIdTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else {
			if(clientIdTextField.getText().length() == 9) {
				id = clientIdTextField.getText();
			}
			else {
				clientIdTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el cif no cumple con la longitud requerida.");
				this.errorData.setVisible(true);
			}
		}
		
		if(clientNameTextField.getText() == null || clientNameTextField.getText().isEmpty()) {
			clientNameTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		String phone = "";
		if(clientPhoneTextField.getText() == null || clientPhoneTextField.getText().isEmpty()) {
			clientPhoneTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		else{
			if(clientPhoneTextField.getText().length() == 9) {
				phone = clientPhoneTextField.getText();
			}
			else {
				clientPhoneTextField.setStyle(SceneUtils.ERROR_STYLE);
				hasMissingData = true;
				this.errorData.setText("Error: el telefono no cumple la longitud requerida.");
				this.errorData.setVisible(true);
			}
		}
		
		String country = clientCountryTextField.getText();
		if(clientCountryTextField.getText() == null || clientCountryTextField.getText().isEmpty()) {
			clientCountryTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		
		String poblacio = clientPoblacionTextField.getText();
		if(clientPoblacionTextField.getText() == null || clientPoblacionTextField.getText().isEmpty()) {
			clientPoblacionTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		
		String address = clientAddressTextField.getText();
		if(clientAddressTextField.getText() == null || clientAddressTextField.getText().isEmpty()) {
			clientAddressTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
		}
		
		String cordinates = cleintCoordsTextField.getText();
		double latitud = 0;
		double longitud = 0;
		if((this.cleintCoordsTextField.getText().isEmpty() || this.cleintCoordsTextField.getText() == null))
		{
			this.cleintCoordsTextField.setStyle(SceneUtils.ERROR_STYLE);
			hasMissingData = true;
			//this.providerCoordsTextField.setText(this.object.getContact().getCoords().getLatitude() + " " + this.object.getContact().getCoords().getLongitude());
		}
		else {
			if(!Coordinates.validateCoordinates(this.cleintCoordsTextField.getText())){
				hasMissingData = true;
				this.errorData.setText("Error: se espera 'latitud longitud' con formato 'xx.xx xx.xx' en las coordenadas.");
				this.errorData.setVisible(true);
			}
			else {
				try {
					latitud = Double.parseDouble(cordinates.split(" ")[0]);
					longitud = Double.parseDouble(cordinates.split(" ")[1]);
				}
				catch(Exception e) {
					hasMissingData = true;
					this.errorData.setText("Error: se espera 'latitud longitud' con formato 'xx.xx xx.xx' en las coordenadas.");
					this.errorData.setVisible(true);
				}
			}
		}
		
		boolean active = clientActiveCB.isSelected();
		
		Client result = null;
		if(!hasMissingData)
		{
			Coordinates coords = new Coordinates(latitud, longitud);
			ContactInfo contact = new ContactInfo();
			result = this.object;
			contact.fill(address, poblacio, country, phone, coords);
			result.fill(id, name, active, contact);
		}
		return result;
	}
	
}

package controllers;

import java.io.IOException;
import java.util.ArrayList;

import classes.Client;
import classes.OrderLine;
import files.FileIO;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxutils.SceneUtils;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;


public class RouteFrameController extends FrameController<ArrayList<Client>>
{
	@FXML
	private TableView nodeTable;
	
	@FXML
	private TableColumn nodeNumberColumn;
	@FXML
	private TableColumn nodeCifColumn;
	@FXML
	private TableColumn nodeNameColumn;
	@FXML
	private TableColumn nodeCountryColumn;
	@FXML
	private TableColumn nodeCityColumn;
	@FXML
	private TableColumn nodeAddressColumn;
	@FXML
	private TableColumn nodeCoordsColumn;
	
	ObservableList<Client> nodes = null;
	
	@Override
	public void setResult(Object result, Controller whoSetsTheResult) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void prepareEditableNodes() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void setObject(ArrayList<Client> clients)
	{
		nodes = FXCollections.observableArrayList();
		nodes.addAll(clients);
		initTable();
	}
	
	private void initTable()
	{
		nodeNumberColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Client, Integer>, ObservableValue<Integer>>()
		{
			@Override
			public ObservableValue<Integer> call(CellDataFeatures<Client, Integer> param)
			{
				return new SimpleIntegerProperty(nodes.indexOf(param.getValue()) + 1).asObject();
			}
		});
	
		
		nodeCifColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("CIF"));
		nodeNameColumn.setCellValueFactory(new PropertyValueFactory<Client, String>("name"));
		
		nodeCountryColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, ObservableValue<String>>(){
			@Override
			public ObservableValue<String> call(CellDataFeatures<Client, String> data) {
				return data.getValue().getContact().getCountryProperty();
			}		
		});
		nodeCityColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Client, String> data){  
				return data.getValue().getContact().getCityProperty();
			}  
		});
		
		nodeAddressColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Client, String> data){  
				return data.getValue().getContact().getAddressProperty();
			}  
		});
		
		nodeCoordsColumn.setCellValueFactory(new Callback<CellDataFeatures<Client, String>, 
                ObservableValue<String>>() {  
			@Override  
			public ObservableValue<String> call(CellDataFeatures<Client, String> data){  
				return new SimpleStringProperty(data.getValue().getContact().getCoords().toString());
			}  
		});
		
		nodeTable.setItems(nodes);
	}
	
	@FXML
	public void aceptar(ActionEvent event)
	{
		((Stage)((Button) event.getSource()).getScene().getWindow()).close();;
	}
	
	@FXML
	public void exportar(ActionEvent event)
	{
		String[] lines = new String[nodes.size() + 1];
		lines[0] = Client.CSVheader();
		for(int i = 1; i < lines.length; i++)
		{
			lines[i] = nodes.get(i - 1).toCSVString();
		}
		FileIO.writeFile(lines, "ruta.csv");
	}

}

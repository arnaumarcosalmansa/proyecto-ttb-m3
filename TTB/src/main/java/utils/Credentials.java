package utils;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class Credentials implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5141279883135418914L;
	
	private String username;
	private byte[] password;
	private String salt;
	
	public Credentials(String username, String plainPassword)
	{
		this.salt = generateSalt();
		this.username = username;
		this.password = hash(plainPassword, this.salt);
	}
	
	public boolean match(String user, String pass)
	{
		boolean result = false;
		byte[] hashed = this.hash(pass, this.salt);
		if(this.username.equals(user) && Arrays.equals(this.password, hashed))
		{
			result = true;
		}
		return result;
	}

	private byte[] hash(String pass, String salt)
	{
		String total = pass + salt;
		MessageDigest sha256 = null;
		try
		{
			sha256 = MessageDigest.getInstance("SHA-256");
		}
		catch (NoSuchAlgorithmException e)
		{
			return null;
		}
		
		byte[] digested = sha256.digest(total.getBytes());
		
		return digested;
	}
	
	private final static char[] validSalt = ("abcdefghijklmnopqrstuvwxyx" +
											"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
											"0123456789").toCharArray();
	
	private static String generateSalt()
	{
		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 255; i++)
		{
			int index = random.nextInt(validSalt.length);
			sb.append(validSalt[index]);
		}
		return sb.toString();
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Credentials other = (Credentials) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	
}

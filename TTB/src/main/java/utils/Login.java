package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import files.FileIO;

public class Login
{	
	private HashSet<Credentials> credentials = new HashSet<Credentials>();
	
	public Login(String pathToCredentials)
	{
		credentials.addAll(FileIO.<Credentials>deserializeMany(pathToCredentials));
	}
	
	public boolean checkLogin(String username, String password)
	{
		boolean check = false;
		
		Iterator<Credentials> it = credentials.iterator();
		
		while(it.hasNext() && !check) {
			check = it.next().match(username, password);
		}
		
		return check;
	}
}

package pool;

import java.util.HashSet;
import java.util.Set;

import classes.Client;
import classes.Product;
import queries.Condition;

public class Pool<T> extends HashSet<T>
{
	public boolean add(T item)
	{
		return super.add(item);
	}

	public boolean delete(T item)
	{
		return super.remove(item);
	}

	public Set<T> search(Condition<T>... conditions)
	{
		HashSet<T> result = new HashSet<T>();

		for (T p : this)
		{
			boolean isStillValid = true;
			for (int i = 0; isStillValid && i < conditions.length; i++)
			{
				isStillValid = conditions[i].check(p);
			}

			if (isStillValid)
			{
				result.add(p);
			}
		}

		return result;
	}

}

package javafxutils;

import java.io.IOException;

import com.sun.glass.ui.Application;

import classes.Product;
import controllers.Controller;
import controllers.FrameController;
import controllers.TabController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.*;

public class SceneUtils
{
	public static final String ERROR_STYLE = "-fx-text-box-border:rgb(255,0,0,0.5);";
	public static final String DEFAULT_STYLE = "-fx-text-box-border:rgb(78,105,255,0.5);";
	private static SceneUtils instance;

	private SceneUtils()
	{

	}

	public void openSceneOnStage(Stage window, String path) throws Exception
	{
		Parent root = FXMLLoader.load(getClass().getResource(path));
		Scene scene = new Scene(root);
		window.setScene(scene);
		window.centerOnScreen();
		window.show();
	}
	
	public void openNewFrame(String path, Stage parentStage, TabController parentController, int action, Object object, String nombreVentana) throws IOException
	{
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
		Parent root = loader.load();
		FrameController controller = (FrameController) loader.getController();
		controller.setAction(action);
		controller.setParentController(parentController);
		controller.setObject(object);
		
		
		Scene scene = new Scene(root);
		window.setTitle(nombreVentana);
		window.setScene(scene);
		window.setResizable(false);
		window.initModality(Modality.APPLICATION_MODAL);
		window.initOwner(parentStage);
		window.showAndWait();
	}

	public static SceneUtils singleton()
	{
		if (instance == null)
		{
			instance = new SceneUtils();
		}

		return instance;
	}

	public void openNewVisualizer(String path, Stage parentStage, Controller parentController, Object object, String nombreVentana) throws IOException
	{
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
		Parent root = loader.load();
		Controller controller = (Controller) loader.getController();
		controller.setParentController(parentController);
		controller.setObject(object);
		Scene scene = new Scene(root);
		window.setTitle(nombreVentana);
		window.centerOnScreen();
		window.setScene(scene);
		window.setResizable(false);
		window.initModality(Modality.APPLICATION_MODAL);
		window.initOwner(parentStage);
		window.showAndWait();
	}
	
	public void openNewSelector(String path, Stage parentStage, Controller parentController, String nombreVentana) throws IOException
	{
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
		Parent root = loader.load();
		Controller controller = (Controller) loader.getController();
		controller.setParentController(parentController);
		Scene scene = new Scene(root);
		window.setTitle(nombreVentana);
		window.setScene(scene);
		window.setResizable(false);
		window.initModality(Modality.APPLICATION_MODAL);
		window.initOwner(parentStage);
		window.showAndWait();
	}
}

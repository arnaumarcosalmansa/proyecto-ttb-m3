package classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javafx.beans.property.SimpleIntegerProperty;

import classes.conditions.ProductConditionBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import managers.ProviderManager;
import queries.Condition;

public class Product implements Comparable<Product>
{
	public static final String GUI_TAB_PATH = "/gui/product_tab.fxml";

	public static final ProductConditionBuilder CONDITIONS = new ProductConditionBuilder();

	private Integer productId;
	private String productName;
	private String description;
	private Integer stock;
	private Integer minStock;
	private UnitOfMeasurement unitat;
	private Map<Product, Integer> composicio = new HashMap<Product, Integer>();
	private List<Batch> lots = new ArrayList<Batch>();
	private ProductType type;
	private Provider provider;
	private double price = 1; // preu Venda
	private double pes; // peso

	public void afegirLot(int quantitat, LocalDate dataCaducitat)
	{
		//a
		Batch b = new Batch(dataCaducitat, quantitat);
		b.setProduct(this);
		lots.add(b);
	}

	public String veureLots()
	{
		String cadena = "";
		for (Batch ld : lots)
		{
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public String getProductName()
	{
		return this.productName;
	}

	public void setProductName(String nameProduct)
	{
		this.productName = nameProduct;
	}

	@Override
	public String toString()
	{
		String cadena =
				"Producte: " + productId + "\t - " + productName + "\tStock Total: " + getStock() + " " + unitat;
		cadena = cadena + "\tStockMinim:" + minStock + "\t" + this.type + "\t" + "preu: " + this.price + "\t descripcion: " + this.description;
		return cadena;
	}

	public Product()
	{
		this.productId = Generator.getNextProducte();
		this.type = ProductType.INGREDIENTE;
		minStock = 0;
		stock = 0;
	}

	public Product(String nomProducte)
	{
		this();
		this.productName = nomProducte;
	}

	public Product(String nomProducte, UnitOfMeasurement u, int sm, ProductType tipo, double precio)
	{
		this(nomProducte);
		this.setUnitatMesura(u);
		this.minStock = sm;
		this.type = tipo;
		this.price = precio;
	};

	public void fill(String productName, String description, Integer stock, Integer minStock, UnitOfMeasurement unitat,
			ProductType type, Provider provider, double price,
			double pes) {
		this.productName = productName;
		this.description = description;
		this.stock = stock;
		this.minStock = minStock;
		this.unitat = unitat;
		this.type = type;
		this.provider = provider;
		this.price = price;
		this.pes = pes;
	}

	public void setType(ProductType type)
	{
		this.type = type;
	}

	public ProductType getType()
	{
		return this.type;
	}

	public void setProvider(Provider pv)
	{
		this.provider = pv;
	}

	public Provider getProvider()
	{
		return provider;
	}

	public UnitOfMeasurement getUnitatMesura()
	{
		return unitat;
	}

	public void setUnitatMesura(UnitOfMeasurement unitatm)
	{
		unitat = unitatm;
	}

	public void setStock(Integer q)
	{
		this.stock = q;
	}

	public void setMinStock(Integer stockM)
	{
		this.minStock = stockM;
	}

	public Integer getMinStock()
	{
		return this.minStock;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getPrice()
	{
		return price;
	}

	public Map<Product, Integer> getComposicio()
	{
		return composicio;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void afegirComponent(Product p, int q)
	{
		composicio.put(p, q);
	}

	public String veureComposicio()
	{
		String cadena = "";
		Set<Product> claus = composicio.keySet();
		cadena = this.getProductName() + " --> ";
		for (Product p : claus)
			cadena += p.getProductName() + "(" + composicio.get(p) + ") ";
		return cadena;
	}

	@Override
	public int compareTo(Product p)
	{
		return (this.getProductName().compareTo(p.getProductName()));
	}

	public Integer getProductId()
	{
		return this.productId;
	}

	private Integer calclarStockLote()
	{
		int q = 0;
		if(lots != null)
		{
			for (Batch l : lots)
			{
				q += l.getQuantity();
			}
		}
		return q;
	}

	public Integer getStock()
	{
		stock = calclarStockLote();
		return stock;
	}

	public String viewLotsOrdenats()
	{
		lots.sort(null);
		String cadena = "";
		for (Batch ld : lots)
		{
			cadena += "   " + ld + "\n";
		}
		return cadena;
	}

	public List<Batch> getLots()
	{
		return this.lots;
	}

	public double getPes()
	{
		return this.pes;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productId == null)
		{
			if (other.productId != null)
				return false;
		}
		else if (!productId.equals(other.productId))
			return false;
		return true;
	}
	
	public StringProperty getNameProperty()
	{
		return new SimpleStringProperty(this.getProductName());
	}

	public SimpleIntegerProperty getIdProperty()
	{
		return new SimpleIntegerProperty(this.getProductId());
	}
	
	public StringProperty getDescriptionProperty() 
	{
		return new SimpleStringProperty(this.getDescription());
	}
	
	public StringProperty getUnityMeasuraProperty()
	{
		return new SimpleStringProperty(this.getUnitatMesura().name());
	}
	
	public StringProperty getTypeProcedure() {
		return new SimpleStringProperty(this.getType().name());
	}

	public boolean subtractProducte(int quantity)
	{
		Collections.sort(this.getLots(), new Comparator<Batch>()
		{
			@Override
			public int compare(Batch lote1, Batch lote2)
			{	
				return lote1.getExpirationDate().compareTo(lote2.getExpirationDate());
			}
		});
		
		int auxQuantity = quantity;
		List<Batch> auxLots = new ArrayList<Batch>(this.getLots());
		Iterator<Batch> iterador = auxLots.iterator();
		
		while(iterador.hasNext() && auxQuantity > 0)
		{
			Batch b = iterador.next();
			int howMany = b.getQuantity();
			
			if(auxQuantity <  howMany)
			{
				auxQuantity = 0;
				b.setQuantity(howMany - auxQuantity);
			}
			else
			{
				auxQuantity -= howMany;
				iterador.remove();
			}
		}
		
		if(auxQuantity == 0)
		{
			this.lots = auxLots;
			this.stock -= quantity;
			return true;
		}
		else
		{
			return false;
		}
	}
}
package classes;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import queries.Condition;

public class Batch implements Comparable<Batch>
{
	private int id; // numero de lote, id
	private Product product;
	private Integer quantity = 1;
	private LocalDate entryDate = LocalDate.now();
	private LocalDate expirationDate = null;
	
	public Batch() {
		this.id = Generator.getNextLot();
	}
	
	public Batch(int duracio, int quantitat)
	{
		this();
		entryDate = LocalDate.now();
		this.quantity = quantitat;

		if (duracio > 0)
		{
			expirationDate = LocalDate.now();
			expirationDate.plusDays(duracio);
		}
	}

	public Batch(LocalDate dataCaducitat, int quantitat)
	{
		this();
		entryDate = LocalDate.now();
		this.expirationDate = dataCaducitat;
		this.quantity = quantitat;
	}

	public LocalDate getExpirationDate()
	{
		return expirationDate;
	}

	public LocalDate getEntryDate()
	{
		return entryDate;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(Integer q)
	{
		quantity = q;
	}

	public Integer getId()
	{
		return this.id;
	}

	@Override
	public String toString()
	{
		return ("Lot: " + id + "\tQuantitat: " + quantity + "\tData Entrada: " + entryDate + "\tData Caducitat: "
				+ expirationDate);
	}

	@Override
	public int compareTo(Batch l)
	{
		if (this.getExpirationDate() == null && l.getExpirationDate() == null)
			return (this.getEntryDate().compareTo(l.getEntryDate()));

		if (this.getExpirationDate() != null && l.getExpirationDate() != null)
			return (this.getExpirationDate().compareTo(l.getExpirationDate()));

		if (this.getExpirationDate() == null)
			return -1;
		return 1;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Batch other = (Batch) obj;
		if (this.id != other.getId())
			return false;
		return true;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
package classes.conditions;

import java.util.Date;

import classes.Batch;
import queries.Condition;

public class BatchConditionBuilder
{
	/*
	 * iqualLot iqualQuantitat moreQuantitat lessQuantitat iqualDataEntrada
	 * iqualDataCaducitat
	 */

	public Condition<Batch> equalId(Integer id)
	{
		Condition<Batch> condition = new Condition<Batch>(id)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getId() == id)
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> equalQuantitat(Integer quantitat)
	{
		Condition<Batch> condition = new Condition<Batch>(quantitat)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantity() == quantitat)
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> quantityBiggerThan(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantity() > (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> quantityLessThan(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantity() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> quantityEquals(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantity() == (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> quantityBetween(Integer minInclusive, Integer maxExclusive)
	{
		Condition<Batch> condition = new Condition<Batch>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Batch item)
			{
				if ((Integer) var[0] <= item.getId() && item.getId() < (Integer) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> entryDateEquals(Date entrada)
	{
		Condition<Batch> condition = new Condition<Batch>(entrada)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getEntryDate().equals((Date) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Batch> exitDateEquals(Date sortida)
	{
		Condition<Batch> condition = new Condition<Batch>(sortida)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getExpirationDate().equals(sortida))
					return true;
				return false;
			}
		};
		return condition;
	}

}

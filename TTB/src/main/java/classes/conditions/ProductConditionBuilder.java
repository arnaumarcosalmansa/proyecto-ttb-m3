package classes.conditions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.Product;
import classes.Provider;
import classes.ProductType;
import classes.UnitOfMeasurement;
import queries.Condition;

public class ProductConditionBuilder
{

	/* Condiciones de product */
	/*
	 * A implementar: iqualID: id igual nameContainString: el nombre contiene este
	 * estring iqualStock: El producto tiene este estock moreStock: El producto
	 * tiene mas de este stock mostStock: El producto tiene menos de este stock
	 * iqualMinStock: El producto tiene este stock minimo moreMinStock lessMinStock
	 * iqualUnitat iqualType iqualProveidor iqualPrice morePrice lessPrice iqualPes
	 * morePes lessPes
	 */
	public Condition<Product> idEqual(Integer id)
	{
		Condition<Product> condition = new Condition<Product>(id)
		{
			@Override
			public boolean check(Product item)
			{
				if (((Integer) var[0]) == item.getProductId())
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> idMatches(String regex)
	{
		Condition<Product> condition = new Condition<Product>(regex)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getProductId().toString().matches((String) var[0]))
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> nameContainString(String data)
	{
		Condition<Product> condition = new Condition<Product>(data)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getProductName().toLowerCase().contains(data.toLowerCase()))
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> nameMatches(Pattern regex)
	{
		Condition<Product> condition = new Condition<Product>(regex)
		{
			@Override
			public boolean check(Product item)
			{
				Pattern regex = (Pattern) var[0];
				Matcher m = regex.matcher(item.getProductName());
				return m.find();
			}
		};
		return condition;
	}

	public Condition<Product> stockIsBroken()
	{
		Condition<Product> condition = new Condition<Product>()
		{
			@Override
			public boolean check(Product item)
			{
				return item.getStock() < item.getMinStock();
			}
		};
		return condition;
	}

	public Condition<Product> stockEquals(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getStock() == (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> stockLessThan(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getStock() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> stockBiggerThan(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getStock() > (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> stockInRange(Integer minIncluxive, Integer maxExclusive)
	{
		Condition<Product> condition = new Condition<Product>(minIncluxive, maxExclusive)
		{
			@Override
			public boolean check(Product item)
			{
				if ((Integer) var[0] <= item.getStock() && item.getStock() < (Integer) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> minStockEquals(Integer minStock)
	{
		Condition<Product> condition = new Condition<Product>(minStock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() == (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> minStockLessThan(Integer minStock)
	{
		Condition<Product> condition = new Condition<Product>(minStock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> minStockBiggerThan(Integer stock)
	{
		Condition<Product> condition = new Condition<Product>(stock)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getMinStock() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> minStockInRange(Integer minIncluxive, Integer maxExclusive)
	{
		Condition<Product> condition = new Condition<Product>(minIncluxive, maxExclusive)
		{
			@Override
			public boolean check(Product item)
			{
				if ((Integer) var[0] <= item.getMinStock() && item.getMinStock() < (Integer) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> measureUnityEquals(UnitOfMeasurement unity)
	{
		Condition<Product> condition = new Condition<Product>(unity)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getUnitatMesura() == (UnitOfMeasurement) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Product> measureUnityIn(UnitOfMeasurement[] unities)
	{
		Condition<Product> condition = new Condition<Product>(unities)
		{
			@Override
			public boolean check(Product item)
			{
				boolean matches = false;
				for(int i = 0; !matches && i < var.length; i++)
				{
					matches = item.getUnitatMesura().equals((UnitOfMeasurement) var[i]);
				}
				return matches;
			}
		};
		return condition;
	}

	public Condition<Product> batchCountEquals(Integer count)
	{
		Condition<Product> condition = new Condition<Product>(count)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getLots().size() == (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> batchCountLessThan(Integer count)
	{
		Condition<Product> condition = new Condition<Product>(count)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getLots().size() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> batchCountBiggerThan(Integer count)
	{
		Condition<Product> condition = new Condition<Product>(count)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getLots().size() > (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> batchCountEInRange(Integer minInclusive, Integer maxEsclusive)
	{
		Condition<Product> condition = new Condition<Product>(minInclusive, maxEsclusive)
		{
			@Override
			public boolean check(Product item)
			{
				if ((Integer) var[0] <= item.getLots().size() && item.getLots().size() < (Integer) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> typeEquals(ProductType type)
	{
		Condition<Product> condition = new Condition<Product>(type)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getType() == (ProductType) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> providerMatchesConditions(Condition<Provider>... conditions)
	{
		Condition<Product> condition = new Condition<Product>(conditions)
		{
			@Override
			public boolean check(Product item)
			{
				if(item.getProvider() == null) return false;
				boolean stillMatches = true;
				for (int i = 0; stillMatches && i < var.length; i++)
				{
					stillMatches = ((Condition<Provider>) var[i]).check(item.getProvider());
				}

				return stillMatches;
			}
		};
		return condition;
	}

	public Condition<Product> priceEquals(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() == (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> priceLessThan(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() < (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> priceBiggerThan(Double price)
	{
		Condition<Product> condition = new Condition<Product>(price)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPrice() > (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> priceInRange(Double minInclusive, Double maxExclusive)
	{
		Condition<Product> condition = new Condition<Product>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Product item)
			{
				if ((Double) var[0] <= item.getPrice() && item.getPrice() < (Double) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> pesEquals(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() == (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> pesLessThan(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() < (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> pesBiggerThan(Double pes)
	{
		Condition<Product> condition = new Condition<Product>(pes)
		{
			@Override
			public boolean check(Product item)
			{
				if (item.getPes() > (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Product> pesInRange(Double minInclusive, Double maxExclusive)
	{
		Condition<Product> condition = new Condition<Product>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Product item)
			{
				if ((Double) var[0] <= item.getPes() && item.getPes() < (Double) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}
}

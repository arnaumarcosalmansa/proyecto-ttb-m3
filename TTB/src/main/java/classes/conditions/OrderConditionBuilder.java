package classes.conditions;

import java.time.LocalDate;

import classes.Client;
import classes.Order;
import classes.OrderStatus;
import classes.Product;
import queries.Condition;

public class OrderConditionBuilder
{
	public static Condition<Order> clientMatchesConditions(Condition<Client>... conditions)
	{
		Condition<Order> condition = new Condition<Order>(conditions)
		{
			@Override
			public boolean check(Order item)
			{
				boolean matches = true;
				for (int i = 0; matches && i < conditions.length; i++)
				{
					matches = ((Condition) var[i]).check(item.getClient());
				}
				return matches;
			}
		};
		return condition;
	}

	public final static Condition<Order> statusEquals(OrderStatus status)
	{
		Condition<Order> condition = new Condition<Order>(status)
		{
			@Override
			public boolean check(Order item)
			{
				return item.getStatus() == (OrderStatus) status;
			}
		};
		return condition;
	}

	public final static Condition<Order> shippingGreaterThan(Double minExclusive)
	{
		Condition<Order> condition = new Condition<Order>(minExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return item.getShipping() > (Double) var[0];
			}
		};
		return condition;
	}

	public final static Condition<Order> shippingLessThan(Double maxExclusive)
	{
		Condition<Order> condition = new Condition<Order>(maxExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return item.getShipping() < (Double) var[0];
			}
		};
		return condition;
	}

	public final static Condition<Order> shippingBetween(Double minInclusive, Double maxExclusive)
	{
		Condition<Order> condition = new Condition<Order>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return (Double) var[0] <= item.getShipping() && item.getShipping() < (Double) var[1];
			}
		};
		return condition;
	}

	public final static Condition<Order> lineCountGreaterThan(Double minExclusive)
	{
		Condition<Order> condition = new Condition<Order>(minExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return item.getLinies().size() > (Double) var[0];
			}
		};
		return condition;
	}

	public final static Condition<Order> lineCountLessThan(Double maxExclusive)
	{
		Condition<Order> condition = new Condition<Order>(maxExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return item.getLinies().size() < (Double) var[0];
			}
		};
		return condition;
	}

	public final static Condition<Order> lineCountBetween(Double minInclusive, Double maxExclusive)
	{
		Condition<Order> condition = new Condition<Order>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Order item)
			{
				return (Double) var[0] <= item.getLinies().size() && item.getLinies().size() < (Double) var[1];
			}
		};
		return condition;
	}

	public Condition<Order> idEquals(Integer orderCode)
	{
		
		Condition<Order> condition = new Condition<Order>(orderCode)
		{
			@Override
			public boolean check(Order item) 
			{
				
				if (item.getId() == (Integer) var[0])
				{
					return true;
				}
				return false;
			}
	
		};
		return condition;
	}
	
	public Condition<Order> orderShippingBiggerThan(Double orderShipping)
	{
		Condition<Order> condition = new Condition<Order>(orderShipping)
		{
			@Override
			public boolean check(Order item)
			{
				if (item.getShipping() > (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> orderShippingLessThan(Double orderShipping)
	{
		Condition<Order> condition = new Condition<Order>(orderShipping)
		{
			@Override
			public boolean check(Order item)
			{
				if (item.getShipping() < (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> orderShippingEquals(Double orderShipping)
	{
		Condition<Order> condition = new Condition<Order>(orderShipping)
		{
			@Override
			public boolean check(Order item)
			{
				if (item.getShipping() == (Double) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Order> orderDateBiggerThan(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getOrderDate().compareTo((LocalDate) var[0]) > 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> orderDateLessThan(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getOrderDate().compareTo((LocalDate) var[0]) < 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> orderDateEquals(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getOrderDate().compareTo((LocalDate) var[0]) == 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> deliveryDateBiggerThan(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getDeliveryDate().compareTo((LocalDate) var[0]) > 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> deliveryDateLessThan(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getDeliveryDate().compareTo((LocalDate) var[0]) < 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> deliveryDateEquals(LocalDate date)
	{
		Condition<Order> condition = new Condition<Order>(date)
		{
			@Override
			public boolean check(Order item)
			{
				if(item.getDeliveryDate().compareTo((LocalDate) var[0]) == 0)
				{
					return true;
				}
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Order> statusIn(OrderStatus[] estados)
	{
		Condition<Order> condition = new Condition<Order>(estados)
		{
			@Override
			public boolean check(Order item)
			{
				for(Object o : var)
				{
					OrderStatus status = (OrderStatus) o;
					if(item.getStatus().equals(status)) return true;
				}
				return false;
			}
		};
		return condition;
	}
}

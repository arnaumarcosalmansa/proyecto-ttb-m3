package classes.conditions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.Client;
import classes.ContactInfo;
import queries.Condition;

public class ClientConditionBuilder
{
	/*
	 * iqualCIF containName iqualActive
	 */
	// filtros

	public Condition<Client> cifEquals(String cif)
	{
		Condition<Client> condition = new Condition<Client>(cif)
		{
			@Override
			public boolean check(Client item)
			{
				if (item.getCIF().equals((String) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}
	

	public Condition<Client> cifContains(String cif)
	{
		Condition<Client> condition = new Condition<Client>(cif)
		{
			@Override
			public boolean check(Client item)
			{
				if (item.getCIF().contains((String) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}

	
	public Condition<Client> cifMatches(Pattern p)
	{
		Condition<Client> condition = new Condition<Client>(p)
		{
			@Override
			public boolean check(Client item)
			{
				Matcher m = p.matcher(item.getCIF());
				return m.find();
			}
		};
		return condition;
	}

	public Condition<Client> nameContains(String regex)
	{
		Condition<Client> condition = new Condition<Client>(regex)
		{
			@Override
			public boolean check(Client item)
			{
				if (item.getName().matches((String) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Client> nameMatches(Pattern regex)
	{
		Condition<Client> condition = new Condition<Client>(regex)
		{
			@Override
			public boolean check(Client item)
			{
				Matcher m = ((Pattern) var[0]).matcher(item.getName());
				return m.find();
			}
		};
		return condition;
	}

	public Condition<Client> activeEquals(boolean active)
	{
		Condition<Client> condition = new Condition<Client>(active)
		{
			@Override
			public boolean check(Client item)
			{
				if (item.isActive() == (Boolean) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Client> contactInfoMatchConditions(Condition<ContactInfo>... conditions)
	{
		Condition<Client> condition = new Condition<Client>(conditions)
		{
			@Override
			public boolean check(Client item)
			{
				boolean checksAllConditions = true;
				for (int i = 0; checksAllConditions && i < var.length; i++)
				{
					checksAllConditions = ((Condition<ContactInfo>) var[i]).check(item.getContact());
				}
				return checksAllConditions;
			}
		};
		return condition;
	}


}

package classes.conditions;

import classes.Coordinates;
import queries.Condition;

public class CoordinatesConditionBuilder
{
	public final static Condition<Coordinates> closerToCoords(Coordinates coords, double maxDistanceExclusive)
	{
		Condition<Coordinates> condition = new Condition<Coordinates>(coords, maxDistanceExclusive)
		{
			@Override
			public boolean check(Coordinates item)
			{
				return item.distanceTo((Coordinates) var[0]) < (Double) var[1];
			}
		};
		return condition;
	}

	public final static Condition<Coordinates> furtherToCoords(Coordinates coords, double minDistanceInclusive)
	{
		Condition<Coordinates> condition = new Condition<Coordinates>(coords, minDistanceInclusive)
		{
			@Override
			public boolean check(Coordinates item)
			{
				return item.distanceTo((Coordinates) var[0]) >= (Double) var[1];
			}
		};
		return condition;
	}

	public final static Condition<Coordinates> inRangeToCoords(Coordinates coords, double minDistanceInclusive,
			double maxDistanceExclusive)
	{
		Condition<Coordinates> condition =
				new Condition<Coordinates>(coords, minDistanceInclusive, maxDistanceExclusive)
				{
					@Override
					public boolean check(Coordinates item)
					{
						double distance = item.distanceTo((Coordinates) var[0]);
						return (Double) var[1] <= distance && distance < (Double) var[2];
					}
				};
		return condition;
	}
}

package classes.conditions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.Client;
import classes.ContactInfo;
import classes.Provider;
import queries.Condition;

public class ProviderConditionBuilder
{
	/*
	 * containName iqualCIF iqualActive
	 */

	public final static Condition<Provider> nameContains(String name)
	{
		Condition<Provider> condition = new Condition<Provider>(name)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.getNameProveidor().contains(name))
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Provider> nameMatches(String regex)
	{
		Condition<Provider> condition = new Condition<Provider>(regex)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.getNameProveidor().matches((String) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Provider> nameMatches(Pattern regex)
	{
		Condition<Provider> condition = new Condition<Provider>(regex)
		{
			@Override
			public boolean check(Provider item)
			{
				Pattern regex = (Pattern) var[0];
				Matcher m = regex.matcher(item.getNameProveidor());
				return m.find();
			}
		};
		return condition;
	}

	public final static Condition<Provider> cifContains(String cif)
	{
		Condition<Provider> condition = new Condition<Provider>(cif)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.getCIF().contains((String) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}

	public Condition<Provider> cifMatches(Pattern regex)
	{
		Condition<Provider> condition = new Condition<Provider>(regex)
		{
			@Override
			public boolean check(Provider item)
			{
				Pattern regex = (Pattern) var[0];
				Matcher m = regex.matcher(item.getCIF());
				return m.find();
			}
		};
		return condition;
	}
	
	public Condition<Provider> cifEquals(String cif)
	{
		Condition<Provider> condition = new Condition<Provider>(cif)
		{
			@Override
			public boolean check(Provider item)
			{
				return item.getCIF().toUpperCase().equals(((String) var[0]).toUpperCase());
			}
		};
		return condition;
	}

	public Condition<Provider> activeEquals(boolean active)
	{
		Condition<Provider> condition = new Condition<Provider>(active)
		{
			@Override
			public boolean check(Provider item)
			{
				if (item.isActive() == (Boolean) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}
	
	public Condition<Provider> contactInfoMatchConditions(Condition<ContactInfo>... conditions)
	{
		Condition<Provider> condition = new Condition<Provider>(conditions)
		{
			@Override
			public boolean check(Provider item)
			{
				boolean checksAllConditions = true;
				for (int i = 0; checksAllConditions && i < var.length; i++)
				{
					checksAllConditions = ((Condition<ContactInfo>) var[i]).check(item.getContact());
				}
				return checksAllConditions;
			}
		};
		return condition;
	}
}

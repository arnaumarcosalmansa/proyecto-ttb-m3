package classes.conditions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import classes.ContactInfo;
import classes.Coordinates;
import classes.Provider;
import queries.Condition;

public class ContactInfoConditionBuilder
{
	public Condition<ContactInfo> contactInfoContains(String regex)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(regex)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				String[] allData = item.getAllDataString();
				boolean matches = false;
				for (int i = 0; !matches && i < allData.length; i++)
				{
					matches = allData[i].matches((String) var[0]);
				}
				return matches;
			}
		};
		return condition;
	}

	public Condition<ContactInfo> coordsMatchConditions(Condition<Coordinates>... conditions)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(conditions)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				boolean matches = true;
				for (int i = 0; matches && i < conditions.length; i++)
				{
					matches = ((Condition) var[i]).check(item);
				}
				return matches;
			}
		};
		return condition;
	}
	

	public Condition<ContactInfo> phoneContains(String phone)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(phone)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				return item.getPhone().contains((String) var[0]);
			}
		};
		return condition;
	}
	
	public Condition<ContactInfo> phoneMatches(Pattern regex)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(regex)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				Matcher m = ((Pattern) var[0]).matcher(item.getPhone());
				return m.find();
			}
		};
		return condition;
	}

	public Condition<ContactInfo> countryContains(String country)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(country)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				return item.getCountry().contains((String) var[0]);
			}
		};
		return condition;
	}

	public Condition<ContactInfo> countryMatches(Pattern regex)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(regex)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				Matcher m = ((Pattern) var[0]).matcher(item.getCountry());
				return m.find();
			}
		};
		return condition;
	}
	
	public Condition<ContactInfo> cityContains(String city)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(city)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				return item.getPoblacio().contains((String) var[0]);
			}
		};
		return condition;
	}
	
	public Condition<ContactInfo> cityMatches(Pattern regex)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(regex)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				Matcher m = ((Pattern) var[0]).matcher(item.getPoblacio());
				return m.find();
			}
		};
		return condition;
	}
}

package classes;

public class Component {
	protected Product product;
	protected Integer quantity;

	public Component(Product product, Integer quantity) {
		this();
		this.product = product;
		this.quantity = quantity;
	}

	public Component() 
	{
		super();
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}

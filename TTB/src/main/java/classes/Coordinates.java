package classes;

import queries.Condition;

public class Coordinates
{
	private double latitude;
	private double longitude;

	public Coordinates()
	{

	}

	public Coordinates(double lat, double lon)
	{
		this();
		this.latitude = lat;
		this.longitude = lon;
	}

	public double getLatitude()
	{
		return this.latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return this.longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public String toString()
	{
		return "[ " + this.getLatitude() + ", " + this.getLongitude() + " ]";
	}

	public Double distanceTo(Coordinates coordinates)
	{
		double deltaLatitude = coordinates.getLatitude() - this.getLatitude();
		double deltaLongitude = coordinates.getLongitude() - this.getLongitude();
		deltaLatitude *= deltaLatitude;
		deltaLongitude *= deltaLongitude;
		double distance = Math.sqrt(deltaLatitude + deltaLongitude);
		return distance;
	}
	
	public static boolean validateCoordinates(String data) {
		boolean result = true;

		if(!data.equals("") && data.contains(" ")) {
			int[] pos = new int[5];
			pos[0] = data.indexOf(".");
			pos[1] = data.indexOf(" ");
			pos[2] = data.lastIndexOf(".");
			pos[3] = data.indexOf("-");
			pos[4] = data.lastIndexOf("-");
			
			if(pos[1] == 0 || pos[1] == (data.length() - 1)) result = false;
			
			if(result) {
				if(pos[3] < pos[4]) {
					if(pos[3] != 0 || pos[4] != (pos[1] + 1)) result = false;
				}
				else {
					if(pos[3] == pos[4] && pos[3] != -1) {
						if(pos[3] > 0) {
							if(pos[3] != (pos[1] + 1)) result = false;
						}
						else {
							if(pos[3] != 0) result = false;
						}
					}
				}
				
				if(result) {
					if(pos[2] > pos[0]) {
						if(pos[0] == 0 || pos[2] == (data.length() - 1) || pos[2] == (pos[1] + 1) || pos[0] == (pos[1] - 1)) result = false;
					}
					else {
						if(pos[0] == pos[2]) {
							if(pos[0] == 0 || pos[0] == (data.length() - 1) || pos[0] == (pos[1] + 1) || pos[0] == (pos[1] - 1)) result = false;
						}
					}
				}
			}
		}
		return result; 
	}
	
	public void fill(String coords) {
		
	}
}

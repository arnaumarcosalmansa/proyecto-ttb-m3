package classes;

import classes.conditions.ContactInfoConditionBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import queries.Condition;

public class ContactInfo
{
	public static final ContactInfoConditionBuilder CONDITIONS = new ContactInfoConditionBuilder();
	
	private String address;
	private String poblacio;
	private String country;
	private String personaContacte;
	private String phone;
	private Coordinates coords;

	public ContactInfo()
	{
		this.setAddress("");
		this.setPoblacio("");
		this.setCountry("");
		this.setPhone("");
		this.coords = new Coordinates();
	}

	public ContactInfo(String address, String poblacio, String country, String persona, String phone, double latitude, double longitude)
	{
		this.setAddress(address);
		this.setPoblacio(poblacio);
		this.setCountry(country);
		this.setPhone(phone);
		this.coords = new Coordinates(latitude,longitude);
	}

	public void setCoordinates(Coordinates coordinates)
	{
		this.coords = coordinates;
	}

	public String getAddress()
	{
		return this.address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPoblacio()
	{
		return this.poblacio;
	}

	public void setPoblacio(String poblacio)
	{
		this.poblacio = poblacio;
	}

	public String getCountry()
	{
		return this.country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getPersonaContacte()
	{
		return this.personaContacte;
	}

	public void setPersonaContacte(String personaContacte)
	{
		this.personaContacte = personaContacte;
	}

	public String getPhone()
	{
		return this.phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Coordinates getCoords()
	{
		return this.coords;
	}
	
	public StringProperty getPhoneProperty()
	{
		return new SimpleStringProperty(this.getPhone());
	}
	
	public StringProperty getCountryProperty() {
		return new SimpleStringProperty(this.getCountry());
	}

	public StringProperty getCityProperty() {
		return new SimpleStringProperty(this.getPoblacio());
	}
	
	public String[] getAllDataString()
	{
		String[] data = new String[6];
		data[0] = this.getAddress();
		data[1] = this.getPoblacio();
		data[2] = this.getCountry();
		data[3] = this.getPersonaContacte();
		data[4] = this.getPhone();
		data[5] = this.getCoords().toString();

		return data;
	}

	public StringProperty getAddressProperty() {
		// TODO Auto-generated method stub
		return new SimpleStringProperty(this.getAddress());
	}
	
	public void fill(String address, String poblacio, String country, String personaContacte, String phone, Coordinates coords) {
		this.address = address;
		this.poblacio = poblacio;
		this.personaContacte = personaContacte;
		this.phone = phone;
		this.coords = coords;
		this.country = country;
	}
	
	public void fill(String address, String poblacio, String country, String phone, Coordinates coords) {
		this.address = address;
		this.poblacio = poblacio;
		this.phone = phone;
		this.coords = coords;
		this.country = country;
	}
	
	@Override
	public String toString() {
		return this.personaContacte;
	}
}

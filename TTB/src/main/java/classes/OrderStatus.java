package classes;

public enum OrderStatus
{
	PENDENT,
	PREPARADA,
	TRANSPORT,
	LLIURADA
}

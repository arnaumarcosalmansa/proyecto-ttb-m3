package classes;

import java.util.Set;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import classes.conditions.ProviderConditionBuilder;
import javafx.beans.value.ObservableValue;
import queries.Condition;

public class Provider
{
	public static final ProviderConditionBuilder CONDITIONS = new ProviderConditionBuilder();

	public static final String GUI_TAB_PATH = "/gui/provider_tab.fxml";

	private String nameProveidor;
	private String CIF;
	private boolean active;
	private ContactInfo contact;
	// nada

	public Provider(boolean active) {
		this();
		this.active = active;
	}
	
	public Provider()
	{
		this.contact = new ContactInfo();
		this.CIF = "";
	}
	
	public Provider(String cif) {
		this();
		this.CIF = cif;
	}

	public Provider(String nameProveidor, String CIF, ContactInfo contact)
	{
		this();
		this.nameProveidor = nameProveidor;
		this.CIF = CIF;
		this.contact = contact;
	}

	public String getNameProveidor()
	{
		return this.nameProveidor;
	}
	
	public void setNameProvider(String name) {
		this.nameProveidor = name;
	}

	public String getCIF()
	{
		return this.CIF;
	}

	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public ContactInfo getContact()
	{
		return this.contact;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Provider other = (Provider) obj;
		if (CIF == null)
		{
			if (other.CIF != null)
				return false;
		}
		else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return this.getCIF();
	}

	public StringProperty getIsActiveProperty()
	{
		String result = this.isActive() ? "Sí" : "No";
		return new SimpleStringProperty(result);
	}
	
	public StringProperty getCIFProperty()
	{
		return new SimpleStringProperty(this.getCIF());
	}
	
	public void fill(String CIF, String nameProveidor, boolean active, ContactInfo contact)
	{
		this.CIF = CIF;
		this.nameProveidor = nameProveidor;
		this.active = active;
		this.contact = contact;
	}
}
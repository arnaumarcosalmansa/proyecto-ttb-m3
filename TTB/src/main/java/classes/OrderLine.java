package classes;

public class OrderLine
{
	private Product producte;
	private int quantity;
	private double price; // preu venda

	public OrderLine()
	{
		
	}
	
	public OrderLine(Product p, int q, double price)
	{
		this.producte = p;
		this.quantity = q;
		this.price = price;
	}

	public Product getProducte()
	{
		return this.producte;
	}

	public int getQuantity()
	{
		return this.quantity;
	}

	public double getPrice()
	{
		return this.price;
	}

	public void setProducte(Product producte) {
		this.producte = producte;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void fill(Product producte, int quantity) {
		this.producte = producte;
		this.quantity = quantity;
		this.price = producte.getPrice() * quantity;
	}

	/*
	 * 
	 */
}
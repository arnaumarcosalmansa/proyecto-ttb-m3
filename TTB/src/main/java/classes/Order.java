package classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import classes.conditions.OrderConditionBuilder;
import classes.conditions.ProductConditionBuilder;
import queries.Condition;

public class Order
{
	public static final String GUI_TAB_PATH = "/gui/order_tab.fxml";
	
	public static final OrderConditionBuilder CONDITIONS = new OrderConditionBuilder();
	
	private int id;
	private Client client;
	private LocalDate orderDate = LocalDate.now();
	private LocalDate deliveryDate = null;
	private OrderStatus status = OrderStatus.PENDENT; // PENDENT - PREPARAT - TRANSPORT - LLIURAT
	private Double shipping = 0.0; // preu de transport
	private List<OrderLine> linies = new ArrayList<OrderLine>();;

	public Order()
	{
		super();
		this.id = Generator.getNextComanda();
		client = new Client();
		deliveryDate = LocalDate.now();
	}

	public Order(Client client)
	{
		this();
		this.client = client;
	}
	
	

	public Order(Client client, LocalDate orderDate, LocalDate deliveryDate, OrderStatus status, Double shipping,
			List<OrderLine> linies) {
		this();
		this.client = client;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.status = status;
		this.shipping = shipping;
		this.linies = linies;
	}

	public Order(Integer id, Client client, LocalDate orderDate, LocalDate deliveryDate, OrderStatus status,
			Double shipping, List<OrderLine> linies) {
		super();
		this.id = id;
		this.client = client;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.status = status;
		this.shipping = shipping;
		this.linies = linies;
	}

	public List<OrderLine> getLinies()
	{
		return linies;
	}

	public int getId()
	{
		return this.id;
	}

	public Client getClient()
	{
		return this.client;
	}

	public LocalDate getOrderDate()
	{
		return this.orderDate;
	}

	public LocalDate getDeliveryDate()
	{
		return this.deliveryDate;
	}

	public OrderStatus getStatus()
	{
		return this.status;
	}

	public void setStatus(OrderStatus status)
	{
		this.status = status;
	}

	public double getShipping()
	{
		return this.shipping;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public void fill(int id, Client cli, LocalDate orderDate, LocalDate deliveryDate, OrderStatus status, double Shipping, List<OrderLine> linies) {
		this.id = id;
		this.client = cli;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.status = status;
		this.shipping = Shipping;
		this.linies = linies;
	}

}
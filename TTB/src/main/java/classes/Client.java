package classes;

import java.util.Set;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import classes.conditions.ClientConditionBuilder;
import javafx.beans.value.ObservableValue;
import queries.Condition;

public class Client
{
	public static final ClientConditionBuilder CONDITIONS = new ClientConditionBuilder();
	
	public static final String GUI_TAB_PATH = "/gui/client_tab.fxml";
	
	private String name;
	private String CIF;
	private boolean active;
	private ContactInfo contact;
	
	public Client(boolean active) {
		this();
		this.active = active;
	}
	
	public Client()
	{
		this.CIF = "";
		contact = new ContactInfo();
	}

	//constructor por cif, ya que el cif es la clave principal y no el nombre
	public Client(String cif)
	{
		this();
		this.CIF = cif;
	}
	
	public Client(String CIF, String nom, boolean activo, ContactInfo contacte)
	{
		this.CIF = CIF;
		this.name = nom;
		this.active = activo;
		this.contact = contacte;
	}

	public Client(String nom, double lat, double lon)
	{
		this(nom);
		contact.setCoordinates(new Coordinates(lat, lon));
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCIF()
	{
		return this.CIF;
	}

	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public ContactInfo getContact()
	{
		return this.contact;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (CIF == null)
		{
			if (other.CIF != null)
				return false;
		}
		else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}

	public StringProperty getIsActiveProperty()
	{
		String result = this.isActive() ? "Sí" : "No";
		return new SimpleStringProperty(result);
	}

	public StringProperty getCIFProperty()
	{
		return new SimpleStringProperty(this.getCIF());
	}

	public String toCSVString()
	{
		String ret = this.getCIF() + ";" + this.getName() + ";" + this.getContact().getCountry() + ";" + this.getContact().getPoblacio() + ";" + this.getContact().getAddress() + ";" + this.getContact().getCoords().toString();
		return ret;
	}

	public static String CSVheader()
	{
		return "CIF ; NOMBRE ; PAÍS ; CIUDAD ; DIRECCIÓN ; COORDENADAS";
	}
	
	public void fill(String cif, String name, boolean active, ContactInfo contact) {
		this.CIF = cif;
		this.name = name;
		this.active = active;
		this.contact = contact;
	}
}
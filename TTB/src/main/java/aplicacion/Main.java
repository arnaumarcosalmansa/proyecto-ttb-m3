package aplicacion;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import classes.Batch;
import classes.Client;
import classes.ContactInfo;
import classes.Coordinates;
import classes.Order;
import classes.OrderLine;
import classes.OrderStatus;
import classes.Product;
import classes.ProductType;
import classes.Provider;
import classes.UnitOfMeasurement;
import files.FileIO;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import managers.ClientManager;
import managers.OrderManager;
import managers.ProductManager;
import managers.ProviderManager;
import queries.Condition;
import utils.Credentials;

public class Main extends Application
{
	public static void main(String[] args)
	{
		prepareData();
		Application.launch(args);
	}

	public void start(Stage stage) throws IOException
	{
		Parent root = FXMLLoader.load(getClass().getResource("/gui/login_form.fxml"));
		Scene scene = new Scene(root);
		stage.setResizable(false);
		stage.setTitle("Aplicacion");
		stage.setScene(scene);
		stage.show();
	}

	private static void prepareData()
	{
		
		ContactInfo contactoA = new ContactInfo("Armstrong Ave", "Stoutland", "Estados Unidos", "Mike Stinger", "323555323",37.815336,-92.512971);
		ContactInfo contactoB = new ContactInfo("Campbell St", "Kansas city", "Estados Unidos", "Peter Walcott", "227382282",39.092190, -94.572906);
		ContactInfo contactoC = new ContactInfo("Jazz Alley", "Sacramento", "Estados Unidos", "Steve Johnson", "777127712",38.577872, -121.486541);
		ContactInfo contactoD = new ContactInfo("Postkamp", "Hannover", "Alemania", "Andy Klop", "227382282",52.379944, 9.731114);
		ContactInfo contactoE = new ContactInfo("Yamabe Nakamachi", "Furano-shi", "Japón", "Shawn Frost", "634832584",43.243849, 142.382088);
		ContactInfo contactoF = new ContactInfo("Nishigokencho", "Tokyo", "Japón", "Mamoru Endō", "643724754",35.707578, 139.737087);
		ContactInfo contactoG = new ContactInfo("Calle de Valencia", "Barcelona", "España", "Juan Perez", "967345682",41.379868, 2.146285);
		ContactInfo contactoH = new ContactInfo("Calle Vilardell", "Barcelona", "España", "Manolo Sanchez", "967345682",41.374617, 2.143477);
		ContactInfo contactoI = new ContactInfo("Av del Paralelo", "Barcelona", "España", "Pepe Garcia", "967382282",41.375006, 2.159216);
		
		
		Provider provider1 = new Provider("Mike Stinger", "12345678A", contactoA);
		Provider provider2 = new Provider("Peter Walcott", "12345678B", contactoB);
		Provider provider3 = new Provider("Steve Johnson", "12345678C", contactoC);
		Provider provider4 = new Provider("Andy Klop", "52375413Z", contactoD);
		Provider provider5 = new Provider("Pepe Garcia", "74633425F", contactoI);
		Provider provider6 = new Provider("Manolo Sanchez", "74633425F", contactoH);
		Provider provider7 = new Provider("Manolo Sanchez", "74633425F", contactoG);
		Provider provider8 = new Provider("Shawn Frost", "72864234F", contactoE);
		Provider provider9 = new Provider("Mamoru Endō", "86435354S", contactoF);
		
		ProviderManager.singleton().save(provider1);
		ProviderManager.singleton().save(provider2);
		ProviderManager.singleton().save(provider3);
		ProviderManager.singleton().save(provider4);
		ProviderManager.singleton().save(provider5);
		ProviderManager.singleton().save(provider6);
		ProviderManager.singleton().save(provider7);
		ProviderManager.singleton().save(provider8);
		ProviderManager.singleton().save(provider9);
		
		Product product1 = new Product("Leche", UnitOfMeasurement.LITROS, 25, ProductType.INGREDIENTE, 2.3);
		product1.setProvider(provider1);
		product1.afegirLot(10, LocalDate.now().plusDays(37));
		product1.afegirLot(27, LocalDate.now().plusDays(37));
		product1.afegirLot(17, LocalDate.now().plusDays(37));

		Product product2 = new Product("Arroz", UnitOfMeasurement.GRAMOS, 30, ProductType.INGREDIENTE, 1.2);
		product2.setProvider(provider2);
		product2.afegirLot(15, LocalDate.now().plusDays(20));
		product2.afegirLot(15, LocalDate.now().plusDays(32));

		Product product3 = new Product("Pastel de guindilla", UnitOfMeasurement.UNIDADES, 10, ProductType.VENDIBLE, 49.99);
		product3.setProvider(provider3);
		
		Product product4 = new Product("Agua", UnitOfMeasurement.LITROS, 100, ProductType.INGREDIENTE, 1);
		product4.setProvider(provider4);
		product4.afegirLot(30, LocalDate.now().plusDays(10));
		product4.afegirLot(30, LocalDate.now().plusDays(20));
		product4.afegirLot(30, LocalDate.now().plusDays(30));
		product4.afegirLot(30, LocalDate.now().plusDays(40));
		
		Product product5 = new Product("Harina", UnitOfMeasurement.GRAMOS, 45, ProductType.INGREDIENTE, 3.49);
		product5.setProvider(provider5);
		product5.afegirLot(30, LocalDate.now().plusDays(10));
		product5.afegirLot(30, LocalDate.now().plusDays(25));
		
		Product product6 = new Product("Bolas de arroz", UnitOfMeasurement.UNIDADES, 20, ProductType.VENDIBLE, 19.99);
		product6.setProvider(provider9);
		
		Product product7 = new Product("Tofu", UnitOfMeasurement.UNIDADES, 10, ProductType.INGREDIENTE, 6.99);
		product7.setProvider(provider8);
		product5.afegirLot(10, LocalDate.now().plusDays(2));
		
		
		Product product8 = new Product("Azucar", UnitOfMeasurement.GRAMOS, 125, ProductType.INGREDIENTE, 0.8);
		product8.setProvider(provider3);
		product8.afegirLot(20, LocalDate.now().plusDays(15));
		product8.afegirLot(20, LocalDate.now().plusDays(30));
		product8.afegirLot(20, LocalDate.now().plusDays(45));
		product8.afegirLot(20, LocalDate.now().plusDays(60));
		
		product3.afegirComponent(product1, 5);
		product3.afegirComponent(product5, 7);
		
		product6.afegirComponent(product2, 10);
		
		ProductManager.singleton().save(product1);
		ProductManager.singleton().save(product2);
		ProductManager.singleton().save(product3);
		ProductManager.singleton().save(product4);
		ProductManager.singleton().save(product5);
		ProductManager.singleton().save(product6);
		ProductManager.singleton().save(product7);
		ProductManager.singleton().save(product8);
		
		ContactInfo contacto1 = new ContactInfo("Hommachi Dori", "Osaka", "Japón", "Rimuru Tempest", "643621643",34.683463, 135.512127);
		ContactInfo contacto2 = new ContactInfo("Inokashira Dori,", "Tokyo", "Japón", "Sakata Gintoki", "965246754",35.660714, 139.698917);
		ContactInfo contacto3 = new ContactInfo("A4200", "Londres", "Reino unido", "Charles Sturidge", "325675345",51.517156, -0.120146);
		ContactInfo contacto4 = new ContactInfo("Calle Martínez Maldonado", "Malaga", "España", "Pedro Domingez", "754135754",95.0,32.0);
		ContactInfo contacto5 = new ContactInfo("Calle San Benito", "Sevilla", "España", "Carmen Sanchez", "642336243",37.388154, -5.979598);
		ContactInfo contacto6 = new ContactInfo("C-1413a", "Barcelona", "España", "Jorge Lopez", "643135754",41.551800, 2.105416);
		ContactInfo contacto7 = new ContactInfo("Via Alexandra", "Barcelona", "España", "Adrián Izquierdo", "644437542",41.555477, 2.087674);
		ContactInfo contacto8 = new ContactInfo("Carrer dels Enamorats", "Barcelona", "España", "Josep Gandia", "643637835",41.407896, 2.184419);
		ContactInfo contacto9 = new ContactInfo("Carrer del Dos", "Barcelona", "España", "Joan Cano", "865234753",41.407180, 2.181830);
		
		Client c1 = new Client("65442467E", "Rimuru Tempest", true, contacto1);
		Client c2 = new Client("67437633V", "Sakata Gintoki", true, contacto2);
		Client c3 = new Client("36436351Q", "Pedro Domingez", false, contacto4);
		Client c4 = new Client("54363464T", "Charles Sturidge", true, contacto3);
		Client c5 = new Client("65474723U", "Carmen Sanchez", false, contacto5);
		Client c6 = new Client("12436434R", "Jorge Lopez", true, contacto6);
		Client c7 = new Client("32526463A", "Adrián Izquierdo", true, contacto7);
		Client c8 = new Client("74545543C", "Josep Gandia", true, contacto8);
		Client c9 = new Client("75435433N", "Joan Cano", false, contacto9);
		
		ClientManager.singleton().save(c1);
		ClientManager.singleton().save(c2);
		ClientManager.singleton().save(c3);
		ClientManager.singleton().save(c4);
		ClientManager.singleton().save(c5);
		ClientManager.singleton().save(c6);
		ClientManager.singleton().save(c7);
		ClientManager.singleton().save(c8);
		ClientManager.singleton().save(c9);
		
		LocalDate today = LocalDate.now();
		
		Order order1 = new Order(c1, today, today.plusDays(5), OrderStatus.PREPARADA, 120.0, new ArrayList<OrderLine>());
		Order order2 = new Order(c2, today, today.plusDays(7), OrderStatus.PREPARADA, 100.0, new ArrayList<OrderLine>());
		Order order3 = new Order(c4, today, today.plusDays(15), OrderStatus.PREPARADA, 32.0, new ArrayList<OrderLine>());
		Order order4 = new Order(c6, today, today.plusDays(53), OrderStatus.PENDENT, 32.0, new ArrayList<OrderLine>());
		Order order5 = new Order(c7, today, today.plusDays(27), OrderStatus.PREPARADA, 12.0, new ArrayList<OrderLine>());
		Order order6 = new Order(c8, today, today.plusDays(16), OrderStatus.PREPARADA, 53.0, new ArrayList<OrderLine>());
		Order order7 = new Order(c9, today, today.plusDays(75), OrderStatus.PENDENT, 43.0, new ArrayList<OrderLine>());
		Order order8 = new Order(c2, today, today.plusDays(43), OrderStatus.PENDENT, 12.0, new ArrayList<OrderLine>());
		Order order9 = new Order(c1, today, today.plusDays(83), OrderStatus.PENDENT, 21.0, new ArrayList<OrderLine>());

		
		
		OrderManager.singleton().save(order1);
		OrderManager.singleton().save(order2);
		OrderManager.singleton().save(order3);
		OrderManager.singleton().save(order4);
		OrderManager.singleton().save(order5);
		OrderManager.singleton().save(order6);
		OrderManager.singleton().save(order7);
		OrderManager.singleton().save(order8);
		OrderManager.singleton().save(order9);
		
		ArrayList<Credentials> credenciales = new ArrayList<Credentials>();
		
		Credentials credentials1 = new Credentials("admin", "1234");
		Credentials credentials2 = new Credentials("admin2", "contraseña");
		
		credenciales.add(credentials1);
		credenciales.add(credentials2);
		
		FileIO.serializeMany("shadow", credenciales);
	}
}

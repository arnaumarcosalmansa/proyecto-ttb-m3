package travellingsalesman;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.PrecisionModel;
import org.opengis.geometry.PositionFactory;
import org.opengis.geometry.coordinate.GeometryFactory;
import org.opengis.geometry.primitive.Point;
import org.opengis.geometry.primitive.PrimitiveFactory;

import classes.Client;
import classes.Provider;




public class TravelingSalesman
{
	static GeometryBuilder builder = new GeometryBuilder(DefaultGeographicCRS.WGS84);
	ArrayList<Client> clientes = new ArrayList<Client>();
	
	PositionFactory posF = builder.getPositionFactory();
	PrimitiveFactory primFF = builder.getPrimitiveFactory();
	GeometryFactory geomF = builder.getGeometryFactory();
	
	public static void main(String[] args)
	{
		
		
		Point point = builder.createPoint(41, 41);
		Point point2 = builder.createPoint(42.505516, 1.519708);
		Point point3 = builder.createPoint(45, 4);
		Point point4 = builder.createPoint(56, 103);
		Point point5 = builder.createPoint(13, 1);
		Point point6 = builder.createPoint(16, 178);
		
		ArrayList<Point> puntos = new ArrayList<Point>(Arrays.asList(new Point[] {point2, point3, point4, point6, point5, point}));
		System.out.println(point.distance(point2));
		ArrayList<Point> camino = new ArrayList<Point>();
		double lastResult = startTravelPoint(puntos, camino);
		System.out.println(lastResult);
		while(true)
		{
			camino = new ArrayList<Point>();
			Collections.shuffle(puntos);
			double result = startTravelPoint(puntos, camino);
			System.out.println(result);
			System.out.println(camino);
			if(result != lastResult) {
				System.out.println(puntos);
				throw new RuntimeException ("Ha petado.");
			}
		}
	}
	
	public static double startTravel(ArrayList<Client> clientes, ArrayList<Client> caminoTotal)
	{
		double minDistance = 0;
		for(int i = 0; i < clientes.size(); i++)
		{
			ArrayList<Client> tmpClientes = new ArrayList<Client>(clientes);
			tmpClientes.remove(clientes.get(i));
			ArrayList<Client> caminoHijo = new ArrayList<Client>();
			double distance = travel(clientes.get(i), tmpClientes, caminoHijo);
			if(i == 0 || distance < minDistance)
			{
				caminoTotal.clear();
				caminoTotal.addAll(caminoHijo);
				caminoTotal.add(clientes.get(i));
				minDistance = distance;
			}
		}
		return minDistance;
	}
	
	//POCO EFICIENTE
	//POCO LEGIBLE
	public static double travel(Client start, ArrayList<Client> clientes, ArrayList<Client> camino)
	{
		ArrayList<Client> caminoMasCorto = new ArrayList<Client>();
		Point startPoint = builder.createPoint(start.getContact().getCoords().getLatitude(), start.getContact().getCoords().getLongitude());
		if(clientes.size() == 1) 
		{
			camino.add(clientes.get(0));
			Point endPoint = builder.createPoint(clientes.get(0).getContact().getCoords().getLatitude(), clientes.get(0).getContact().getCoords().getLongitude());
			return startPoint.distance(endPoint);
		}
		
		double minDistance = 0;
		for(int i = 0; i < clientes.size(); i++)
		{
			ArrayList<Client> tmpClientes = new ArrayList<Client>(clientes);
			Client nextStart = clientes.get(i);
			tmpClientes.remove(nextStart);
			Point endPoint = builder.createPoint(clientes.get(i).getContact().getCoords().getLatitude(), clientes.get(i).getContact().getCoords().getLongitude());

			ArrayList<Client> caminoHijo = new ArrayList<Client>();
			double distance = startPoint.distance(endPoint) + travel(nextStart, tmpClientes, caminoHijo);
			if(i == 0 || distance < minDistance)
			{
				caminoMasCorto.clear();
				caminoMasCorto.addAll(caminoHijo);
				caminoMasCorto.add(nextStart);
				minDistance = distance;
			}
		}
		camino.clear();
		camino.addAll(caminoMasCorto);
		return minDistance;
	}
	
	public static double startTravelPoint(ArrayList<Point> clientes, ArrayList<Point> caminoTotal)
	{
		double minDistance = 0;
		for(int i = 0; i < clientes.size(); i++)
		{
			ArrayList<Point> tmpClientes = new ArrayList<Point>(clientes);
			Point nextStart = clientes.get(i);
			tmpClientes.remove(nextStart);
			ArrayList<Point> caminoHijo = new ArrayList<Point>();
			double distance = travelPoint(nextStart, tmpClientes, caminoHijo);
			if(i == 0 || distance < minDistance)
			{
				caminoTotal.clear();
				caminoTotal.addAll(caminoHijo);
				caminoTotal.add(nextStart);
				minDistance = distance;
			}
		}
		return minDistance;
	}
	
	public static double travelPoint(Point start, ArrayList<Point> clientes, ArrayList<Point> camino)
	{
		ArrayList<Point> caminoMasCorto = new ArrayList<Point>();
		if(clientes.size() == 1) 
		{
			camino.add(clientes.get(0));
			return start.distance(clientes.get(0));
		}
		double minDistance = 0;
		for(int i = 0; i < clientes.size(); i++)
		{
			ArrayList<Point> tmpClientes = new ArrayList<Point>(clientes);
			Point nextStart = clientes.get(i);
			tmpClientes.remove(nextStart);
			
			ArrayList<Point> caminoHijo = new ArrayList<Point>();
			double distance = start.distance(clientes.get(i)) + travelPoint(nextStart, tmpClientes, caminoHijo);
			if(i == 0 || distance < minDistance)
			{
				caminoMasCorto.clear();
				caminoMasCorto.addAll(caminoHijo);
				caminoMasCorto.add(nextStart);
				minDistance = distance;
			}
		}
		camino.clear();
		camino.addAll(caminoMasCorto);
		return minDistance;
	}
}

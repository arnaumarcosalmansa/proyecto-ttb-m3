package exceptions;

public class ObjectNotRegistered extends RuntimeException
{
	public ObjectNotRegistered()
	{
		super();
	}

	public ObjectNotRegistered(String message)
	{
		super(message);
	}
}

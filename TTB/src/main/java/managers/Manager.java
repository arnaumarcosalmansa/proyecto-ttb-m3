package managers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classes.Provider;

import java.util.Map.Entry;

import exceptions.ObjectNotRegistered;
import pool.Pool;
import queries.Condition;

public abstract class Manager<T>
{
	protected Pool<T> pool;

	/*
	 * Altes -> meter Baixes -> sacar modify -> modificar search -> buscar
	 */

	public void save(T object)
	{
		pool.add(object);
	}

	public boolean drop(T object)
	{
		return pool.remove(object);
	}

	public void update(T object)
	{
		pool.remove(object);
		pool.add(object);
	}
	
	public void saveOrUpdate(T object)
	{
		if(pool.contains(object))
		{
			this.update(object);
		}
		else
		{
			this.save(object);
		}
	}

	public void update(T object, Map<String, Object> newOptions)
	{
		// la comprovacion del metodo se hace con el nombre del atributo en minusculas
		if (!pool.contains(object))
		{
			throw new ObjectNotRegistered("Object " + object.toString() + " is already registered!");
		}

		Method[] metodos = object.getClass().getMethods();

		for (Entry<String, Object> opt : newOptions.entrySet())
		{
			boolean methodFound = false;
			for (Method m : metodos)
			{
				if (m.getName().startsWith("set"))
				{
					String name = m.getName().substring(3, m.getName().length());
					name = name.substring(0, 1).toLowerCase() + name.substring(1);
					if (opt.getKey().toLowerCase().equals(name))
					{
						Class<?>[] parametros = m.getParameterTypes();

						try
						{
							m.invoke(object, parametros[0].cast(opt.getValue()));
						}
						catch (IllegalAccessException e)
						{
							e.printStackTrace();
						}
						catch (IllegalArgumentException e)
						{
							e.printStackTrace();
						}
						catch (InvocationTargetException e)
						{
							e.printStackTrace();
						}
						finally
						{
							methodFound = true;
						}
					}
				}
			}

			if (!methodFound)
			{
				throw new IllegalArgumentException("Setter with name " + opt.getKey() + " was not found!");
			}
		}
	}

	public Set<T> readAll()
	{
		return (Set<T>) pool.clone();
	}

	public Set<T> search(Condition<T>... condiciones)
	{
		return pool.search(condiciones);
	}

	public static Manager singleton()
	{
		return null;
	}

	public T searchOne(Condition<T>... condiciones)
	{
		Set<T> result = search(condiciones);
		
		if(result.size() > 1)
		{
			throw new RuntimeException("Too many results!");
		}
		
		T ret = null;
		for(T o : result)
		{
			ret = o;
		}
		return ret;
	}
}

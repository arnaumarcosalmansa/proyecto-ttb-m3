package managers;

import classes.Product;
import pool.ProductPool;

public class ProductManager extends Manager<Product>
{
	private static ProductManager instance;
	private ProductManager()
	{
		pool = new ProductPool();
	}

	// return this object, static
	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new ProductManager();
		}

		return instance;
	}
}

package managers;

import java.util.List;
import java.util.Map;
import java.util.Set;

import classes.Order;
import pool.OrderPool;
import queries.Condition;

public class OrderManager extends Manager<Order>
{
	private static OrderManager instance;
	private OrderManager()
	{
		pool = new OrderPool();
	}

	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new OrderManager();
		}

		return instance;
	}
}

package managers;

import classes.Provider;
import pool.ProviderPool;

public class ProviderManager extends Manager<Provider>
{
	private static ProviderManager instance;
	private ProviderManager()
	{
		pool = new ProviderPool();
	}

	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new ProviderManager();
		}

		return instance;
	}
}

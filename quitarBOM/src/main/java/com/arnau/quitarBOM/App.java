package com.arnau.quitarBOM;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
	
	public static final String UTF8_BOM = "\uFEFF";
	static String regex = "^[a-zA-Z0-9>=<\"'? 	-_.@/:]*$";
	static String regexUno = "^[a-zA-Z0-9>=<\"'? 	-_.@/:]$";
	
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Scanner input = new Scanner(System.in);
        while(true)
        {
        	String text = input.nextLine();
        	if(text.isEmpty()) continue;
        	if(!text.matches(regex))
        	{
        		System.out.println("Esto no pasa -> " + text);
        		String espacios = "Esto no pasa -> ";
        		for(int i = 0; i < text.length(); i++)
        		{
        			String sub = text.substring(i, i + 1);
        			if(sub.matches(regexUno))
        				espacios += " ";
        			else
        				espacios += "A";
        		}
        		espacios += "F";
        		System.out.println(espacios);
        	}
        	//System.out.println(text.charAt(0));
        }
    }
}

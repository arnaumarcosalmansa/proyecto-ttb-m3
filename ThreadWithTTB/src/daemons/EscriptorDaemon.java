package daemons;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Semaphore;

import tastat.DiariMoviments;
import tastat.Magatzem;

public class EscriptorDaemon extends Daemon
{
	private Deque<DiariMoviments> moviments;
	private Semaphore semaforo;
	
	public EscriptorDaemon(Magatzem magatzem)
	{
		super();
		this.moviments = magatzem.getMoviments();
		this.semaforo = magatzem.getSemaforoMoviments();
	}
	
	public void run()
	{
		boolean run = true;
		FileWriter fw = null;
		try
		{
			fw = new FileWriter("moviments.txt");
		}
		catch (IOException e1)
		{
			
			e1.printStackTrace();
			run = false;
		}
		
		while(run)
		{
			try
			{
				semaforo.acquire();
				for(DiariMoviments mov : moviments)
				{
					try
					{
						fw.write(mov.toString() + "\n");
						fw.flush();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				moviments.clear();
				semaforo.release();
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

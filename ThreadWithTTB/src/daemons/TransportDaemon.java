package daemons;

import java.util.Deque;
import java.util.List;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;
import tastat.Comanda;
import tastat.ComandaEstat;
import tastat.Magatzem;

public class TransportDaemon extends Daemon
{
	private List<Comanda> cajas;
	private Semaphore semaforo;
	
	private Deque<Comanda> transportadas;
	private Semaphore semaforoTransportadas;
	
	public TransportDaemon(Magatzem mgtzm)
	{
		super();
		this.cajas = mgtzm.getComandasEnCaja();
		this.semaforo = mgtzm.getSemaforoCajas();
		
		this.transportadas = mgtzm.getComandasTransportadas();
		this.semaforoTransportadas = mgtzm.getSemaforoTransportadas();
	}
	
	public void run()
	{
		while(true)
		{
			boolean cajaAcquired = semaforo.tryAcquire();
			boolean transportadasAcquired = semaforoTransportadas.tryAcquire();
			if(cajaAcquired && transportadasAcquired)
			{
				if(cajas.size() > 0 && cajas.get(0).estat(null).equals(ComandaEstat.PREPARADA))
				{
					Comanda c = cajas.remove(0);
					c.estat(ComandaEstat.TRANSPORT);
					this.transportadas.push(c);
				}
			}
			
			if(cajaAcquired) semaforo.release();
			if(transportadasAcquired) semaforoTransportadas.release();
			
			if(cajaAcquired && transportadasAcquired)
			{
				try
				{
					Thread.sleep(5000 / Config.divisorTiempo);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}

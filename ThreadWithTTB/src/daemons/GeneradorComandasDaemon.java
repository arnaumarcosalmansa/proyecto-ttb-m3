package daemons;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;
import tastat.Client;
import tastat.Comanda;
import tastat.ComandaEstat;
import tastat.ComandaLinia;
import tastat.Magatzem;
import tastat.Producte;

public class GeneradorComandasDaemon extends Daemon
{
	private Semaphore semaforoClientes = new Semaphore(1, true);
	private List<Client>  clients = new ArrayList <Client> ();
	
	private Semaphore semaforoProductos = new Semaphore(1, true);
	private List<Producte> productes = new ArrayList <Producte>();
	
	private Semaphore semaforoComandas = new Semaphore(1, true);
	private Deque<Comanda> comandes = new ArrayDeque <Comanda>();
	
	private Random rand = new Random();
	
	public GeneradorComandasDaemon(Magatzem magatzem)
	{
		super();
		this.semaforoClientes = magatzem.getSemaforoClientes();
		this.clients = magatzem.getClients();
		
		this.semaforoProductos = magatzem.getSemaforoProductos();
		this.productes = magatzem.getProductes();
		
		this.semaforoComandas = magatzem.getSemaforoComandas();
		this.comandes = magatzem.getComandes();
	}
	
	public void run()
	{
		int paso = 0;
		
		Client client = null;
		ArrayList<ComandaLinia> linies = new ArrayList<>();
		Comanda comanda = null;
		
		while(true)
		{
			if(paso == 0)
			{
				if(semaforoClientes.tryAcquire())
				{
					int index = rand.nextInt(clients.size());
					client = clients.get(index);
					semaforoClientes.release();
					paso++;
				}
			}
			
			if(paso == 1)
			{
				if(semaforoProductos.tryAcquire())
				{
					Producte p = productes.get(rand.nextInt(productes.size()));
					int q = rand.nextInt(10) + 1;
					ComandaLinia linia = new ComandaLinia(p, q);
					linies.add(linia);
					
					for(int i = 0; i < 3; i++)
					{
						p = productes.get(rand.nextInt(productes.size()));
						q = rand.nextInt(10);
						
						if(q > 0)
						{
							linia = new ComandaLinia(p, q);
							linies.add(linia);
						}
					}
					
					semaforoProductos.release();
					comanda = new Comanda();
					comanda.setClient(client);
					comanda.estat(ComandaEstat.PENDENT);
					comanda.setDataComanda(new Date());
					comanda.setLinies(linies);
					paso++;
				}
			}
			
			if(paso == 2)
			{
				if(semaforoComandas.tryAcquire())
				{
					comandes.add(comanda);
					semaforoComandas.release();
					
					client = null;
					linies = new ArrayList<>();
					comanda = null;
					
					try
					{
						Thread.sleep(20000 / Config.divisorTiempo);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					paso = 0;
				}
			}
		}
	}
}

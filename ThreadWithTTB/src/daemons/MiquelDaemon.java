package daemons;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;
import tastat.Magatzem;
import tastat.Operari;

public class MiquelDaemon extends Daemon
{
	private Random rand = new Random();
	private List<Operari> operarios;
	private Semaphore semaforo;
	
	public MiquelDaemon(Magatzem magatzem)
	{
		super();
		this.operarios = magatzem.getOperaris();
		this.semaforo = magatzem.getSemaforoOperaris();
	}
	
	public void run()
	{
		int cont = 0;
		while(true)
		{
			try
			{
				semaforo.acquire();
				cont++;
				if(cont == 5)
				{
					cont = 0;
					if(operarios.size() > 0)
					{
						int index = rand.nextInt(operarios.size());
						Operari op = operarios.remove(index);
						op.finish();
					}
				}
				semaforo.release();
				Thread.sleep(1000 * 60 / Config.divisorTiempo);
			}
			catch (InterruptedException e1)
			{
				e1.printStackTrace();
			}
		}
	}
}

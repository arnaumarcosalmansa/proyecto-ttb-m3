package daemons;

import java.util.Deque;
import java.util.List;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;
import tastat.Comanda;
import tastat.Magatzem;
import tastat.Operari;
import tastat.OrdreCompra;

public class MonitorDaemon extends Daemon
{
	private Semaphore semaforoOperaris;
	private List<Operari> operaris;
	 
	private Semaphore semaforoComandas;
	private Deque<Comanda> comandes;
	
	private Semaphore semaforoComandasEnCaja;
	private List<Comanda> comandesEnCaja;
	
	private Semaphore semaforoTransportadas;
	private Deque<Comanda> transportadas;
	
	private Semaphore semaforoCompres;
    private Deque<OrdreCompra> compres;
    

    
    public MonitorDaemon(Magatzem magatzem)
    {
    	super();
    	this.semaforoOperaris = magatzem.getSemaforoOperaris();
    	this.operaris = magatzem.getOperaris();
    	
    	this.semaforoComandas = magatzem.getSemaforoComandas();
    	this.comandes = magatzem.getComandes();
    	
    	this.semaforoComandasEnCaja = magatzem.getSemaforoComandasEnCaja();
    	this.comandesEnCaja = magatzem.getComandasEnCaja();
    	
    	this.semaforoCompres = magatzem.getSemaforoCompres();
    	this.compres = magatzem.getCompres();
    	
    	this.transportadas = magatzem.getComandasTransportadas();
		this.semaforoTransportadas = magatzem.getSemaforoTransportadas();
    }
    
    public void run()
    {
    	while(true)
    	{
    		try
    		{
				this.semaforoComandas.acquire();
				System.out.println(
	    				"################################\n" +
	    				"##        MONITOREANDO        ##\n" + 
	    				"################################\n\n\n"
	    				);
	    		System.out.println("MONITOREO DE COMANDAS");
	    		for(Comanda comanda : comandes)
	    		{
	    			System.out.println(comanda.getMonitorData());
	    		}
	    		this.semaforoComandas.release();
			}
    		catch (InterruptedException e2)
    		{
				e2.printStackTrace();
			}
    		
    		try
    		{
				this.semaforoComandasEnCaja.acquire();
				System.out.println("\nCOMANDAS EN CAJA\n");
	    		for(Comanda comanda : comandesEnCaja)
	    		{
	    			System.out.println(comanda.getMonitorData());
	    		}
	    		this.semaforoComandasEnCaja.release();
			}
    		catch (InterruptedException e2)
    		{
				e2.printStackTrace();
			}
    		
    		try
    		{
				this.semaforoTransportadas.acquire();
				System.out.println("\nCOMANDAS TRANSPORTADAS\n");
	    		for(Comanda comanda : transportadas)
	    		{
	    			System.out.println(comanda.getMonitorData());
	    		}
	    		this.semaforoTransportadas.release();
	    		System.out.println("FINAL DE COMANDAS\n\n");
			}
    		catch (InterruptedException e2)
    		{
				e2.printStackTrace();
			}
    		
    		try
    		{
				semaforoOperaris.acquire();
				System.out.println("MONITOREO DE OPERARIOS");
	    		for(Operari operari : operaris)
	    		{
	    			System.out.println(operari.getMonitorData());
	    		}
	    		semaforoOperaris.release();
	    		System.out.println("FINAL DE OPERARIOS\n\n");
			}
    		catch (InterruptedException e1)
    		{
				e1.printStackTrace();
			}
    		

    		try
    		{
				semaforoCompres.acquire();
				System.out.println("MONITOREO DE COMPRAS");

	    		for(OrdreCompra ordre : compres)
	    		{
	    			System.out.println(ordre.getMonitorData());
	    		}
	    		semaforoCompres.release();
	    		System.out.println("FINAL DE COMPRAS\n\n");
	    		
	    		System.out.println(
	    				"################################\n" +
	    				"##            FINAL           ##\n" + 
	    				"################################\n\n\n"
	    				);
			}
    		catch (InterruptedException e1)
    		{
				e1.printStackTrace();
			}
    		
    		
    		try
			{
				Thread.sleep(30000 / Config.divisorTiempo);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
    	}
    }
}

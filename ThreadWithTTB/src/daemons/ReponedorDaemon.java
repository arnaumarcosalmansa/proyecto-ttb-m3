package daemons;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;
import tastat.DiariMoviments;
import tastat.Magatzem;
import tastat.OrdreCompra;
import tastat.Producte;

public class ReponedorDaemon extends Daemon
{
	private Semaphore semaforoCompres;
    private Deque<OrdreCompra> compres;
    
    private Semaphore semaforoMoviments;
    private Deque<DiariMoviments> moviments;
    
	private Semaphore semaforoProductos;
	private List<Producte> productes;
    
	public ReponedorDaemon(Magatzem magatzem)
	{
		super();
		this.semaforoCompres = magatzem.getSemaforoCompres();
		this.compres = magatzem.getCompres();
		
		this.semaforoMoviments = magatzem.getSemaforoMoviments();
		this.moviments = magatzem.getMoviments();
		
		this.semaforoProductos = magatzem.getSemaforoProductos();
		this.productes = magatzem.getProductes();
	}
	
	public void run()
	{
		while(true)
		{
			boolean compresAquired = semaforoCompres.tryAcquire();
			boolean movimentsAquired = semaforoMoviments.tryAcquire();
			//boolean productosAquired = semaforoProductos.tryAcquire();
			boolean hasAquired = (compresAquired && movimentsAquired/* && productosAquired*/);
			
			if(hasAquired)
			{
				if(compres.size() > 0)
				{
					OrdreCompra compra = this.compres.pollFirst();
					Producte p = compra.getProducte();
					int q = compra.getQuantitat();
					p.addStock(q);
					
					DiariMoviments mov = new DiariMoviments();
					mov.setProducte(p);
					mov.setOrdreCompra(compra);
					mov.setQuantitat(q);
					mov.setTipusMoviment('E');
					
					moviments.add(mov);
				}
			}
			
			if(compresAquired) semaforoCompres.release();
			if(movimentsAquired) semaforoMoviments.release();
			//if(productosAquired) semaforoProductos.release();
			
			if(hasAquired)
			{
				try
				{
					Thread.sleep(10000 / Config.divisorTiempo);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}

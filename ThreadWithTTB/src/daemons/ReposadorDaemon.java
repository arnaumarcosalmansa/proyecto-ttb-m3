package daemons;

import java.util.List;
import java.util.concurrent.Semaphore;

import tastat.DiariMoviments;
import tastat.Magatzem;
import tastat.OrdreCompra;
import tastat.Producte;

public class ReposadorDaemon extends Daemon
{
	private Semaphore semaforoCompres;
    private List<OrdreCompra> compres;
    
	private List<DiariMoviments> moviments;
	private Semaphore semaforoMoviments;
	
	private List<Producte> productes;
	private Semaphore semaforoProductes;
	
	public ReposadorDaemon(Magatzem magatzem)
	{
    	this.semaforoCompres = magatzem.getSemaforoCompres();
    	this.compres = magatzem.getCompres();
    	
		this.moviments = magatzem.getMoviments();
		this.semaforoMoviments = magatzem.getSemaforoMoviments();
		
		
		this.productes = magatzem.getProductes();
		this.semaforoProductes = magatzem.getSemaforoProductos();
	}
	
	public void run()
	{
		boolean hasAquired;
		while(true)
		{
			if(hasAquired = (semaforoCompres.tryAcquire() && semaforoMoviments.tryAcquire()))
			{
				if(compres.size() > 0)
				{
					compres.get(0);
					compres.remove(0);
					
					
				}
			}
			semaforoCompres.release();
			semaforoMoviments.release();
			if(hasAquired)
			{
				try
				{
					Thread.sleep(10000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}

package tastat;

import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;

import aplicationpackage.Config;


public class Operari extends Thread{
	
	private boolean running = true;
	
	private int idOperari;
	private String nomOperari;
	private int numPastissos;
	private long tiempoInicio = 0;
	private boolean haPedidoStock = false;
	
	private Deque<Comanda> comandas;
	private Semaphore semaforoComandas;
	
	private List<Comanda> comandasEnCaja;
	private Semaphore semaforoComandasEnCaja;
	
	private Deque<DiariMoviments> moviments;
	private Semaphore semaforoMoviments;
	
	private Deque<OrdreCompra> compres;
	private Semaphore semaforoCompres;
	
	private Semaphore semaforoPasteles;
		
	public Operari(Magatzem magatzem)
	{
		idOperari = Generador.getNextOperari();
		numPastissos = 0;
		
		this.comandas = magatzem.getComandes();
		this.semaforoComandas = magatzem.getSemaforoComandas();
		
		this.comandasEnCaja = magatzem.getComandasEnCaja();
		this.semaforoComandasEnCaja = magatzem.getSemaforoCajas();
		
		this.moviments = magatzem.getMoviments();
		this.semaforoMoviments = magatzem.getSemaforoMoviments();
		
		this.compres = magatzem.getCompres();
		this.semaforoCompres = magatzem.getSemaforoCompres();
		
		this.semaforoPasteles = magatzem.getSemaforoPasteles();
	}
	
	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	public Operari(String nom, Magatzem mg)
	{
		this(mg);
		nomOperari = nom;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

	@Override
	public void run() 
	{
		//registramos el momento de inicio
		tiempoInicio = System.currentTimeMillis();
		while(running)
		{
			//a�adimos un pedido a la caja si es necesario
			addComandaACajaIfNeeded();
			//empezamos a hacer comandas
			hacerComanda();
			//si hemos trabajado m�s de 1 hora dormimos 5 minutos
			if(exceedsTime())
			{
				try
				{
					Thread.sleep(5000 / Config.divisorTiempo);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				//reseteamos el tiempo de inicio
				this.tiempoInicio = System.currentTimeMillis();
			}
		}
	}
	
	private boolean exceedsTime()
	{
		return System.currentTimeMillis() - this.tiempoInicio > 60000 / Config.divisorTiempo;
	}

	private void hacerComanda()
	{
		//obtenemos comanda
		Comanda comanda = obtenerComanda();
		if(comanda != null)
		{
			while(comanda.estat(null).equals(ComandaEstat.PENDENT))
			{
				ComandaLinia linia = comanda.cogerLinia();
				if(linia != null)
				{
					try
					{
						//obtenemos permiso para hacer pasteles (1 de 12)
						this.semaforoPasteles.acquire();
						
						//preparamos el producto
						prepararProductoDeLinia(comanda, linia);
						//Config.safePrint("" + this.idOperari);
						boolean acabada = comanda.a�adirLineaPreparada(linia);
						this.semaforoPasteles.release();
						
						if(acabada)
						{
							comanda.acabar();
						}
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					
					if(this.exceedsTime())
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
		}
	}

	private void prepararProductoDeLinia(Comanda comanda, ComandaLinia linia)
	{
		Producte p = linia.getProducte();
		List<ProducteFabricacio> pasos = p.getFabricacio();
		haPedidoStock = false;
		//hacemos cada paso
		for(int i = 0; i < pasos.size(); i++)
		{
			//cogemos el paso y el ingrediente
			ProducteFabricacio paso = pasos.get(i);
			Producte ingredient = paso.getProducte();
			int stockIngredienteDisponible = ingredient.stock(0);
			int stockIngredienteNecesario = paso.getQuantitat();
			
			//si hay stock
			if(stockIngredienteDisponible >= stockIngredienteNecesario)
			{
				realizarPaso(comanda, paso);
				haPedidoStock = false;
			}
			else //si no hay stock
			{
				//pedimos stock si no hemos pedido antes
				if(!haPedidoStock)
				{
					pedirStock(paso);
					haPedidoStock = true;
				}
				//para repetir el paso
				i--;
			}
		}
		try
		{
			Thread.sleep((Tools.getRandom(1000) + 1000) / Config.divisorTiempo);
			linia.getSemaforo().acquire();
			linia.quantitatPreparada(1);
			linia.getSemaforo().release();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			DiariMoviments moviment = new DiariMoviments(linia.getProducte(), 0, 'E', 1, "");
			
			semaforoMoviments.acquire();
			moviments.add(moviment);
			semaforoMoviments.release();
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}

		this.numPastissos++;
	}
	
	private void realizarPaso(Comanda comanda, ProducteFabricacio paso)
	{
		try
		{
			Producte ingredient = paso.getProducte();
			int stockIngredienteNecesario = paso.getQuantitat();
			//crea el movimiento
			DiariMoviments moviment = new DiariMoviments();
			moviment.setComanda(comanda);
			moviment.setProducte(ingredient);
			moviment.setQuantitat(stockIngredienteNecesario);
			moviment.setTipusMoviment('S');
			//resta el stock
			ingredient.addStock(-stockIngredienteNecesario);
			
			//a�ade el movimiento
			semaforoMoviments.acquire();
			moviments.add(moviment);
			semaforoMoviments.release();
			
			//calcula el tiempo
			long tiempo = paso.getTempsFabricacio();
						
			//duerme
			Thread.sleep((tiempo * 1000) / Config.divisorTiempo);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}	
	}
	
	private void pedirStock(ProducteFabricacio paso)
	{
		try
		{
			Producte ingredient = paso.getProducte();
			//pediremos como minimo la cantidad que necesitamos
			//luego a�adimos un extra que indica la cantidad de compra del producto
			//no tiene sentido pedir un numero que puede ser menor del que necesitas
			int stockIngredienteNecesario = paso.getQuantitat();
			
			//genera la orden de compra
			OrdreCompra compra = new OrdreCompra();
			compra.setProducte(ingredient);
			compra.setQuantitat(stockIngredienteNecesario + ingredient.getQuantitatCompra());
			
			//inserta la orden de compra
			semaforoCompres.acquire();
			boolean exists = false;
			for(OrdreCompra compraExistente : compres)
			{
				if(compraExistente.getProducte().equals(ingredient))
				{
					compraExistente.addQuantitat(stockIngredienteNecesario + ingredient.getQuantitatCompra());
					exists = true;
					break;
				}
			}
			if(!exists)
			{
				compres.add(compra);
			}
			semaforoCompres.release();
			
			//espera para evitar repetir el bucle innecesariamente
			Thread.sleep(100 / Config.divisorTiempo);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private Comanda obtenerComanda()
	{
		//esto no tiene misterio
		Comanda c = null;
		if(semaforoComandasEnCaja.tryAcquire())
		{
			for(int i = 0; i < comandasEnCaja.size(); i++)
			{
				if(comandasEnCaja
						.get(i)
						.estat(null)
						.equals(ComandaEstat.PENDENT))
				{
					c = comandasEnCaja.get(i);
					i = comandasEnCaja.size();
				}
			}
			semaforoComandasEnCaja.release();
		}
		return c;
	}

	private void addComandaACajaIfNeeded()
	{
		//obtenemos los permisos de las cajas y los pedidos
		//miramos si podemos meter pedidos y metemos cuantos podamos hasta 3
		boolean aquiredComandasEnCaja = semaforoComandasEnCaja.tryAcquire();
		if(aquiredComandasEnCaja)
		{
			if(comandasEnCaja.size() < 3)
			{
				if(semaforoComandas.tryAcquire())
				{
					while(comandasEnCaja.size() < 3 && comandas.size() > 0)
					{
						Comanda c = comandas.pollFirst();
						comandasEnCaja.add(c);
					}
					semaforoComandas.release();
				}
			}
			semaforoComandasEnCaja.release();
		}
	}
	
	
	public String getMonitorData()
	{
		return "operari: " + idOperari + " (" + this.nomOperari + ") pastissos: " + numPastissos;
	}
	
	public void finish()
	{
		running = false;
	}
}
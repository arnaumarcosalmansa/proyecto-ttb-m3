package tastat;

import java.util.Set;

public class Missatge{
	
	private Operari operari;
	private String missatge;

	public Missatge(Operari o, String m){
		operari = o;
		missatge = m;
	}
	
	@Override
	public String toString() {
		return (operari.getNomOperari() + ": " + missatge);
	}

}
package tastat;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Tools {
	
	private static Random rnd = new Random();
	
	public static Date sumarDies(Date dataInici, int dies) {
        Calendar cal = Calendar.getInstance(); 
        cal.setTime(dataInici); 
        cal.add(Calendar.DATE, dies);
        return cal.getTime();
	}
	
	public static long getRandom(long max) {
		return ThreadLocalRandom.current().nextLong(max);
	}
}

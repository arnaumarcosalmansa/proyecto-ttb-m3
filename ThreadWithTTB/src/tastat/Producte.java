package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;

public class Producte implements Comparable<Producte> {

	private int codiProducte;
	private String nomProducte;
	private int stock;
	private int stockMinim;
	private UnitatMesura unitat;
	private List<ProducteFabricacio> fabricacio;
	private Tipus tipus;
	private Proveidor proveidor;
	private double preuVenda;
	private double pes;
	private int quantitatCompra;
	private Semaphore semaforoProducto = new Semaphore(1, true);

	public Producte() {
		codiProducte = Generador.getNextProducte();
		fabricacio = new ArrayList<ProducteFabricacio>();
		
		tipus = Tipus.INGREDIENT;
		stockMinim = 0;
		stock = 0;
		quantitatCompra = 100;
	}

	public Producte(String nomProducte){
		this();
		this.nomProducte = nomProducte;
	}
	
	public Producte(String nomProducte, UnitatMesura u, int sm){
		this(nomProducte);
		this.setUnitatMesura(u);
		this.stockMinim = sm;
	};
	
	public Producte(String nomProducte, UnitatMesura u, int sm, Proveidor pv){
		this(nomProducte, u, sm);
		proveidor = pv;
	};
	
	public Producte(String nomProducte, UnitatMesura u, int sm, Proveidor pv, double preu){
		this(nomProducte, u, sm, pv);
		preuVenda = preu;
	};

	
	public UnitatMesura getUnitat() {
		return unitat;
	}

	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}



	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}

	public void setComposicio(List<ProducteFabricacio> fabricacio) {
		this.fabricacio = fabricacio;
	}

	
	public void afegirLot (int quantitat, Date dataCaducitat) {
		int qLot = Generador.getNextLot();
		//lots.add(new LotDesglossat(qLot,dataCaducitat,quantitat));
	}

	
	public int getQuantitatCompra() {
		return this.quantitatCompra;
	}
	
	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nom) {
		nomProducte = nom;
	}

	@Override
	public String toString() {
		String cadena = "Producte: " + codiProducte + "\t - " + nomProducte + "\tStock Total: " + getStock() + " " + unitat ;
		cadena = cadena + "\tStockMínim:" + stockMinim + "\t" + tipus;
		return cadena;
	}

	
	
	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}
	
	public Tipus getTipus() {
		return tipus;
	}
	
	public void setProveidor(Proveidor pv) {
		this.proveidor = pv;
	}
	
	public Proveidor getProveidor() {
		return proveidor;
	}
	
	
	public UnitatMesura getUnitatMesura() {
		return unitat;
	}

	public void setUnitatMesura(UnitatMesura unitatm) {
		unitat = unitatm;
	}

	void setStock(int q) {
		stock = q;
	}

	public void setStockMinim(int stockM) {
		stockMinim = stockM;
		if (stockM!=0)	quantitatCompra = stockM*2; 
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public List <ProducteFabricacio> getFabricacio() {
		return fabricacio;
	}

	public void afegirFabricacioPas(Producte p, int q, int tempsMin, int tempsMax) {
		
		ProducteFabricacio pf = new ProducteFabricacio(p,q,tempsMin,tempsMax);
		
		fabricacio.add(pf);
	}

	public String veureComposicio() {
		String cadena = "";
		cadena = getNomProducte() + " --> ";
		for (ProducteFabricacio pf : this.getFabricacio()) 
			cadena += pf.getProducte().getNomProducte() + "(" + pf.getProducte().getNomProducte() + ") ";
		return cadena;
	}
	
	@Override
	public int compareTo(Producte p) {
		return (getNomProducte().compareTo(p.getNomProducte()));
	}

	public int getCodiProducte() {
		return codiProducte;
	}
	
	public int getStock() {
		return stock;
	}
	
	public synchronized void addStock(int q)
	{
		this.stock += q;
	}
	
	public Semaphore getSemaforoProducto() {
		return this.semaforoProducto;
	}

	public synchronized int stock(int i)
	{
		this.stock += i;
		return this.stock;
	}
}
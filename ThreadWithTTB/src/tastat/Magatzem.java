package tastat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Magatzem {
	
	private Semaphore semaforoProductos = new Semaphore(1, true);
	private List<Producte> magatzem = new ArrayList <Producte>();
	
	private Semaphore semaforoClientes = new Semaphore(1, true);
	private List<Client>  clients = new ArrayList <Client> ();
	
	private Semaphore semaforoComandas = new Semaphore(1, true);
	private Deque<Comanda> comandes = new ArrayDeque <Comanda>();
	
	private Semaphore semaforoProveidors = new Semaphore(1, true);
    private List<Proveidor> proveidors = new ArrayList <Proveidor>(); 
    
    private Semaphore semaforoOperaris = new Semaphore(1, true);
    private List<Operari> operaris = new ArrayList <Operari>();
    
    private Semaphore semaforoMoviments = new Semaphore(1, true);
    private Deque<DiariMoviments> moviments = new ArrayDeque<DiariMoviments>();
    
    private Semaphore semaforoPasteles = new Semaphore(12, true);
    
    private Deque<Comanda> comandasTransportadas = new ArrayDeque<Comanda>();
    private Semaphore semaforoTransportadas = new Semaphore(1, true);
    
    public Deque<Comanda> getComandasTransportadas() {
		return comandasTransportadas;
	}

	public Semaphore getSemaforoTransportadas() {
		return semaforoTransportadas;
	}

	public Semaphore getSemaforoPasteles() {
		return semaforoPasteles;
	}

	public Deque<DiariMoviments> getMoviments()
	{
		return moviments;
	}

	private Semaphore semaforoCompres = new Semaphore(1, true);
    private Deque<OrdreCompra> compres = new ArrayDeque<OrdreCompra>();
    
    public Deque<OrdreCompra> getCompres()
	{
		return compres;
	}

	private Semaphore semaforoCajas = new Semaphore(1, true); //solo 3 porque solo caven 3 cajas
    private List<Caja> cajas = new ArrayList<Caja>();
    
    private Semaphore semaforoComandasEnCaja = new Semaphore(1, true); //solo 3 porque solo caven 3 cajas
    private List<Comanda> comandasEnCaja = new ArrayList<Comanda>();
	
    public Magatzem(){
	}
	
	public Magatzem(List<Producte> lp, List<Client> lc, Deque<Comanda> lm, List<Proveidor> lpv, List<Operari> lop, Deque<DiariMoviments> ldm, Deque<OrdreCompra> loc){
		magatzem = lp;
		clients = lc;
		comandes = lm;
		proveidors = lpv;
		operaris = lop;
		moviments = ldm;
		compres = loc;
	}
	
	public boolean afegirQuantitatProducte(int codiProducte, int quantitat){
		boolean trobat = false;
		for (Producte p: magatzem) {
			if (p.getCodiProducte() == codiProducte) {
				p.setStock(p.getStock() + quantitat);
				trobat = true;
				break;
			}
		}
		return trobat;
	}
	
	public void addCaja(Caja caja) {
		this.cajas.add(caja);
	}
	
	public boolean afegirProducte(Producte prod) {
		boolean trobat = false;
		for (Producte p: magatzem) {
			if (p.getCodiProducte() == prod.getCodiProducte()) {
				trobat = true;
				break;
			}
		}
		if (trobat) 
			return false;
		else {
			magatzem.add(prod);
			return true;
		}
	}
	
	public List <Producte>getProductes(){
		return magatzem;
	}
	
	public List <Client> getClients(){
		return clients;
	}
	
	public Deque<Comanda> getComandes(){
		return comandes;
	}
	
	public List<Proveidor> getProveidors(){
		return proveidors;
	}
	
	public List<Operari> getOperaris(){
		return operaris;
	}	
	
	public boolean add(Producte p) {
		magatzem.add(p);
		return true;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		for (Producte p : magatzem)
			s +=  "\n" + p;
		s += "\nTotal " + magatzem.size() + " Refer�ncies";
		return s;
	}
	
	public void veureComandes() {
		for(Comanda c :this.getComandes()) {
			System.out.println(c);
		}
	}
	
	/**
	 * GETTERS Y SETTERS DE SEMAFOROS
	 */

	public Semaphore getSemaforoProductos() {
		return semaforoProductos;
	}

	public Semaphore getSemaforoClientes() {
		return semaforoClientes;
	}

	public Semaphore getSemaforoComandas() {
		return semaforoComandas;
	}

	public Semaphore getSemaforoProveidors() {
		return semaforoProveidors;
	}

	public Semaphore getSemaforoOperaris() {
		return semaforoOperaris;
	}

	public Semaphore getSemaforoMoviments() {
		return semaforoMoviments;
	}

	public Semaphore getSemaforoCompres() {
		return semaforoCompres;
	}
	
	public Semaphore getSemaforoCajas() {
		return this.semaforoCajas;
	}
	
	public void addCaja(Comanda com) throws InterruptedException {
		this.cajas.add(new Caja(com));
	}

	public List<Caja> getCajas()
	{
		return cajas;
	}

	public Semaphore getSemaforoComandasEnCaja() {
		return semaforoComandasEnCaja;
	}

	public List<Comanda> getComandasEnCaja() {
		return comandasEnCaja;
	}
}

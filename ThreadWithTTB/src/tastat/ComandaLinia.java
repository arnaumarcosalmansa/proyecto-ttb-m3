package tastat;

import java.util.concurrent.Semaphore;

import aplicationpackage.Config;

public class ComandaLinia {
	private Producte producte;
	private int quantitat;
	private double preuVenda;
	private int quantitatPreparada;
	private int quantitatPendent;
	
	private Semaphore semaforoCantidad = new Semaphore(1, true);
    
	public Semaphore getSemaforo() {
		return semaforoCantidad;
	}

	public ComandaLinia(Producte p, int q, double preu) {
		producte = p;
		quantitat = q;
		preuVenda = preu;
		quantitatPreparada = 0;
		quantitatPendent = q;
	}
	
	public ComandaLinia(Producte p, int q){
		producte = p;
		quantitat = q;
		preuVenda = p.getPreuVenda();
		quantitatPreparada = 0;
		quantitatPendent = q;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;		
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public int getQuantitatPreparada() {
		return quantitatPreparada;
	}

	public void setQuantitatPreparada(int quantitatPreparada) {
		this.quantitatPreparada = quantitatPreparada;
	}

	public int getQuantitatPendent() {
		return quantitatPendent;
	}

	public void setQuantitatPendent(int quantitatPendent) {
		this.quantitatPendent = quantitatPendent;
	}
	
	public synchronized void afegirQuantitatPreparada(int quantitat) {
		this.quantitatPendent -= quantitat;
		this.quantitatPreparada += quantitat;
	}

	public synchronized boolean isAcabada()
	{
		boolean acabada = false;
		try
		{
			semaforoCantidad.acquire();
			//Config.safePrint("pendent : " + this.quantitatPendent + " preparada : " + this.quantitatPreparada + " cantidad : " + this.quantitat);
			acabada = this.quantitatPreparada == this.quantitat;
			semaforoCantidad.release();

		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		return acabada;
	}

	public int quantitatPendent(int i)
	{
		this.quantitatPendent += i;
		return this.quantitatPendent;
	}

	public int quantitatPreparada(int i)
	{
		this.quantitatPreparada += i;
		return this.quantitatPreparada;
	}
	
	public String toString()
	{
		return  "producto: " + this.getProducte().getNomProducte() + " cantidad: " + this.getQuantitat() + " hecha: " + this.getQuantitatPreparada() + " pendiente: " + this.getQuantitatPendent();
	}
}
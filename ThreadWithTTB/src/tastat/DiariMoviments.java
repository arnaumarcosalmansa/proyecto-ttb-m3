package tastat;

import java.util.Date;

public class DiariMoviments {
	private int numMoviment;
	private Date moment;
	private Producte producte;
	private int lot;
	private char tipusMoviment;
	private int quantitat;
	private String observacions;
	private Comanda comanda;
	private OrdreCompra ordreCompra;
	
	public DiariMoviments(){
		numMoviment = Generador.getNextDiariMoviment();
		moment = new Date();
	}
	
	public int getNumMoviment() {
		return numMoviment;
	}

	public void setNumMoviment(int numMoviment) {
		this.numMoviment = numMoviment;
	}

	public Date getMoment() {
		return moment;
	}

	public void setMoment(Date moment) {
		this.moment = moment;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getLot() {
		return lot;
	}

	public void setLot(int lot) {
		this.lot = lot;
	}

	public char getTipusMoviment() {
		return tipusMoviment;
	}

	public void setTipusMoviment(char tipusMoviment) {
		this.tipusMoviment = tipusMoviment;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public String getObservacions() {
		return observacions;
	}

	public void setObservacions(String observacions) {
		this.observacions = observacions;
	}

	public Comanda getComanda() {
		return comanda;
	}

	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}

	public OrdreCompra getOrdreCompra() {
		return ordreCompra;
	}

	public void setOrdreCompra(OrdreCompra ordreCompra) {
		this.ordreCompra = ordreCompra;
	}

	DiariMoviments(Producte p, int l, char tipusM, int q, String obs) {
		this();
		producte = p;
		lot = l;
		tipusMoviment = tipusM;
		quantitat = q;
		observacions = obs;
	}
	
	@Override
	public String toString() {
		return "num movimiento: " + this.getNumMoviment() + "\tproducte: " + this.producte.getNomProducte() + "\tquantitat: " + this.quantitat + "\ttipo: " + this.tipusMoviment;
	}
	
}
package tastat;

import java.util.HashMap;
import java.util.Map;

import tastat.*;

public class Caja {
	
	private Comanda comanda;
	private Map<ComandaLinia, Integer> contenido; //comandalinia, cantidad fabricada
	private Boolean complet = false;
	
	//crea la caja
	public Caja(Comanda comanda) 
	{
		this.comanda = comanda;
		this.contenido = new HashMap<ComandaLinia, Integer>();
	}
	
	public void setComplet(boolean complet) {
		this.complet = complet;
	}
	
	//a�ade un pastel al map
	public void pastelAdd(ComandaLinia com) 
	{
		if(contenido.containsKey(com)) 
		{
			contenido.replace(com, contenido.get(com) + 1);
		}
		else 
		{
			contenido.put(com, 1);
		}
	}
	
	//devuelve el total de pasteles creados
	public Integer totalPasteles() 
	{
		Integer cantidad = 0;
		for(ComandaLinia com: contenido.keySet()) 
		{
			cantidad = contenido.get(com) + cantidad;
		}
		return cantidad;
	}
	
	//indica si ya se ha terminado de fabricar la linia de comanda
	public boolean liniaFin(ComandaLinia linia) {
		
		return false;
	}
	
	
}

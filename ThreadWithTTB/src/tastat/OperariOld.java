package tastat;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;


public class OperariOld extends Thread{
	
	private boolean hacerCosas = true;
	
	private int idOperari;
	private String nomOperari;
	private int numPastissos;
	
	private static Magatzem magatem; //temporal??
	private static Comanda comandaActual = null;
	private static Caja caja;
	private static Map<ComandaLinia, Semaphore> semaforosLineas = new HashMap<ComandaLinia, Semaphore>();
	
	public OperariOld(Magatzem em){
		idOperari = Generador.getNextOperari();
		numPastissos = 0;
		magatem = em;
	}
	
	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	public OperariOld(String nom, Magatzem mg){
		nomOperari = nom;
		magatem = mg;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

	@Override
	public void run() 
	{
		try 
		{	
			do 
			{
				//mostramos los datos iniciales.
				System.out.println("Max Operarios: " + magatem.getSemaforoOperaris().availablePermits() + "\n" + 
						"Max Cajas: " + magatem.getSemaforoCajas().availablePermits() + "\n" +
						"Max comandas: " + magatem.getSemaforoComandas().availablePermits());
				
				if(comandaActual == null && magatem.getSemaforoCajas().availablePermits() > 0) //esto puede traer problemas
				{
					if(magatem.getSemaforoCajas().tryAcquire()) 
					{
						if(magatem.getSemaforoComandas().tryAcquire()) //esto puede traer problemas
						{   /*
							si podemos adquirir el pase, cogemos la primera comanda de la cola, creamos en el almacen una caja para esta comanda, limpiamos la lista, y a�adimos un sem�foro por cada comanda linea, 
							cada sem�foro con su respectiva cantidad, finalmente asignamos la comanda a comandaActual, la cual es con la que trabajamos y hacemos un release
							*/
							Comanda com = magatem.getComandes().poll();
							caja = new Caja(com);
							semaforosLineas.clear();
							for(ComandaLinia coml: com.getLinies()) semaforosLineas.put(coml, new Semaphore(coml.getQuantitat(), true));
							comandaActual = com;
							magatem.getSemaforoComandas().release();
						}
						else 
						{ //en caso contrario devolvemos el valor del sem�foro de cajas que hemos obtenido 
							magatem.getSemaforoCajas().release();
						}
					}
				}
				else 
				{
					if(magatem.getSemaforoOperaris().availablePermits() > 0 && readyForProduction()) 
					{ //si se permiten mas operarios y estamos listos para fabricar
						if(magatem.getSemaforoOperaris().tryAcquire()) //adquirimos un valor del sem�foro de operarios
						{
							System.out.println("Ha entrado");
							runLineas();//llamamos a la funci�n que producir� los pasteles???? tmp
							
							magatem.getSemaforoOperaris().release(); //hacemos un release del sem�foro de operarios, no lo hace
							if(magatem.getSemaforoOperaris().availablePermits() == 12)
							{ //miramos si eres el ultimo, si lo eres, pones la comanda actual a null, ya que esta actual ya se a completado
								
								System.out.println("Final");
								caja.setComplet(true);
								magatem.addCaja(caja);
								comandaActual = null;
								
							}
						}
					}
					else 
					{//en caso de que no puedas entrar, por alg�n motivo de los anteriores te duermes con un precioso sleep de 10s
						Thread.sleep(10000);
					}
				}
			}while(hacerCosas); //el buen bucle infinito
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	//tmp, simula las lineas, en pruebas
	private void runLineas() throws InterruptedException {
		
		do {
			ComandaLinia aFabricar = getComandaLiniaAFabricar();
			if(aFabricar != null) { //fabricas el prodicto
				for(ProducteFabricacio fabricacio: aFabricar.getProducte().getFabricacio()) {
					boolean result = false;
					do {
						if(fabricacio.getProducte().getSemaforoProducto().tryAcquire()) {
						
							//if(fabricacio.getProducte().getStock() >= fabricacio.getQuantitat()) {
								//falta guardar en diari moviments
								
								DiariMoviments mov = new DiariMoviments();
								mov.setComanda(comandaActual);
								mov.setProducte(aFabricar.getProducte());
								mov.setTipusMoviment('S');
								mov.setQuantitat(aFabricar.getQuantitat());
								System.out.println("Tab: " + this.getName() + " Grabado en diari moviments, \n" + mov);
								//no se si se deve tocar el lot
								
								//comentado por pruebas
								//fabricacio.getProducte().setStock(fabricacio.getProducte().getStock() - fabricacio.getQuantitat());	
							
								result = true;
							//}
							/*else {
								//peticion a almacen
								magatem.getCompres().add(new OrdreCompra(null, aFabricar.getProducte(), aFabricar.getProducte().getQuantitatCompra()));
								System.out.println("Peticion a almacen");
							}*/
							
							fabricacio.getProducte().getSemaforoProducto().release();
						}
						else {
							Thread.sleep(3000);
						}
					}while(!result);
					System.out.println("Hace la fabricacion");
					Thread.sleep(fabricacio.getTempsFabricacio()); //simula la fabricacion
				}
				caja.pastelAdd(aFabricar);//se suma un pastel de esta linea de comanda mas a la caja
			}
			
		}while(readyForProduction());
	}
	
	//devuelve true si quedan pasteles por fabricar
	private boolean readyForProduction() 
	{
		boolean ok = false;
		for(ComandaLinia lincom: semaforosLineas.keySet()) 
		{
			if(semaforosLineas.get(lincom).availablePermits() > 0) ok = true;
		}
		return ok;
	}
	
	//parecida a readyForProduction, solo que esta devuelve la linea que de la cual el operario debe fabricar el producto
	private ComandaLinia getComandaLiniaAFabricar() throws InterruptedException 
	{ 
		ComandaLinia comFab = null;
			
		for(ComandaLinia lincom: semaforosLineas.keySet()) 
		{
			if(comFab == null) 
			{
				if(semaforosLineas.get(lincom).tryAcquire()) 
				{
					comFab = lincom;
				}
			}
		}
		
		return comFab;
	}
	
	//muestra datos, esto es tmp
	private void mostData() {
		synchronized (semaforosLineas) {
			String e = "name: " + this.nomOperari;
			e = e + " comanda: " + comandaActual.getIdComanda() + "\n";
			for(ComandaLinia lincom: semaforosLineas.keySet()) {
				e = e + "Total a fabricar: " + lincom.getQuantitat() + ", pendiente: " + semaforosLineas.get(lincom).availablePermits() + "\n";
			}
			System.out.println(e);
		}
	}
	
	public String getMonitorData()
	{
		return "operari: " + idOperari + " pastissos: " + numPastissos;
	}
	
	public void finish()
	{
		hacerCosas = false;
	}
}
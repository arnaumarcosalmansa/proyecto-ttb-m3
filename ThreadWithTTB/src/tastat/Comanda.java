package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import aplicationpackage.Config;

public class Comanda {
	
	private int idComanda;
	private Client client;
	private Date dataComanda;
	private Date dataLliurament;   
	private ComandaEstat estat;	//PENDENT - PREPARAT - TRANSPORT - LLIURAT
	private Double portes;		//preu de transport
	private int numLinies;
	private List<ComandaLinia> liniesAbsolutes = new ArrayList<ComandaLinia>();
	private List<ComandaLinia> liniesPendents;
	private List<ComandaLinia> liniesPreparades;
	
	public Comanda() {
		idComanda = Generador.getNextComanda();
		dataComanda = new Date();
		dataLliurament = Tools.sumarDies(new Date(), 1);
		estat = ComandaEstat.PENDENT;
		portes = 0.0;
		liniesPendents = new ArrayList<ComandaLinia>();
		liniesPreparades = new ArrayList<ComandaLinia>();
	}

	public Comanda(Client client) {
		this();
		this.client = client;
	};	
	
	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public Double getPortes() {
		return portes;
	}

	public void setPortes(Double portes) {
		this.portes = portes;
	}

	public void setLinies(List<ComandaLinia> linies) {
		this.liniesPendents = linies;
		this.liniesAbsolutes.addAll(linies);
		this.numLinies = this.liniesPendents.size();
	}

	public List <ComandaLinia> getLinies (){
		return liniesPendents;
	}

	@Override 
	public String toString() {
		String cadena = "Comanda: " + getIdComanda() + " Client: " + getClient().getNomClient() + " " + estat(null) + "\n";
		for(ComandaLinia cl:getLinies()) 
			cadena = cadena + "  --> Producte: " + cl.getProducte().getNomProducte() + " Quantitat: " + cl.getQuantitat() + "\n";
		return cadena;
	}
	
	public String getMonitorData()
	{
		String ret = "comanda: " + idComanda + " estat: " + this.estat;

		//if(this.estat.equals(ComandaEstat.PENDENT))
		{
			for(ComandaLinia linia : this.liniesAbsolutes)
			{
				try
				{
					linia.getSemaforo().acquire();
					ret += "\nproducto: " + linia.getProducte().getNomProducte() + " cantidad: " + linia.getQuantitat() + " hecha: " + linia.getQuantitatPreparada() + " pendiente: " + linia.getQuantitatPendent() + " preparando: " + (linia.getQuantitat() - linia.getQuantitatPendent() - linia.getQuantitatPreparada());
					linia.getSemaforo().release();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
		return ret;
	}
	
	public synchronized ComandaLinia cogerLinia()
	{
		ComandaLinia linia = null;
		if(this.liniesPendents.size() > 0)
		{
			linia = this.liniesPendents.get(0);
			try
			{
				linia.getSemaforo().acquire();
				if(linia.getQuantitatPendent() == 1)
				{
					linia = this.liniesPendents.remove(0);
				}
				linia.quantitatPendent(-1);
				linia.getSemaforo().release();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		return linia;
	}
	
	public void aņadirLinea(ComandaLinia linia)
	{
		this.liniesPendents.add(linia);
		this.liniesAbsolutes.add(linia);
		this.numLinies++;
	}
	
	public synchronized boolean aņadirLineaPreparada(ComandaLinia linia)
	{
		if(linia.isAcabada())
		{
			this.liniesPreparades.add(linia);
		}
		
		//loop revisar que las lineas esten acabadas
		boolean acabada = this.liniesPreparades.size() == this.liniesAbsolutes.size();
		for(int i = 0; i < this.liniesAbsolutes.size() && acabada; i++)
		{
			acabada &= liniesAbsolutes.get(i).isAcabada();
		}
		return acabada;
	}
	
	public synchronized void acabar()
	{
		this.estat = ComandaEstat.PREPARADA;
	}

	public synchronized ComandaEstat estat(ComandaEstat estat)
	{
		if(estat != null)
		{
			this.estat = estat;
		}
		return this.estat;
	}
}
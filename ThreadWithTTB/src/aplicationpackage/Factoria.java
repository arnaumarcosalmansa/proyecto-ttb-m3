package aplicationpackage;

import java.util.ArrayList;
import java.util.List;

import daemons.EscriptorDaemon;
import daemons.GeneradorComandasDaemon;
import daemons.MiquelDaemon;
import daemons.MonitorDaemon;
import daemons.ReponedorDaemon;
import daemons.TransportDaemon;
import tastat.*;

public class Factoria {

	private static Magatzem mg; 
	
	public static void runFactoria(Magatzem mag) {
		mg = mag;
		
		if(mg == null) {
			System.out.println("El almacen es nullo");
		}
		
		List<Operari> opers = mg.getOperaris();
		
		for(Operari oper: opers) {
			oper.start();
		}
		
		new EscriptorDaemon(mg).start();
		new GeneradorComandasDaemon(mg).start();
		new MiquelDaemon(mg).start();
		new MonitorDaemon(mg).start();
		new ReponedorDaemon(mg).start();
		new TransportDaemon(mg).start();
	}
}

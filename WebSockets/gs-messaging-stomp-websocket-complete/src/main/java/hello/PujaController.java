package hello;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class PujaController {

	public static Subasta subasta = null;
	public static ArrayList<Puja> pujas = new ArrayList<Puja>();
	
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        //if(true) throw new IllegalArgumentException("bamboozled");
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
    
    @MessageMapping("/puja")
    @SendTo("/answer/puja")
    public Puja pujar(Puja puja) throws Exception {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        //if(true) throw new IllegalArgumentException("bamboozled");
    	return puja;
    }
    
    @MessageExceptionHandler
    @SendToUser("/answer/error")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
    
    public static Timer timer = new Timer();
    public static void startTimer() {
    	timer.schedule(new TimerTask() {

			@Override
			public void run() {
				
			}
    		
    	}, 0, 500);
    }
}

package hello;

import java.util.Date;

public class Subasta {

	private String producto = null;
	private int precioSalida = 0;
	private Date inicio = new Date();

	public Subasta(String producto, int precioSalida, Date inicio) {
		super();
		this.producto = producto;
		this.precioSalida = precioSalida;
		this.inicio = inicio;
	}

	public Subasta() {
		super();
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public int getPrecioSalida() {
		return precioSalida;
	}

	public void setPrecioSalida(int precioSalida) {
		this.precioSalida = precioSalida;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

}

package hello;

import java.util.Date;

public class Puja {
	
	private String persona = null;
	private int cantidad = 0;
	private Date momento = null;

	public Puja(String persona, int cantidad, Date momento) {
		super();
		this.persona = persona;
		this.cantidad = cantidad;
		this.momento = momento;
	}

	public Puja() {
		super();
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getMomento() {
		return momento;
	}

	public void setMomento(Date momento) {
		this.momento = momento;
	}
}

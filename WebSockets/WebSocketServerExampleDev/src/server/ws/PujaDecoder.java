package server.ws;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;

public class PujaDecoder implements Decoder.Text<Puja> {
	private static Gson gson = new Gson();

	// @Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	// @Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub

	}

	// @Override
	public Puja decode(String arg0) throws DecodeException {
		return gson.fromJson(arg0, Puja.class);
	}

	// @Override
	public boolean willDecode(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}

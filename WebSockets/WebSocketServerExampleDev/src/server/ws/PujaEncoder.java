package server.ws;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;


public class PujaEncoder implements Encoder.Text<Puja> {

	private static Gson gson = new Gson();

	//@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public String encode(Puja arg0) throws EncodeException {
		// TODO Auto-generated method stub
		return gson.toJson(arg0);
	}

}

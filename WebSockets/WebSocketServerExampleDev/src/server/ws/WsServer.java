package server.ws;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/websocketendpoint", decoders = PujaDecoder.class, encoders = PujaEncoder.class)
public class WsServer {

	static Subasta subasta = new Subasta();
	
	private Session session;
	private static Set<WsServer> clientEndpoints = new CopyOnWriteArraySet<WsServer>();
	private static HashMap<String, String> users = new HashMap<String, String>();
	
	@OnOpen
	public void onOpen() {
		System.out.println("Open Connection ...");
	}

	@OnClose
	public void onClose() {
		System.out.println("Close Connection ...");
	}

	@OnMessage
	public String onMessage(String message) {
		System.out.println("Message from the client: " + message);
		String echoMsg = "Echo from the server : " + message;
		return echoMsg;
	}

	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}

	private void broadcast(Puja puja) {
		
	}
}

package queries;

import java.util.Iterator;
import java.util.List;

public class Filter<T> implements Iterator<T>
{

	private int index = 0;

	private List<T> list;
	private Condition<T>[] conditions;

	public Filter(Condition<T>... conditions)
	{
		this.conditions = conditions;
	}

	public Filter(List<T> list, Condition<T>... conditions)
	{
		this.list = list;
		this.conditions = conditions;
	}

	public void setList(List<T> list)
	{
		this.list = list;
	}

	public void setConditions(Condition<T>... conditions)
	{
		this.conditions = conditions;
	}

	public boolean hasNext()
	{
		// TODO Auto-generated method stub
		int validity = 0;

		if (index > list.size() - 1)
		{
			index = 0;
			return false;
		}

		while (validity < conditions.length)
		{
			for (int i = 0; i < conditions.length; i++)
			{
				// System.out.println("index: " + index);
				if (!conditions[i].check(list.get(index)))
				{
					validity = 0;
					index++;
					break;
				}
				else
				{
					validity++;
				}
			}

			if (index > list.size() - 1)
			{
				index = 0;
				return false;
			}
		}

		return true;
	}

	public T next()
	{
		// TODO Auto-generated method stub
		index++;
		return list.get(index - 1);
	}

}

package queries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class QueryableList<T extends Comparable<? super T>> extends ArrayList<T> implements Queryable<T>, Sortable<T>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Filter<T> filter;

	public QueryableList()
	{
		super();
	}

	public QueryableList(int size)
	{
		super(size);
	}

	public QueryableList<T> query(Condition<T>... conditions)
	{
		return this.query(new Filter<T>(conditions));
	}

	public QueryableList<T> query(Filter<T> filter)
	{
		QueryableList<T> result = new QueryableList<T>();

		filter.setList(this);

		this.filter = filter;

		for (T item : this)
		{
			result.add(item);
		}

		return result;
	}

	public void sort()
	{
		Collections.sort(this);
	}

	public void sortBy(Comparator<? super T>... sortCriteria)
	{
		if (sortCriteria.length < 1)
		{
			return;
			// throw new IllegalArgumentException("Array too short.");
		}

		Collections.sort(this, sortCriteria[0]);

		if (sortCriteria.length == 1)
		{
			return;
		}

		ArrayList<QueryableList<T>> splitted = splitEquals(sortCriteria[0]);

		Comparator<? super T>[] subCriteria = Arrays.copyOfRange(sortCriteria, 1, sortCriteria.length);

		for (int j = 0; j < splitted.size(); j++)
		{
			splitted.get(j).sortBy(subCriteria);
		}

		QueryableList<T> merged = new QueryableList<T>();

		for (int i = 0; i < splitted.size(); i++)
		{
			for (int j = 0; j < splitted.get(i).size(); j++)
			{
				merged.add(splitted.get(i).get(j));
			}
		}

		this.clear();
		this.addAll(merged);
	}

	private ArrayList<QueryableList<T>> splitEquals(Comparator<? super T> comparator)
	{
		ArrayList<QueryableList<T>> splitted = new ArrayList<QueryableList<T>>();

		QueryableList<T> current = new QueryableList<T>();
		splitted.add(current);
		current.add(this.get(0));

		for (int i = 0; i < this.size() - 1; i++)
		{
			if (comparator.compare(this.get(i), this.get(i + 1)) != 0)
			{
				current = new QueryableList<T>();
				splitted.add(current);
				current.add(this.get(i + 1));
			}
			else
			{
				current.add(this.get(i + 1));
			}
		}

		return splitted;
	}

	public QueryableList setFilter(Condition... conditions)
	{
		this.filter = new Filter(conditions);
		return this;
	}

	public QueryableList setFilter(Filter filter)
	{
		this.filter = filter;
		return this;
	}

	public Iterator<T> iterator()
	{
		if (filter != null)
		{
			return filter;
		}
		else
		{
			return super.iterator();
		}
	}
}

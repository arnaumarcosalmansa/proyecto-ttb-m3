package queries;

public abstract class Condition<T>
{
	protected Object[] var;

	public Condition()
	{

	}

	public Condition(Object... vars)
	{
		this.var = vars;
	};

	public abstract boolean check(T item);

}

package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import clases.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import managers.*;
import queries.Condition;

public class Controler implements Initializable {
	
	//controles para filtros
	@FXML private TextField inputIdProduct;
	@FXML private CheckBox trencaStockCheck;
	@FXML private Button refreshButton;
	@FXML private TextField inputNameProduct;
	@FXML private TextField inputProviderCif;
	@FXML private Button searchProviderButton;
	
	//creamos la tabla con la clase que mostrara, ojo con el nombre de estas ya que hacen referencia a la id de la tabla en la ventana
	@FXML private TableView<Product> tablaProdu;
	//igual para cada una de las columnas
	@FXML private TableColumn idProColumn;
	@FXML private TableColumn nameProColumn;
	@FXML private TableColumn desProColumn;
	@FXML private TableColumn preuVnedaProColumn;
	@FXML private TableColumn totalStockProColumn;
	@FXML private TableColumn minStockProColumn;
	@FXML private TableColumn mesuraUnityProColumn;
	@FXML private TableColumn tipusProColumn;
	@FXML private TableColumn idProveidorProColumn;
	//contiene los objetos a mostrar
	private ObservableList<Product> productos;
		
	public Controler() {
		
	}
		
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.iniciarTabla();
		/*final ObservableList<Product> tablaProductosSel = tablaProdu.getSelectionModel().getSelectedItems();
		tablaProductosSel.addL*/
			
		/*Provider pp = new Provider("jamonero", "ES339033", null);
		//a�ade productos, en un futuro extraer de su almacen...
		for(int i = 0; i < 10; i++) {
			Product p = new Product("Jamon" + i, UnityMeasure.UNITAT, 10);
			p.setProveidor(pp);
			productos.add(p);
		}*/
		Manager<Product> pm = ProductManager.singleton();
		Condition<Product> er = new Condition<Product>() {

			@Override
			public boolean check(Product item) {
				return true;
			}
			
		};
		Set<Product> productos2 = pm.search(er);
		for(Product prod: productos2) {
			System.out.println(prod);
			System.out.println(prod.getUnitatMesura());
			productos.add(prod);
		}
			
	}
		
	//notas-adri: Los atributos de la clase deven llamarse igual que los setters y getters
	private void iniciarTabla() {
		idProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("IdProduct"));
		nameProColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("nameProduct"));
		//desProColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("null"));
		preuVnedaProColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
			
		totalStockProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
		minStockProColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("minStock"));
		mesuraUnityProColumn.setCellValueFactory(new PropertyValueFactory<Product, UnityMeasure>("unitatMesura"));
		tipusProColumn.setCellValueFactory(new PropertyValueFactory<Product, Types>("type"));
			
		//usa su methodo toString
		idProveidorProColumn.setCellValueFactory(new PropertyValueFactory<Product, Provider>("proveidor"));
			
		productos = FXCollections.observableArrayList();
		tablaProdu.setItems(productos);
	}
	
	@FXML private void actionEvent(ActionEvent event) {
		Object desencadenante = event.getSource();
		
		if(desencadenante instanceof Button) {
			
			Button bb = (Button) desencadenante;
			Condition<Product> id = null;
			if(!this.inputIdProduct.getText().equals("")) {
				
				System.out.println(inputIdProduct.getText());
				id = Product.iqualId(Integer.parseInt(inputIdProduct.getText()));
			}
			Manager<Product> pm = ProductManager.singleton();
			if(id != null) {
				
				Set<Product> productos2 = pm.search(id);
				
				productos.removeAll(productos);
				
				for(Product prod: productos2) {
					System.out.println(prod);
					System.out.println(prod.getUnitatMesura());
					productos.add(prod);
				}
			}
			else {
				productos.removeAll(productos);
				Condition<Product> er = new Condition<Product>() {

					@Override
					public boolean check(Product item) {
						return true;
					}
				};
				Set<Product> productos2 = pm.search(er);
				for(Product prod: productos2) {
					System.out.println(prod);
					System.out.println(prod.getUnitatMesura());
					productos.add(prod);
				}
			}
			
		}
	}
	
	/*Filtramos las letras para que solo se puedan introducir numeros*/
	@FXML public void onKeyTyped(KeyEvent val) {
	
		char a = val.getCharacter().charAt(0);
		
		if(!Character.isDigit(a)) {
			val.consume();
		}
	}
}

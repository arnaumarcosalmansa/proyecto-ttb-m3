package application;
	
import java.net.URL;
import java.nio.file.Paths;

import clases.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import managers.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

/*https://www.youtube.com/watch?v=OT_qBWKiRfY*/
public class Main extends Application {
	
	private Controler control;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setResizable(false);
			primaryStage.setTitle("Productor");
			
			URL file = Paths.get("src\\application\\window.fxml").toUri().toURL();
			
			/*FXMLLoader load = FXMLLoader.load(file);
			this.control = load.getController();*/
			
			Scene scene = new Scene( FXMLLoader.load(file),950,500);
			scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Manager<Product> pm = ProductManager.singleton();
		Provider per = new Provider("Mike Mallers", "ES09092122", new ContactInfo("C/ alegria 123", "Barcelona", "Spain"));
		Product p = new Product("Galletas", UnityMeasure.GRAMS,4);
		p.setVendible();
		p.setProveidor(per);
		pm.register(p);
		
		per = new Provider("Alfonso X el Savio", "ES01012933", new ContactInfo("C/ El manzano 33", "Madrid", "Spain"));
		
		p = new Product("Elados de fresa", UnityMeasure.UNITAT, 10);
		p.setVendible();
		p.setProveidor(per);
		pm.register(p);
		
		p = new Product("Mantequilla", UnityMeasure.GRAMS, 10);
		p.setProveidor(per);
		pm.register(p);
		
		launch(args);
	}
}

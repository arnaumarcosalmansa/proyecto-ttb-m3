package clases;

import queries.Condition;

public class Coordinates
{
	private double latitude;
	private double longitude;

	public Coordinates()
	{

	}

	public Coordinates(double lat, double lon)
	{
		this();
		this.latitude = lat;
		this.longitude = lon;
	}

	public double getLatitude()
	{
		return this.latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return this.longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}
	
	public String toString()
	{
		return "[ " + this.getLatitude() + ", " + this.getLongitude() + " ]";
	}
	
	public Double distanceTo(Coordinates coordinates)
	{
		double deltaLatitude = coordinates.getLatitude() - this.getLatitude();
		double deltaLongitude = coordinates.getLongitude() - this.getLongitude();
		deltaLatitude *= deltaLatitude;
		deltaLongitude *= deltaLongitude;
		double distance = Math.sqrt(deltaLatitude + deltaLongitude);
		return distance;
	}
	
	public final static Condition<Coordinates> closerToCoords(Coordinates coords, double maxDistanceExclusive)
	{
		Condition<Coordinates> condition = new Condition<Coordinates>(coords, maxDistanceExclusive)
		{
			@Override
			public boolean check(Coordinates item)
			{
				return item.distanceTo((Coordinates) var[0]) < (Double) var[1];
			}
		};
		return condition;
	}

	public final static Condition<Coordinates> furtherToCoords(Coordinates coords, double minDistanceInclusive)
	{
		Condition<Coordinates> condition = new Condition<Coordinates>(coords, minDistanceInclusive)
		{
			@Override
			public boolean check(Coordinates item)
			{
				return item.distanceTo((Coordinates) var[0]) >= (Double) var[1];
			}
		};
		return condition;
	}

	public final static Condition<Coordinates> inRangeToCoords(Coordinates coords, double minDistanceInclusive, double maxDistanceExclusive)
	{
		Condition<Coordinates> condition = new Condition<Coordinates>(coords, minDistanceInclusive, maxDistanceExclusive)
		{
			@Override
			public boolean check(Coordinates item)
			{
				double distance = item.distanceTo((Coordinates) var[0]);
				return  (Double) var[1] <= distance && distance < (Double) var[2];
			}
		};
		return condition;
	}
}

package clases;

public enum OrderStatus
{
	PENDENT,
	PREPARADA,
	TRANSPORT,
	LLIURADA
}

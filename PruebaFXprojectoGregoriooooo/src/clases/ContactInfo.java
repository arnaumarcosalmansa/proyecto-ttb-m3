package clases;

import queries.Condition;

public class ContactInfo
{
	private String address;
	private String poblacio;
	private String country;
	private String personaContacte;
	private String phone;
	private Coordinates coords;

	public ContactInfo()
	{

	}

	public ContactInfo(String address, String poblacio, String country)
	{
		this.setAddress(address);
		this.setPoblacio(poblacio);
		this.setCountry(country);
		this.coords = new Coordinates();
	}

	public void setCoordinates(Coordinates coordinates)
	{
		this.coords = coordinates;
	}

	public String getAddress()
	{
		return this.address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPoblacio()
	{
		return this.poblacio;
	}

	public void setPoblacio(String poblacio)
	{
		this.poblacio = poblacio;
	}

	public String getCountry()
	{
		return this.country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getPersonaContacte()
	{
		return this.personaContacte;
	}

	public void setPersonaContacte(String personaContacte)
	{
		this.personaContacte = personaContacte;
	}

	public String getPhone()
	{
		return this.phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Coordinates getCoords()
	{
		return this.coords;
	}
	
	private String[] getAllDataString()
	{
		String[] data = new String[6];
		data[0] = this.getAddress();
		data[1] = this.getPoblacio();
		data[2] = this.getCountry();
		data[3] = this.getPersonaContacte();
		data[4] = this.getPhone();
		data[5] = this.getCoords().toString();
		
		return data;
	}
	
	public final static Condition<ContactInfo> contactInfoContains(String regex)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(regex)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				String[] allData = item.getAllDataString();
				boolean matches = false;
				for(int i = 0; !matches && i < allData.length; i++)
				{
					matches = allData[i].matches((String) var[0]);
				}
				return matches;
			}
		};
		return condition;
	}
	
	public final static Condition<ContactInfo> coordsMatchConditions(Condition<Coordinates>... conditions)
	{
		Condition<ContactInfo> condition = new Condition<ContactInfo>(conditions)
		{
			@Override
			public boolean check(ContactInfo item)
			{
				boolean matches = true;
				for(int i = 0; matches && i < conditions.length; i++)
				{
					matches = ((Condition)var[i]).check(item);
				}
				return matches;
			}
		};
		return condition;
	}
}

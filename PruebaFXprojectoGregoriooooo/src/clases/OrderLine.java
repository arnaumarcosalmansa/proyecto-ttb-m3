package clases;

public class OrderLine
{
	private Product producte;
	private int quantity;
	private double price; // preu venda

	public OrderLine(Product p, int q, double price)
	{
		this.producte = p;
		this.quantity = q;
		this.price = price;
	}

	public Product getProducte()
	{
		return this.producte;
	}

	public int getQuantity()
	{
		return this.quantity;
	}

	public double getPrice()
	{
		return this.price;
	}

	/*
	 * 
	 */
}
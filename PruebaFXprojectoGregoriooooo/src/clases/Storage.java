package clases;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

public class Storage
{

	private List<Product> magatzem = new ArrayList<Product>();
	private List<Client> clients = new ArrayList<Client>();
	private List<Order> comandes = new ArrayList<Order>();
	private List<Provider> proveidors = new ArrayList<Provider>();

	public Storage()
	{
	}

	public Storage(List<Product> lp, List<Client> lc, List<Order> lm, List<Provider> lpv)
	{
		magatzem = lp;
		clients = lc;
		comandes = lm;
		proveidors = lpv;
	}

	public boolean afegirQuantitatProducte(int codiProducte, int quantitat)
	{
		boolean trobat = false;
		for (Product p : magatzem)
		{
			if (p.getIdProduct() == codiProducte)
			{
				p.setStock(p.getStock() + quantitat);
				trobat = true;
				break;
			}
		}
		return trobat;
	}

	public boolean afegirProducte(Product prod)
	{
		boolean trobat = false;
		for (Product p : magatzem)
		{
			if (p.getIdProduct() == prod.getIdProduct())
			{
				trobat = true;
				break;
			}
		}
		if (trobat)
			return false;
		else
		{
			magatzem.add(prod);
			return true;
		}
	}

	// baixa d'un producte
	public Product treureProducte(int codiProducte)
	{
		Iterator<Product> productes = this.magatzem.iterator();
		Product tmp = null;
		boolean ok = true;
		while (productes.hasNext() && ok)
		{
			tmp = productes.next();
			if (tmp.getIdProduct() == codiProducte)
			{
				productes.remove();
				ok = false;
			}
		}
		if (!ok)
			return tmp;
		return null;
	}

	// m

	// buscar producte
	public Product searchProduct(int codiProducte)
	{
		for (Product prod : this.magatzem)
		{
			if (prod.getIdProduct() == codiProducte)
				return prod;
		}
		return null;
	}

	public Product searchProduct(String nameProducte)
	{
		for (Product prod : this.magatzem)
		{
			if (prod.getNameProduct() == nameProducte)
				return prod;
		}
		return null;
	}

	public Product searchProduct(int codiProducte, String nameProducte)
	{
		for (Product prod : this.magatzem)
		{
			if (prod.getNameProduct() == nameProducte && prod.getIdProduct() == codiProducte)
				return prod;
		}
		return null;
	}
	// fin buscar producte

	public List<Product> getProductes()
	{
		return magatzem;
	}

	public List<Client> getClients()
	{
		return clients;
	}

	public List<Order> getComandes()
	{
		return comandes;
	}

	public List<Provider> getProveidors()
	{
		return proveidors;
	}

	public Deque<Batch> apilarCaducats()
	{
		Iterator<Batch> it = null;
		Deque<Batch> pila = new ArrayDeque<Batch>();
		Batch ldg;
		LocalDate avui = LocalDate.now();
		for (Product p : magatzem)
		{
			it = p.getLots().iterator();
			while (it.hasNext())
			{
				ldg = it.next();
				if (ldg.getDataCaducitat().compareTo(avui) < 0)
				{
					pila.push((ldg));
					it.remove();
				}
			}
		}
		return pila;
	}

	// igual a afegirProducte
	/*
	 * public boolean add(Producte p) { //magatzem.add(p); return true; }
	 */

	@Override
	public String toString()
	{
		String s = "";

		for (Product p : magatzem)
			s += "\n" + p;
		s += "\nTotal " + magatzem.size() + " Refer�ncies";
		return s;
	}

}

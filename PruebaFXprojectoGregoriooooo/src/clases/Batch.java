package clases;

import java.util.Calendar;
import java.util.Date;

import queries.Condition;

public class Batch implements Comparable<Batch>
{
	private int quantity = 1;
	private Date entryDate = new Date();
	private Date expirationDate = null;
	private int id; // numero de lote, id

	public Batch(int qLot, int duracio, int quantitat)
	{
		if (qLot < 0)
			throw new IllegalArgumentException("Batch quantity cannot be negative: " + qLot);

		id = qLot;
		entryDate = new Date();
		this.quantity = quantitat;

		if (duracio > 0)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(entryDate);
			calendar.add(Calendar.DAY_OF_YEAR, duracio);
			expirationDate = calendar.getTime();
		}
	}

	public Batch(int qLot, Date dataCaducitat, int quantitat)
	{
		id = qLot;
		entryDate = new Date();
		this.expirationDate = dataCaducitat;
		this.quantity = quantitat;
	}

	public Date getDataCaducitat()
	{
		return expirationDate;
	}

	public Date getDataEntrada()
	{
		return entryDate;
	}

	public Integer getQuantitat()
	{
		return quantity;
	}

	public void setQuantitat(int q)
	{
		quantity = q;
	}

	public Integer getId()
	{
		return this.id;
	}

	@Override
	public String toString()
	{
		return ("Lot: " + id + "\tQuantitat: " + quantity + "\tData Entrada: " + entryDate + "\tData Caducitat: "
				+ expirationDate);
	}

	@Override
	public int compareTo(Batch l)
	{
		if (this.getDataCaducitat() == null && l.getDataCaducitat() == null)
			return (this.getDataEntrada().compareTo(l.getDataEntrada()));

		if (this.getDataCaducitat() != null && l.getDataCaducitat() != null)
			return (this.getDataCaducitat().compareTo(l.getDataCaducitat()));

		if (this.getDataCaducitat() == null)
			return -1;
		return 1;
	}

	/*
	 * iqualLot iqualQuantitat moreQuantitat lessQuantitat iqualDataEntrada
	 * iqualDataCaducitat
	 */

	public final static Condition<Batch> equalId(Integer id)
	{
		Condition<Batch> condition = new Condition<Batch>(id)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getId() == id)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> equalQuantitat(Integer quantitat)
	{
		Condition<Batch> condition = new Condition<Batch>(quantitat)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantitat() == quantitat)
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> quantityBiggerThan(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantitat() > (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> quantityLessThan(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantitat() < (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> quantityEquals(Integer quantity)
	{
		Condition<Batch> condition = new Condition<Batch>(quantity)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getQuantitat() == (Integer) var[0])
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> quantityBetween(Integer minInclusive, Integer maxExclusive)
	{
		Condition<Batch> condition = new Condition<Batch>(minInclusive, maxExclusive)
		{
			@Override
			public boolean check(Batch item)
			{
				if ((Integer) var[0] <= item.getId() && item.getId() < (Integer) var[1])
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> entryDateEquals(Date entrada)
	{
		Condition<Batch> condition = new Condition<Batch>(entrada)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getDataEntrada().equals((Date) var[0]))
					return true;
				return false;
			}
		};
		return condition;
	}

	public final static Condition<Batch> exitDateEquals(Date sortida)
	{
		Condition<Batch> condition = new Condition<Batch>(sortida)
		{
			@Override
			public boolean check(Batch item)
			{
				if (item.getDataCaducitat().equals(sortida))
					return true;
				return false;
			}
		};
		return condition;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Batch other = (Batch) obj;
		if (this.id != other.getId())
			return false;
		return true;
	}

}
package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import queries.Condition;

public class Order
{
	private int id;
	private Client client;
	private LocalDate orderDate;
	private LocalDate deliveryDate;
	private OrderStatus status; // PENDENT - PREPARAT - TRANSPORT - LLIURAT
	private Double portes; // preu de transport
	private List<OrderLine> linies;

	public Order()
	{
		this.id = Generator.getNextComanda();
		this.orderDate = LocalDate.now();
		this.deliveryDate = LocalDate.now().plusDays(1);
		this.status = OrderStatus.PENDENT;
		this.portes = 0.0;
		this.linies = new ArrayList<OrderLine>();
	}

	public Order(Client client)
	{
		this();
		this.client = client;
	}

	public List<OrderLine> getLinies()
	{
		return linies;
	}

	public int getId()
	{
		return this.id;
	}

	public Client getClient()
	{
		return this.client;
	}

	public LocalDate getOrderDate()
	{
		return this.orderDate;
	}

	public LocalDate getDeliveryDate()
	{
		return this.deliveryDate;
	}

	public OrderStatus getStatus()
	{
		return this.status;
	}

	public void setStatus(OrderStatus status)
	{
		this.status = status;
	}

	public double getPortes()
	{
		return this.portes;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	

	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
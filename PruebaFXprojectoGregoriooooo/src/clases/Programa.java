package clases;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.List;

public class Programa
{

	public static void main(String[] args)
	{
		// 1.- Generaci� d'un magatzem petit�
		Storage elMeuMagatzem = new Storage(new ArrayList<Product>(), new ArrayList<Client>(), new ArrayList<Order>(),
				new ArrayList<Provider>());
		generarDadesBasiques(elMeuMagatzem);
		System.out.println("Veure Magatzem:");
		System.out.println(elMeuMagatzem);

		// 2.- Veure la composici� dels productes del magatzem
		for (Product p : elMeuMagatzem.getProductes())
			if (p.getType() == ProductType.VENDIBLE)
				System.out.println(p.veureComposicio());

		// 3.- Veure magatzem ordenat per nom producte
		elMeuMagatzem.getProductes().sort(null);
		System.out.println("3.- Magatzem ordenat per nomProducte");
		System.out.println(elMeuMagatzem);

		// 4.- Veure magatzem ordenat per stock

		System.out.println("3.- Magatzem ordenat per Stock");
		elMeuMagatzem.getProductes().sort(new CompareStock());
		System.out.println(elMeuMagatzem);
	}

	private static void generarDadesBasiques(Storage mgz)
	{

		// Prove�dors
		Provider pv1 = new Provider("UNOproveidor", "ES020202020", new ContactInfo("C/hola", "sabadell", "Spain"));
		Provider pv2 = new Provider("DOSproveidor", "ES020202029", new ContactInfo("C/hola33", "sabadell", "Spain"));

		// Productes, composici� i lots
		Product pliv = new Product("pLiviano", UnityMeasure.UNITAT, 4);
		Product pllim = new Product("pLLimona", UnityMeasure.UNITAT, 6);
		LocalDate dataCaducitat;
		Product p = new Product("sucre", UnityMeasure.GRAMS, 100000);

		dataCaducitat = LocalDate.now().plusDays(10);
		p.afegirLot(40000, dataCaducitat);
		p.afegirLot(30000, dataCaducitat);
		dataCaducitat = dataCaducitat.plusDays(20);
		p.afegirLot(70000, dataCaducitat);

		p.setProveidor(pv1);
		mgz.afegirProducte(p);

		pliv.afegirComponent(p, 115);
		pllim.afegirComponent(p, 4);

		p = new Product("ous", UnityMeasure.UNITAT, 240);
		p.afegirLot(480, dataCaducitat);
		mgz.afegirProducte(p);

		pliv.afegirComponent(p, 4);

		p = new Product("farina", UnityMeasure.GRAMS, 30000);
		p.afegirLot(10000, dataCaducitat);
		p.afegirLot(20000, dataCaducitat);

		p.setProveidor(pv1);
		mgz.afegirProducte(p);

		pliv.afegirComponent(p, 115);

		p = new Product("llevadura", UnityMeasure.GRAMS, 5000);
		p.afegirLot(200, (LocalDate.now()));
		dataCaducitat = LocalDate.now().plusDays(-5);
		p.afegirLot(400, dataCaducitat);
		dataCaducitat = LocalDate.now().plusDays(5);
		p.afegirLot(100, dataCaducitat);

		p.setProveidor(pv1);
		mgz.afegirProducte(p);

		pliv.afegirComponent(p, 10);
		pllim.afegirComponent(p, 8);

		Product pSec = new Product("Secret", UnityMeasure.UNITAT, 0);
		pSec.setStock(100);

		pliv.afegirComponent(pSec, 1);
		pllim.afegirComponent(pSec, 1);

		p = new Product("nabius", UnityMeasure.GRAMS, 4000);
		dataCaducitat = LocalDate.now().plusDays(15);
		p.afegirLot(2000, dataCaducitat);
		p.setProveidor(pv2);
		mgz.afegirProducte(p);

		pSec.afegirComponent(p, 100);
		mgz.afegirProducte(p);

		p = new Product("llimona", UnityMeasure.GRAMS, 4000);
		dataCaducitat = LocalDate.now().plusDays(15);
		p.afegirLot(2000, dataCaducitat);

		p.setProveidor(pv2);
		mgz.afegirProducte(p);

		pliv.afegirComponent(p, 40);
		pllim.afegirComponent(p, 4);

		p = new Product("albahaca", UnityMeasure.GRAMS, 4000);
		dataCaducitat = LocalDate.now().plusDays(15);
		p.afegirLot(2000, dataCaducitat);

		p.setProveidor(pv2);
		mgz.afegirProducte(p);

		pllim.afegirComponent(pSec, 20);

		pliv.setType(ProductType.VENDIBLE);
		pllim.setType(ProductType.VENDIBLE);

		pliv.setPrice(20);
		pllim.setPrice(15);
		pllim.setStock(18);

		mgz.afegirProducte(pliv);
		mgz.afegirProducte(pllim);

		// clients
		Client c1 = new Client("La Canasta", 39.1174353, -5.7933869);
		Client c2 = new Client("Baires", 41.5442476, 2.0604163);
		Client c3 = new Client("Pierre Herme", 48.8513876, 2.3304912);
		Client c4 = new Client("Aux Pains de Papy", 51.5293753, -0.1903852);
		Client c5 = new Client("La Santiaguesa", 40.9284811, -5.2618384);
		mgz.getClients().add(c1);
		mgz.getClients().add(c2);
		mgz.getClients().add(c3);
		mgz.getClients().add(c4);
		mgz.getClients().add(c5);

		Order m1 = new Order(c1);
		m1.getLinies().add(new OrderLine(pliv, 100, 20));
		m1.getLinies().add(new OrderLine(pllim, 40, 12));
		mgz.getComandes().add(m1);

		m1 = new Order(c1);
		m1.getLinies().add(new OrderLine(pllim, 20, 15));
		m1.getLinies().add(new OrderLine(pllim, 4, 0));
		mgz.getComandes().add(m1);

		m1 = new Order(c1);
		m1.getLinies().add(new OrderLine(pliv, 50, 18));
		mgz.getComandes().add(m1);

		m1 = new Order(c1);
		m1.getLinies().add(new OrderLine(pliv, 20, 20));
		m1.getLinies().add(new OrderLine(pllim, 1, 0));
		mgz.getComandes().add(m1);

	}

}

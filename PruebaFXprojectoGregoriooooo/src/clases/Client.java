package clases;

import java.util.Set;

import queries.Condition;

public class Client
{
	private String name;
	private String CIF;
	private boolean active;
	private ContactInfo contact;

	public Client()
	{
		contact = new ContactInfo();
	}

	public Client(String nom)
	{
		this();
		name = nom;
	}

	public Client(String nom, double lat, double lon)
	{
		this(nom);
		contact.setCoordinates(new Coordinates(lat, lon));
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCIF()
	{
		return this.CIF;
	}

	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public ContactInfo getContact()
	{
		return this.contact;
	}

	

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CIF == null) ? 0 : CIF.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (CIF == null)
		{
			if (other.CIF != null)
				return false;
		}
		else if (!CIF.equals(other.CIF))
			return false;
		return true;
	}

}
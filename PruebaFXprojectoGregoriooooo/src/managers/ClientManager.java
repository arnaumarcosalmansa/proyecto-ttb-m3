package managers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import pool.ClientPool;
import queries.Condition;
import clases.Client;
import exceptions.ObjectNotRegistered;

public class ClientManager extends Manager<Client>
{
	private ClientManager()
	{
		pool = new ClientPool();
	}

	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new ClientManager();
		}
		return instance;
	}

}

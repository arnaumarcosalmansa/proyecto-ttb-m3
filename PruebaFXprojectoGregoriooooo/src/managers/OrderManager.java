package managers;

import java.util.List;
import java.util.Map;
import java.util.Set;

import clases.Order;
import pool.OrderPool;
import queries.Condition;

public class OrderManager extends Manager<Order>
{
	private OrderManager()
	{
		pool = new OrderPool();
	}

	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new OrderManager();
		}

		return instance;
	}
}

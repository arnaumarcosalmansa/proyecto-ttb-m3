package managers;

import clases.Provider;
import pool.ProviderPool;

public class ProviderManager extends Manager<Provider>
{
	private ProviderManager()
	{
		pool = new ProviderPool();
	}

	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new ProviderManager();
		}

		return instance;
	}
}

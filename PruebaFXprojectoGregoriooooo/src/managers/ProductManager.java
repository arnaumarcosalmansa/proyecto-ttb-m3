package managers;

import clases.Product;
import pool.ProductPool;

public class ProductManager extends Manager<Product>
{
	// constructor
	private ProductManager()
	{
		pool = new ProductPool();
	}

	// return this object, static
	public static Manager singleton()
	{
		if (instance == null)
		{
			instance = new ProductManager();
		}

		return instance;
	}
}

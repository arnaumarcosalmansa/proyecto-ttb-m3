var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/answer/start', (mensaje) => {
        	displaySubasta(mensaje.body);
        });
        stompClient.subscribe('/answer/puja', (puja) => {
            showPuja(JSON.parse(puja.body));
        });
        stompClient.subscribe('/user/answer/start', (mensaje) => {
        	console.log("aviso usuario subasta");
        	displaySubasta(mensaje.body);
        });
        stompClient.subscribe('/user/answer/update', (pujas) => {
        	pujas = JSON.parse(pujas.body);
        	pujas.forEach((puja) => {
                showPuja(puja);
        	})
        });
        stompClient.subscribe('/answer/error', (error) => {
            displayError(error.body);
        });
        stompClient.subscribe('/answer/final', (message) => {
        	displayError(message.body);
        	cleanPujas();
        	displaySubasta('No se está subastando nada');
        });
        stompClient.subscribe('/user/answer/error', (error) => {
            displayError(error.body);
        });
        stompClient.subscribe('/user/answer/final', (error) => {
            displayError(error.body);
            cleanPujas();
        });
        
        stompClient.send("/app/fullupdate", {});
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendPuja() {
    stompClient.send("/app/puja", {}, JSON.stringify({'id': -1, 'persona': $("#name").val(), 'cantidad': $("#quantity").val(), 'momento': new Date()}));
}

/*
 * function showPuja(puja) { $("#greetings").append(`<tr><td>${puja.id}</td><td>${puja.persona}</td><td>${puja.cantidad}</td><td>${puja.momento}</td></tr>`); }
 */

function showPuja(puja) {
    $("#output").append(`<p class="last">${puja.momento} -- ${puja.id} -- ${puja.persona} -- ${puja.cantidad}</p>`);
}

function cleanPujas() {
	$("#output").empty();
}

function displayError(error) {
    let modal = document.getElementById("modal");
    modal.style.display = 'block';
    console.log(modal.getElementsByTagName('p'));
    modal.getElementsByTagName('p')[0].innerHTML = error;
}

function displaySubasta(subasta) {
	let subastah4 = document.getElementById('subasta');
	subastah4.innerHTML = subasta;
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendPuja(); });
});


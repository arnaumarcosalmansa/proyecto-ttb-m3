package hello;

import java.util.Date;

public class Puja {

	private int id = 0;
	private String persona = null;
	private String sessionId = null;
	private int cantidad = 0;
	private Date momento = new Date();

	public Puja(int id, String persona, int cantidad, Date momento) {
		super();
		this.id = id;
		this.persona = persona;
		this.cantidad = cantidad;
		this.momento = momento;
	}

	public Puja() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getMomento() {
		return momento;
	}

	public void setMomento(Date momento) {
		this.momento = momento;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}

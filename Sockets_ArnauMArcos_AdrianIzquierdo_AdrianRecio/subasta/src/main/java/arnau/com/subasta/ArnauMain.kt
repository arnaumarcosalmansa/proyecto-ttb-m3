package arnau.com.subasta

import hibernate.*;
import dataobjects.Cliente

fun main(args: Array<String>)
{
	DataSource.source.clientes.list();
	val cliente = Cliente();
	cliente.nombre = "arnau"
	cliente.contrasena = "1234"
	cliente.saldoInicial = 500
	cliente.saldoActual = 200
	
	DataSource.source.clientes.saveOrUpdate(cliente)
	
	DataSource.source.clientes.list();

}
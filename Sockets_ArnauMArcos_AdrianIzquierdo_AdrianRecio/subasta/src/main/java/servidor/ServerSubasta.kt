package servidor

import java.util.Scanner;
import java.net.ServerSocket
import java.net.Socket
import java.io.IOException
import java.util.concurrent.Semaphore
import java.net.SocketException
import java.net.MulticastSocket
import messages.Message
import dataobjects.Lote
import java.util.Timer
import java.util.TimerTask
import java.util.Date
import arnau.com.subasta.Config
import messages.subasta.EndSubasta
import dataobjects.Licitacion
import messages.subasta.StartSubastaMessage
import hibernate.Hibernate
import messages.EchoMessage

fun main(args: Array<String>) {

	Init.registrarProductos()
	Init.registrarLotes()
	Init.registrarUsuarios()

	val input: Scanner = Scanner(System.`in`);
	ServerSubasta.server.start()


	var command: String?;
	do {
		command = input.nextLine();

		when {
			command == "stop" -> {
				println("stoping server")
				ServerSubasta.server.stopServer()
			}
			command == "totalClientes" ->
				println("Clientes totales: " + ServerSubasta.server.getTotalClients())
			command.startsWith("echo") -> {
				ServerSubasta.server.sendToAllLogged(EchoMessage(command.substring(5)))
			}
			command == "help" -> {
				println("\n--------- Comandos del Sistema ----------")
				println("stop => Deteiene el servidor, así como sus clientes,\n ej: stop.");
				println("totalClientes => Muestra el numero actual de clientes conectados a este servidor, \n ej: totalClientes.")
				println("------- Fin Comandos del Sistema -------")
			}
			else ->
				println("¡¡El comando introducido no existe!!");
		}

	} while (command != "stop");
	input.close();
}

public class ServerSubasta(val portNumber: Int) : Thread() {
	private var running: Boolean = false;
	private var server: ServerSocket = ServerSocket(portNumber);
	private var multicast: MulticastSocket = MulticastSocket()
	private var clientes: ArrayList<ClientDispatcher> = ArrayList<ClientDispatcher>();
	private val serverTimeOut = 5000;

	private var mutexClientes: Semaphore = Semaphore(1, true);

	init {
		//server.setSoTimeout(serverTimeOut);
		//this.start();
		this.running = true;

		ServerSubasta.subasta = Subasta(Lote())
	}

	//el servidor trabaja
	public override fun run() {
		do {
			try {
				//var client : Socket = server.accept();

				//mutexClientes.acquire();
				val socket = server.accept()
				val cm = ClientDispatcher(socket, subasta, mutexSubasta)
				
				cm.start()
				println("nueva conexión desde ${socket.inetAddress}")
				
				mutexClientes.acquire()
				clientes.add(cm)
				mutexClientes.release();


			} catch (soe: SocketException) {

			} catch (ioe: IOException) {
				println(ioe.message)
				ioe.printStackTrace()
			}
		} while (running);

		this.stopClientsDispatchers(false); //paramos a los clientes forzandolos
	}

	//paramos todos los threads hijos.
	private fun stopClientsDispatchers(forced : Boolean) {
		mutexClientes.acquire();
		
		clientes.forEach {
			try {
				it.stop(forced);
			} catch (e: Throwable) {
				println(e.message);
			}
		}

		mutexClientes.release()
		
		//timerSubasta.finish()
		timerSubasta.cancel()
		timerSubasta.purge()
	}

	//indica al servidor que pare
	public fun stopServer() {
		println("stopping")
		this.running = false
		server.close()
		Hibernate.sessionFactory!!.close()
	}

	//devuelve el numero de clientes
	public fun getTotalClients(): Int {
		mutexClientes.acquire();
		var total = this.clientes.size;
		mutexClientes.release();

		return total;
	}

	public fun sendToAllLogged(message: Message) {
		mutexClientes.acquire();
		this.clientes.forEach {
			if (it.logged) {
				try{
					it.oos.writeObject(message)
					it.oos.flush()
				}
				catch(e : Exception) {
					println("El cliente se ha desconectado por causas desconocidas.")
				}
			}
		}
		mutexClientes.release();
	}
	
	public fun notifyDisconnect(dispatcher: ClientDispatcher) : Boolean {
		this.mutexClientes.acquire()
		val resultado = this.clientes.remove(dispatcher)
		this.mutexClientes.release()
		return resultado
	}

	companion object {

		val server = ServerSubasta(Config.portNumber)
		var subasta: Subasta? = null
			get
			private set

		var subastaSavedAt = Date()
			get
			private set

		val mutexSubasta = Semaphore(1, true)

		val timerSubasta = Timer()

		init {
			timerSubasta.schedule(object : TimerTask() {
				override fun run() {
					val now = System.currentTimeMillis()
					Subasta.permit.acquire()
					Subasta.instancia?.let {
						var lastTime : Long
						if (it.licitaciones.isNotEmpty()) {
							lastTime = it.licitaciones.last().momento.time
						} else {
							lastTime = it.startedAt.time
						}
						
						if (now - lastTime > 30000) {
							var pujaGanadora: Licitacion? = null
							if(it.licitaciones.isNotEmpty()) {
								pujaGanadora = Subasta.instancia?.licitaciones?.last()
							}
							server.sendToAllLogged(EndSubasta(pujaGanadora))
							println("cerrando subasta")
							Subasta.cerrar()
						}
					} ?: run {
						if(now - Subasta.closedAt > 20000) {
							println("creando subasta")
							Subasta.crear()
							server.sendToAllLogged(StartSubastaMessage(Subasta.instancia!!.lote))
						}
					}
					Subasta.permit.release()
				}
			}, 0, 500)
		}
	}
}

package hibernate

import dao.ClienteDao
import dao.ProductoDao
import dao.LoteDao
import dao.LicitacionDao
import java.util.concurrent.Semaphore

class DataSource
{
	val clientes = ClienteDao()
	val productos = ProductoDao()
	val lotes = LoteDao()
	val licitaciones = LicitacionDao()
	
	companion object : Semaphore(1, true) {
		val source = DataSource()
	}
}
package messages.login

import messages.Message
import dataobjects.Cliente

class LoginAnswerMessage(val status : Int, val cliente : Cliente?) : Message("answerlogin")
{
	
}
package messages.login

import messages.Message
import dataobjects.Cliente

class LogoutMessage(val client : Cliente) : Message("logout") {
}
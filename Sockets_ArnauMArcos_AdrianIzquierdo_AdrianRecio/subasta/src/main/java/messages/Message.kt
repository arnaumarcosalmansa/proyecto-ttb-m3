package messages

import java.io.Serializable

open class Message(val action: String) : Serializable
{
	
}

class EchoMessage(val message: String) : Message("echo")
{
	
}
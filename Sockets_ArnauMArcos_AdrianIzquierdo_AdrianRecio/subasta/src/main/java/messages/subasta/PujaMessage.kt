package messages.subasta

import messages.Message
import dataobjects.Cliente

class PujaMessage(val cliente : Cliente, val puja : Int) : Message("pujasubasta"){
}
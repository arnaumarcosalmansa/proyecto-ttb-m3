package dao

import dataobjects.Cliente
import dataobjects.Producto
import dataobjects.Lote
import dataobjects.Licitacion


interface IClienteDao : IGenericDao<Cliente, String>
{
	
}


interface IProductoDao : IGenericDao<Producto, Int>
{
	
}

interface ILoteDao : IGenericDao<Lote, Int>
{
	
}

interface ILicitacionDao : IGenericDao<Licitacion, Int>
{
	
}

package dao

import dataobjects.Lote
import dataobjects.Licitacion

class LoteDao : GenericDao<Lote, Int>(), ILoteDao {
	
}

class LicitacionDao : GenericDao<Licitacion, Int>(), ILicitacionDao {
	public fun getMaxLicitacion(idLote : Int) : Licitacion?
	{
		val session = sessionFactory.getCurrentSession();
	
		session.beginTransaction();
			
		val sqlstring = "FROM Licitacion WHERE lote_id = :idLote ORDER BY cantidad DESC"
		val query = session.createQuery(sqlstring)
		query.setParameter("idLote", idLote)
		//query.setInteger("idLote", idLote)
		val results = query.list();
				
		session.getTransaction().commit()
		
		return if (results.size > 0) (results[0] as Licitacion) else null
	}
}
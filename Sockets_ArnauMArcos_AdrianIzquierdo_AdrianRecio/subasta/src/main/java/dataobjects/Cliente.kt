package dataobjects

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Column

import java.io.Serializable

@Entity
class Cliente : Serializable
{
	@Id
	@Column(length=100)
	var nombre: String? = null
	
	@Column
	var contrasena: String? = null
	
	@Column
	var saldoInicial: Int = 0
	
	@Column
	var saldoActual: Int = 0
	
	override fun equals(other: Any?): Boolean {
		other ?: return false
		if(other !is Cliente) return false
		
		return this.nombre?.equals(other.nombre) ?: false
	}

	override fun hashCode(): Int {
		return this.nombre?.hashCode() ?: 0
	}

	public override fun toString() : String
	{
		return "Cliente=> Nombre: " + this.nombre + ", saldo inicial: " + this.saldoInicial +
				", saldo actual: " + this.saldoActual;
	}
}
package cliente

import messages.Message
import messages.login.RequestLoginMessage
import messages.login.LoginAnswerMessage
import messages.subasta.EndSubasta
import messages.subasta.UpdateStatusMessage
import messages.subasta.UpdateFullStatusMessage
import messages.subasta.StartSubastaMessage
import messages.subasta.PujaRechazadaMessage
import messages.EchoMessage

class ClientNetworkReader(val cliente: ClienteSubasta) : Thread() {
	override public fun run() {
		try {
			while (true) {
				val message = cliente.ois.readObject() as Message
				processMessage(message)
			}
		} catch (e: Exception) {

		}
	}

	private fun processMessage(message: Message) {
		when (message) {
			is RequestLoginMessage -> {
				cliente.processRequestLogin(message)
			}
			is LoginAnswerMessage -> {
				cliente.processLoginAnswer(message)
			}
			is StartSubastaMessage -> {
				cliente.processStartSubasta(message)
			}
			is UpdateFullStatusMessage -> {
				cliente.processUpdateFullStatus(message)
			}
			is UpdateStatusMessage -> {
				cliente.processUpdateStatus(message)
			}
			is PujaRechazadaMessage -> {
				cliente.processPujaRechazada(message)
			}
			is EndSubasta -> {
				cliente.processEndSubasta(message)
			}
			is EchoMessage -> {
				cliente.processEchoMessage(message)
			}
		}
	}
}
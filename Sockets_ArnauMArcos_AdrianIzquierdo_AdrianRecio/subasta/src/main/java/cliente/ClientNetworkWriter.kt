package cliente

import java.util.Queue
import messages.Message
import java.util.concurrent.Semaphore

class ClientNetworkWriter(val cliente: ClienteSubasta) : Thread() {
	
	private val colaEnviar: MutableList<Message> = mutableListOf()
	val semaforoColaEnviar = Semaphore(1, true)

	override public fun run() {
		while (cliente.running) {
			semaforoColaEnviar.acquire()
			if (!colaEnviar.isEmpty()) {
				val message = colaEnviar.removeAt(0)
				semaforoColaEnviar.release()

				cliente.oos.writeObject(message)
				cliente.oos.flush()
			} else {
				semaforoColaEnviar.release()
			}

		}
	}
	
	public fun ponerEnCola(message: Message) {
		semaforoColaEnviar.acquire()
		colaEnviar.add(message)
		semaforoColaEnviar.release()
	}
}